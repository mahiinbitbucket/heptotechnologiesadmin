	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="http://heptotechnologies.org/wegacabs/public/uploads/54efc89051a4aba3cad0e5d4ac225ca1ed2a3ccb.jpeg">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Wega cabs</title>

      

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/linearicons.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/font-awesome.min.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/bootstrap.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/bootstrap.min.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/magnific-popup.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/nice-select.css">					
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/animate.min.css">
			<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">			
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/owl.carousel.css">
			<link rel="stylesheet" href="http://139.59.27.56/asset/new/css/main.css">
			
			

		</head>
		<body>
			<section class="banner-area relative" id="home">
				<div class="overlay overlay-bg"></div>	
		<!--top header start-->		
<section>
<div class="top_header_area">
	  
	    		<div class="row">
			    		<div class="col-md-6">
            <ul class="top_nav">
                <li><a href="#"><i class="fa fa-phone"></i>+91-08041553311</a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i>help@wegacabs.com</a></li>
               
            </ul>
			</div>
			
			 <div class="col-md-6">
            <ul class="social_navs">
                <li><a href="https://www.facebook.com/Wega-Cabs-Pvt-Ltd-555155441164651/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/wegacabs"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://plus.google.com/u/0/discover"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
			
	    </div>
		</div>
		</div>
	</section>
	<!-- top header closed-->
	<!-- header-manu start -->

	<section>		 
	
	
	<div id="navbar0">
		
<div id="h">
    <nav class="navb navbar-fixed">

       
			    	<div class="row">
			    		<div class="col-lg-6 logo">
				    
				    <a>weg<span style="color:#fdd943;font-size:30px;">AC</span>abs</a>
				</div>




 <div class="col-lg-6">
				      <nav id="nav-menu-container">
				        <ul class="nav-menu navbar-right">

				        	 <li><a class="active" href="index.html">Home</a></li>
   <li><a href="about.html">About</a></li>
   <li><a href="cars.html">Cars</a></li>
   <li><a href="service.html">Service</a></li>
	 <li><a href="contact.html">Contact</a></li>			          
				          
				   <a href="http://139.59.27.56/provider/login"  class="primary-btn9 text-uppercase">Booking</a>
				           		          
				       		          
				        </ul>
				      </nav><!-- #nav-menu-container -->		    		
			    	</div>
			    </div>
			     </nav>
			    </div>
			     </div>
			      
 </section>



<!-- header-manu closed -->

				<div class="container">
					<div class="row full-screen d-flex align-items-center justify-content-center" style="height:500px;">
						<div class="banner-content col-lg-7 col-md-6 ">
							
							<h1 class="text-white text-uppercase">
								Book Your Car Service To Any destinations			
							</h1>
							<p class="pt-20 pb-20 text-white">
								Prices vary depending on destinations and time.
							</p>

							<!--<a href="http://139.59.27.56/login"-->  
								<a href="#callaction-area"class="primary-btn text-uppercase">Download Now</a>

							<!--<a href="http://139.59.27.56/provider/login"  class="primary-btn5 text-uppercase">BECOME A DRIVER</a></div>-->
					
	                         			    
							    
							
						</div>											
					</div>
									
			</section>
			<!-- End banner Area -->	

			<!-- Start feature Area -->
			<section class="feature-area section-gap" id="service">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-8 pb-40 header-text">
							<h1>What Services we offer to our clients</h1>
							<p>
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-user"></span>Night Parties</h4>
								<p>
									After parties use our reliable APP to get home safe.
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-license"></span>Weddings</h4>
								<p>
									Book your right ride for your special occasions.
								</p>								
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-phone"></span>Hotels</h4>
								<p>
									Use our App for an easy web booking.
								</p>								
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-rocket"></span>Shopping Malls</h4>
								<p>
									We provide round trips to / From Malls and shopping centers.
								</p>				
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-diamond"></span>Airports</h4>
								<p>
									Book your ride to Airports and arrange your return .
								</p>								
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-bubble"></span>Cinemas</h4>
								<p>
									Late nights we are available to provide you with prompts and reliable services by simply using our App.
								</p>									
							</div>
						</div>						
					</div>
				</div>	
			</section>
			<!-- End feature Area -->		

			<!-- Start home-about Area -->
			<section class="home-about-area" id="about">
				<div class="container-fluid">
                 <div class="container">

					<div class="row justify-content-center align-items-center">
						<div class="col-md-6 no-padding home-about-left">
							<img class="img-fluid" src="http://139.59.27.56/asset/new/img/locations-map.png" alt="">
						</div>
						<div class="col-md-6 no-padding home-about-right">
							<h1>About our company</h1>
							
							<p>
								Wega Cabs app is a modern level of private transportation services that uses live GPS technologies. The service is built to provide our customers a professional ride at All Lift Level Of Transportation Services. Our friendly user interface app allows you to connect lively with drivers who are operating their own vehicles. Furthermore, customers will have access to locate their drivers and have control over the waiting time. By submitting pick up and drop off (destination) information, our app allows you to receive an approximate fare cost of your trip. Customers will also have the option to choose a payment method of cash or credit when prompted. Allo Taxi app is looking forward to serving you best.
							</p>
							<a class="text-uppercase primary-btn1" href="about.html" >get details</a>
						</div>
					</div>
				</div>	
				</div>	
			</section>
			<!-- End home-about Area -->	

			<!-- Start model Area -->
			<section class="model-area section-gap" id="cars">
				<div class="container">
					<div class="row d-flex justify-content-center pb-40">
						<div class="col-md-8 pb-40 header-text">
							<h1 class="text-center pb-10">Choose your Rental Car Model</h1>
							<p class="text-center">
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>				
					<div class="active-model-carusel">
						<div class="row align-items-center single-model item">
							<div class="col-lg-6 model-left">
								<div class="title justify-content-between d-flex">
									<h4 class="mt-20">HATCHBACK</h4>
									<h2>$149<span>/day</span></h2>
								</div>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								</p>
								<p>
									Capacity         : 04 Person <br>
									Doors            : 04 <br>
									Air Condition    : Dual Zone <br>
									Transmission     : Automatic
								</p>
								<a class="text-uppercase primary-btn3" href="#" >Book This Car Now</a>
							</div>
							<div class="col-lg-6 model-right">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/pic3.jpg" alt="">
							</div>
						</div>
						<div class="row align-items-center single-model item">
							<div class="col-lg-6 model-left">
								<div class="title justify-content-between d-flex">
									<h4 class="mt-20">SEDAN</h4>
									<h2>$149<span>/day</span></h2>
								</div>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								</p>
							<p>
									Capacity         : 04 Person <br>
									Doors            : 04 <br>
									Air Condition    : Dual Zone <br>
									Transmission     : Automatic
								</p>
								<a href="#"  class="primary-btn3 text-uppercase">Book This Car Now</a>
							</div>
							<div class="col-lg-6 model-right">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/pic4.jpg" alt="">
							</div>
						</div>
						<div class="row align-items-center single-model item">
							<div class="col-lg-6 model-left">
								<div class="title justify-content-between d-flex">
									<h4 class="mt-20">INNOVA</h4>
									<h2>$149<span>/day</span></h2>
								</div>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								</p>
								<p>
									Capacity         : 04 Person <br>
									Doors            : 04 <br>
									Air Condition    : Dual Zone <br>
									Transmission     : Automatic
								</p>
								<a href="#"  class="primary-btn3  text-uppercase">Book This Car Now</a>
							</div>
							<div class="col-lg-6 model-right">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/pic5.jpg" alt="">
							</div>
						</div>												
					</div>
				</div>	
			</section>
			<!-- End model Area -->


			<!-- Start fact Area -->
			<section class="facts-area section-gap" id="facts-area">
				<div class="container">
					<div class="row">
						<div class="col single-fact">
							<h1 class="counter">2536</h1>
							<p>Projects Completed</p>
						</div>
						<div class="col single-fact">
							<h1 class="counter">6784</h1>
							<p>Really Happy Clients</p>
						</div>
						<div class="col single-fact">
							<h1 class="counter">1059</h1>
							<p>Total Tasks Completed</p>
						</div>
						<div class="col single-fact">
							<h1 class="counter">2239</h1>
							<p>Cups of Coffee Taken</p>
						</div>	
						<div class="col single-fact">
							<h1 class="counter">435</h1>
							<p>In House Professionals</p>
						</div>												
					</div>
				</div>	
			</section>
			<!-- end fact Area -->							

			<!-- Start reviews Area -->
			<section class="reviews-area section-gap" id="review">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-8 pb-40 header-text text-center">
							<h1>Some Features that Made us Unique</h1>
							<p class="mb-10 text-center">
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>					
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>Car service for Every Pocket</h4>
								<p>
									From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket.
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>Secure and Safer Rides</h4>
								<p>
									Verified /Background Check on all drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience..
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>Andre Gonzalez</h4>
								<p>
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>							
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>Your safety first</h4>
								<p>
									Keep your loved ones informed about your travel routes or call emergency services when in need.
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>Customize your trip</h4>
								<p>
									Choose your preferred vehicle and other details to make your ride yours.
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-review">
								<h4>24/7 customer support</h4>
								<p>
									A dedicated 24x7 customer support team always at your service to help solve any problem.
								</p>
								<div class="star">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>												
					</div>
				</div>	
			</section>
			<!-- End reviews Area -->
			

			<!-- Start callaction Area -->
		
			<section class="callaction-area relative section-gap" id="callaction-area">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-12">
							<h1 class="text-white">Experience Great Support</h1>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
							</p>
							
							<div class="row">
								
								<div class="col-lg-6">
									<div class="head1">
									<h>Wega User App</h></div>
  <div class="column">
  	<div id="left2">
    <img src="http://139.59.27.56/asset/new/img/android.jpg" alt="Snow" style="width:100%;" ></div>
  </div>
  <!--<div class="column">
  	<div id="right2">
    <img src="http://139.59.27.56/asset/new/img/IOS.jpg" alt="Forest" style="width:100%;"></div>
  </div>-->
  </div>
  
  <div class="col-lg-6">
  	<div class="head1">
									<h>Wega Partner App</h></div>
   <div class="column">
  	<div id="left2">
    <img src="http://139.59.27.56/asset/new/img/android.jpg" alt="Snow" style="width:100%;" ></div>
  </div>
  	<!--<a href="#" class="primary-btn7 text-uppercase">Book Now</a>
							
  
   	<a href="#" class="primary-btn6 text-uppercase">BECOME A DRIVER</a>-->
  </div>
			</div>				
					</div>
				</div>	
				</div>
				</div>	

			</section>
			
			<!-- End callaction Area -->

			<!-- Start blog Area -->
			<!--<section class="blog-area section-gap" id="blog">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Latest From Our Blog</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua.</p>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-lg-3 col-md-6 single-blog">
							<div class="thumb">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/b1.jpg" alt="">								
							</div>
							<p class="date">10 Jan 2018</p>
							<a href="blog-single.html"><h4>Addiction When Gambling
							Becomes A Problem</h4></a>
							<p>
								inappropriate behavior ipsum dolor sit amet, consectetur.
							</p>
							<div class="meta-bottom d-flex justify-content-between">
								<p><span class="lnr lnr-heart"></span> 15 Likes</p>
								<p><span class="lnr lnr-bubble"></span> 02 Comments</p>
							</div>									
						</div>
						<div class="col-lg-3 col-md-6 single-blog">
							<div class="thumb">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/b2.jpg" alt="">								
							</div>
							<p class="date">10 Jan 2018</p>
							<a href="blog-single.html"><h4>Addiction When Gambling
							Becomes A Problem</h4></a>
							<p>
								inappropriate behavior ipsum dolor sit amet, consectetur.
							</p>
							<div class="meta-bottom d-flex justify-content-between">
								<p><span class="lnr lnr-heart"></span> 15 Likes</p>
								<p><span class="lnr lnr-bubble"></span> 02 Comments</p>
							</div>									
						</div>
						<div class="col-lg-3 col-md-6 single-blog">
							<div class="thumb">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/b3.jpg" alt="">								
							</div>
							<p class="date">10 Jan 2018</p>
							<a href="blog-single.html"><h4>Addiction When Gambling
							Becomes A Problem</h4></a>
							<p>
								inappropriate behavior ipsum dolor sit amet, consectetur.
							</p>
							<div class="meta-bottom d-flex justify-content-between">
								<p><span class="lnr lnr-heart"></span> 15 Likes</p>
								<p><span class="lnr lnr-bubble"></span> 02 Comments</p>
							</div>									
						</div>
						<div class="col-lg-3 col-md-6 single-blog">
							<div class="thumb">
								<img class="img-fluid" src="http://139.59.27.56/asset/new/img/b4.jpg" alt="">								
							</div>
							<p class="date">10 Jan 2018</p>
							<a href="blog-single.html"><h4>Addiction When Gambling
							Becomes A Problem</h4></a>
							<p>
								inappropriate behavior ipsum dolor sit amet, consectetur.
							</p>
							<div class="meta-bottom d-flex justify-content-between">
								<p><span class="lnr lnr-heart"></span> 15 Likes</p>
								<p><span class="lnr lnr-bubble"></span> 02 Comments</p>
							</div>									
						</div>							
					</div>
				</div>	
			</section>-->
			<!-- End blog Area -->


			<!-- start footer Area -->

           <div class="contents">
          <h1>Meet Our Team</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

           </div>

<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Quick links</h6>
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="cars.html">Cars</a></li>
									<li><a href="service.html">Services</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>								
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Features</h6>
								<ul>
									<li><a href="#">Car service for Every Pocket</a></li>
									<li><a href="#">Secure and Safer Rides</a></li>
									<li><a href="#">Your safety first</a></li>
									<li><a href="#">Customize your trip</a></li>
								</ul>								
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Resources</h6>
								<ul>
									<li><a href="#">Jobs</a></li>
									<li><a href="#">Brand Assets</a></li>
									<li><a href="#">Investor Relations</a></li>
									<li><a href="#">Terms of Service</a></li>
								</ul>								
							</div>
						</div>												
						<div class="col-lg-2 col-md-6 col-sm-6 social-widget">
							<div class="single-footer-widget">
								<h6>Follow Us</h6>
								<p>Let us be social</p>
								<div class="footer-social d-flex align-items-center">
									<a href="https://www.facebook.com/Wega-Cabs-Pvt-Ltd-555155441164651/"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/wegacabs"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>							
						<div class="col-lg-4  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Newsletter</h6>
								<p>Stay update with our latest</p>
								<div class="" id="mc_embed_signup">
									<form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
										<input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
			                            	<button class="click-btn btn btn-default"><span class="lnr lnr-arrow-right"></span></button>
			                            	<div style="position: absolute; left: -5000px;">
												<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
											</div>

										<div class="info"></div>
									</form>
								</div>
							</div>
						</div>	
						<p class="mt-50 mx-auto footer-text col-lg-12">
							<!-- Link back to  can't be removed. Template is licensed under CC BY 3.0. -->
Designed &copy 2018     <a href="http://wegacabs.com/">Wegacabs</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
						<div class='scrolltop'>
    <div class='scroll icon'><img src="http://139.59.27.56/asset/new/img/up-arrow-button (2).png"></div>

</div>											
					</div>
				</div>
			</footer>	

			<script src="http://139.59.27.56/asset/new/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="http://139.59.27.56/asset/new/js/vendor/bootstrap.min.js"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>			
  			<script src="http://139.59.27.56/asset/new/js/easing.min.js"></script>			
			<script src="http://139.59.27.56/asset/new/js/hoverIntent.js"></script>
			<script src="http://139.59.27.56/asset/new/js/superfish.min.js"></script>	
			<script src="http://139.59.27.56/asset/new/js/jquery.ajaxchimp.min.js"></script>
			<script src="http://139.59.27.56/asset/new/js/jquery.magnific-popup.min.js"></script>	
			<script src="http://139.59.27.56/asset/new/js/owl.carousel.min.js"></script>			
			<script src="http://139.59.27.56/asset/new/js/jquery.sticky.js"></script>
			<script src="http://139.59.27.56/asset/new/js/jquery.nice-select.min.js"></script>	
			<script src="http://139.59.27.56/asset/new/js/waypoints.min.js"></script>
			<script src="http://139.59.27.56/asset/new/js/jquery.counterup.min.js"></script>					
			<script src="http://139.59.27.56/asset/new/js/parallax.min.js"></script>		
			<script src="http://139.59.27.56/asset/new/js/mail-script.js"></script>	
			<script src="http://139.59.27.56/asset/new/js/main.js"></script>	
			<script>
				// When the user scrolls the page, execute myFunction 

// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};

// Get the navbar
var navbar = document.getElementById("navbar0");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}</script>


<!--sticky background color start-->

<script>
$(document).ready(function(){       
            var scroll_pos = 0;
            $(document).scroll(function() { 
                scroll_pos = $(this).scrollTop();
                if(scroll_pos > 210) {
                    $("nav").css('background-color', 'rgba(0, 0, 0, 0.8);');
                    $("nav a").css('color', 'white');
                      
                } else {
                    $("nav").css('background-color', 'transparent');
                    $("nav a").css('color', 'white');
                }
            });
        });
</script>
<!--sticky background color closed-->
<!--scrolltop button-->
<script>

$(window).scroll(function() {
    if ($(this).scrollTop() > 50 ) {
        $('.scrolltop:hidden').stop(true, true).fadeIn();
    } else {
        $('.scrolltop').stop(true, true).fadeOut();
    }
});
$(function(){$(".scroll").click(function(){$("html,body").animate({scrollTop:$("#home").offset().top},"1000");return false})})
</script>


</body>
</html>



