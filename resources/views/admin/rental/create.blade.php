@extends('admin.layout.base')

@section('title', 'Add Rental Package ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.rental.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Rental Package</h5>

            <form class="form-horizontal" action="{{route('admin.rental.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{ csrf_field() }}
				<div class="form-group row">
                    <label for="service_type" class="col-xs-12 col-form-label">Service Type</label>
                    <div class="col-xs-10">
                        <select class="form-control" name="service_type" id="service_type">
							<?php foreach( $ServiceType as $zone) { ?>
							<option value="<?php echo $zone->id;?>"><?php echo $zone->name;?></option>
							<?php } ?>
						</select>	
                    </div>
                </div>
				
                <div class="form-group row">
                    <label for="hours" class="col-xs-12 col-form-label">Hours</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('hours') }}" name="hours" required id="hours" placeholder="Hours">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kms" class="col-xs-12 col-form-label">kms</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('kms') }}" name="kms" required id="kms" placeholder="kms">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="extra_km" class="col-xs-12 col-form-label">Extra km ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('extra_km') }}" name="extra_km" required id="extra_km" placeholder="Extra km">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="extra_hours" class="col-xs-12 col-form-label">Extra Hours ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('extra_hours') }}" name="extra_hours" required id="extra_hours" placeholder="Extra Hours">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="amount" class="col-xs-12 col-form-label">Amount ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('amount') }}" name="amount" required id="amount" placeholder="amount">
                    </div>
                </div>
				
				<div class="form-group row">
                    <div class="col-xs-10">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="{{ route('admin.rental.index') }}" class="btn btn-danger btn-block">Cancel</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 offset-md-6 col-md-3">
                                <button type="submit" class="btn btn-primary btn-block">Add Rental</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
