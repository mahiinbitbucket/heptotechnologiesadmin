@extends('admin.layout.base')

@section('title', 'Update Rental Package ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.rental.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Rental</h5>

            <form class="form-horizontal" action="{{route('admin.rental.update', $rental->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
				
				<div class="form-group row">
                    <label for="service_type" class="col-xs-12 col-form-label">Service Type</label>
                    <div class="col-xs-10">
                        <select class="form-control" name="service_type" id="service_type">
							<?php foreach( $rentals as $zone) { ?>
							<option value="<?php echo $zone->id;?>" <?php if(@$rental->service_type == $zone->id) echo 'selected="selected"'; ?>><?php echo $zone->name;?></option>
							<?php } ?>
						</select>	
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="hours" class="col-xs-12 col-form-label">Hours</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $rental->hours }}" name="hours" required id="hours" placeholder="Hours">
                    </div>
                </div>
				
                <div class="form-group row">
                    <label for="kms" class="col-xs-12 col-form-label">kms</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $rental->kms }}" name="kms" required id="kms" placeholder="kms">
                    </div>
                </div>
				

                <div class="form-group row">
                    <label for="extra_km" class="col-xs-12 col-form-label">Extra km ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $rental->extra_km }}" name="extra_km" required id="extra_km" placeholder="Extra km">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="extra_hours" class="col-xs-12 col-form-label">Extra Hours ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $rental->extra_hours }}" name="extra_hours" required id="extra_hours" placeholder="Extra Hours">
                    </div>
                </div>
				
				
				<div class="form-group row">
                    <label for="amount" class="col-xs-12 col-form-label">Amount ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $rental->amount }}" name="amount" required id="amount" placeholder="amount">
                    </div>
                </div>
				
				<div class="form-group row">
				<div class="col-xs-10">
                        <div class="row">
						
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="{{route('admin.rental.index')}}" class="btn btn-danger btn-block">Cancel</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 offset-md-6 col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">Update Rental Package</button>
                    </div>
                </div>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection