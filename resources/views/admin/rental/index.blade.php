@extends('admin.layout.base')

@section('title', 'Rental Package ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Rental Packages</h5>
            <a href="{{ route('admin.rental.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Rental</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Service Name</th>
                        <th>Hours</th>
                        <th>Kilometers</th>
                        <th>Extra km</th>
                        <th>Extra hours</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rental as $index => $service)
                    <tr>
                        <td>{{ $index + 1 }}</td>
						<?php 
						$v = '';
						foreach( $ServiceType as $zone) { 
						if($service->service_type == $zone->id)
						{
							$v = $zone->name;
						}
						 
						} ?>
                        <td>{{ $v }}</td>
                        <td>{{ $service->hours }}</td>
                        <td>{{ distance ($service->kms) }}</td>
                        <td>{{ currency($service->extra_km) }}</td>
                        <td>{{ currency($service->extra_hours) }}</td>
                        <td>{{ currency($service->amount) }}</td>
                        <td>
                            <form action="{{ route('admin.rental.destroy', $service->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('admin.rental.edit', $service->id) }}" class="btn btn-info btn-block">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <button class="btn btn-danger btn-block" onclick="return confirm('Are you sure?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                         <th>ID</th>
                        <th>Service Name</th>
                        <th>Hours</th>
                        <th>Kilometers</th>
                        <th>Extra km</th>
                        <th>Extra hours</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection