@extends('admin.layout.base')

@section('title', 'Update Service Type ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.service.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Service</h5>

            <form class="form-horizontal" action="{{route('admin.service.update', $service->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group row">
                    <label for="name" class="col-xs-2 col-form-label">Service Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->name }}" name="name" required id="name" placeholder="Service Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provider_name" class="col-xs-2 col-form-label">Provider Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->provider_name }}" name="provider_name" required id="provider_name" placeholder="Provider Name">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="servicezone" class="col-xs-2 col-form-label">Service Zones</label>
                    <div class="col-xs-10">
                        <select class="form-control" name="servicezone" id="servicezone">
							<option value='all'>All</option>
							<?php foreach( $servicezones as $zone) { ?>
							<option value="<?php echo $zone->id;?>" <?php if(@$service->servicezone == $zone->id) echo 'selected="selected"'; ?>><?php echo $zone->name;?></option>
							<?php } ?>
						</select>	
                    </div>
                </div>

                <div class="form-group row">
                    
                    <label for="image" class="col-xs-2 col-form-label">Picture</label>
                    <div class="col-xs-10">
                        @if(isset($service->image))
                        <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{ $service->image }}">
                        @endif
                        <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="image" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fixed" class="col-xs-2 col-form-label">Inter Base Price ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->fixed }}" name="fixed" required id="fixed" placeholder="Inter Base Price">
                    </div>
                </div>
				
				<!--<div class="form-group row">
                    <label for="threshold_km" class="col-xs-2 col-form-label">Threshold Distance (kms)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->threshold_km }}" name="threshold_km" id="threshold_km" placeholder="Threshold Distance">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="before_threshold_price" class="col-xs-2 col-form-label">Before Threshold Distance Price ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->before_threshold_price }}" name="before_threshold_price" id="before_threshold_price" placeholder="Before Threshold Distance Price">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="after_threshold_price" class="col-xs-2 col-form-label">After Threshold Distance Price ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->after_threshold_price }}" name="after_threshold_price" id="after_threshold_price" placeholder="After Threshold Distance Price">
                    </div>
                </div>-->
				
                <div class="form-group row">
                    <label for="outer_fixed" class="col-xs-2 col-form-label">Outer Base Price ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->outer_fixed}}" name="outer_fixed" required id="outer_fixed" placeholder="Outer Base Price">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fixed" class="col-xs-2 col-form-label">Ride Insurance ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->connection }}" name="connection" required id="connection" placeholder="Ride Insurance">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="day" class="col-xs-2 col-form-label">Day Fees</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->day }}" name="day" required id="day" placeholder="Day Fees">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="distance" class="col-xs-2 col-form-label">Inter Base Distance ({{ distance() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->distance }}" name="distance" required id="distance" placeholder="Inter Base Distance">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="outer_distance" class="col-xs-2 col-form-label">Outer Base Distance ({{ distance() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->outer_distance}}" name="outer_distance" required id="outer_distance" placeholder="Outer  Base Distance">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="minute" class="col-xs-2 col-form-label">Unit Time Pricing (For Rental amount per hour / 60) ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->minute }}" name="minute" required id="minute" placeholder="Unit Time Pricing">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-xs-2 col-form-label">Inter Distance Price ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->price }}" name="price" required id="price" placeholder="Inter Distance Price">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="outer_price" class="col-xs-2 col-form-label">Outer Distance Price ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->outer_price}}" name="outer_price" required id="outer_price" placeholder="Outer Distance Price">
                    </div>
                </div>


				<div class="form-group row">
                    <label for="ride_cancel_fee" class="col-xs-2 col-form-label">Ride Now Cancellation Fee ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->ride_cancel_fee }}" name="ride_cancel_fee" id="ride_cancel_fee" placeholder="Ride Now Cancellation Fee">
                    </div>
                </div>
				
				<div class="form-group row">
                    <label for="airport" class="col-xs-2 col-form-label">Airport Package ({{ currency('') }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->airport }}" name="airport" id="airport" placeholder="Airport Package">
                    </div>
                </div>
				
                 <div class="form-group row">
                    <label for="capacity" class="col-xs-2 col-form-label">Seat Capacity</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ $service->capacity }}" name="capacity" required id="capacity" placeholder="Seat Capacity">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="calculator" class="col-xs-2 col-form-label">Pricing Logic</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="calculator" name="calculator">
                            <option value="MIN" @if($service->calculator =='MIN') selected @endif>@lang('servicetypes.MIN')</option>
                            <option value="HOUR" @if($service->calculator =='HOUR') selected @endif>@lang('servicetypes.HOUR')</option>
                            <option value="DISTANCE" @if($service->calculator =='DISTANCE') selected @endif>@lang('servicetypes.DISTANCE')</option>
                            <option value="DISTANCEMIN" @if($service->calculator =='DISTANCEMIN') selected @endif>@lang('servicetypes.DISTANCEMIN')</option>
                            <option value="DISTANCEHOUR" @if($service->calculator =='DISTANCEHOUR') selected @endif>@lang('servicetypes.DISTANCEHOUR')</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="ride" class="col-xs-2 col-form-label">Applicable Ride</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="ride" name="ride">
                            <option value="INTER" @if($service->ride=='INTER') selected @endif>INTER CITY</option>
                            <option value="OUTER" @if($service->ride=='OUTER') selected @endif>OUTER CITY</option>
                            <option value="BOTH" @if($service->ride=='BOTH') selected @endif>BOTH</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="{{route('admin.service.index')}}" class="btn btn-danger btn-block">Cancel</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 offset-md-6 col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">Update Service Type</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection