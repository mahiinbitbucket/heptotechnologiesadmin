@extends('admin.layout.base')

@section('title', 'Dashboard ')

@section('styles')
    <link rel="stylesheet" href="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
@endsection

@section('content')
    <?php $diff = ['-success','-info','-warning','-danger']; ?>

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <div class="row">
                    <div class="col-md-8">
                        <div class="clearfix mb-1">
                            <h5 class="float-xs-left">Zone setup</h5>
                            <div class="float-xs-right">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="map" style="height: 500px"></div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">

                            <h5 class="mb-2 display-inline" style="display:inline; !important">Available zones</h5>
                        {{--<button class="btn btn-sm btn-success ml-2"> Add new</button>--}}
                        <table class="table table-bordered mt-2">
                            <thead>
                            <tr>
                                <td> Name</td>
                                <td> Price-Min</td>
                                <td> Price-KM</td>
                                <td> Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $zones as $zone)
                                <tr>
                                    <td>
                                        {{$zone->name}}
                                    </td>
                                    <td>
                                        {{$zone->price}}$
                                    </td> <td>
                                        {{$zone->price_km}}$
                                    </td>
                                    <td> <a href="{{url('/admin/zone/del/'.$zone->id)}}" class="btn btn-sm btn-danger"> Remove</a> </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                            {{--@if($providers->count() > 0)--}}
                            {{--@foreach($providers as $provider)--}}
                            {{--<p class="mb-0-5">{{$provider->first_name}} {{$provider->last_name}} <span class="float-xs-right">{{($provider->rating/5)*100}}%</span></p>--}}
                            {{--<progress class="progress progress{{$diff[array_rand($diff)]}} progress-sm" value="{{$provider->rating}}" max="5"></progress>--}}
                            {{--@endforeach--}}
                            {{--@endif--}}
                        </div>

                </div>


            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="savemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Save zone</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        {{--<span aria-hidden="true">&times;</span>--}}
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group ">
                            <label for="example-text-input" class="col-2 col-form-label">Name</label>
                            <div class="col-6">
                                <input class="form-control" type="text"  id="inputName">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="example-search-input" class="col-2 col-form-label">Rate per Minute</label>
                            <div class="col-6">
                                <input class="form-control" type="text"  id="inputPrice">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="example-search-input" class="col-2 col-form-label">Rate per KM</label>
                            <div class="col-6">
                                <input class="form-control" type="text"  id="inputKMPrice">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="example-email-input" class="col-2 col-form-label">Comments</label>
                            <div class="col-6">
                                <input class="form-control" type="email"  id="inputComment">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSave">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main/vendor/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>

    <script src="//maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=drawing,geometry"  ></script>
    <script src="https://cdn.rawgit.com/googlemaps/js-map-label/gh-pages/src/maplabel.js"></script>
    <script type="text/javascript">
        var postZone;
        $(document).ready(function() {
            $(document).on('click', '#btnSave', function(){
                postZone();
            });
            function postZone() {
                var geoJson;
                map.data.toGeoJson(function(obj) {
                    var features=obj.features;
                    var feature=features[features.length - 1];
                   geoJson= JSON.stringify(feature);
                });
                name=  $('#inputName').val();
                price=  $('#inputPrice').val();
                priceKM=  $('#inputKMPrice').val();
                comments=  $('#inputComment').val();
                $.post(window.location.href, {
                    name: name,
                    price: price,
                    price_km: priceKM,
                    comment: comments,
                    polygon: geoJson
                }, function (data) {
                    location.reload();
                });

            }

                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {
                            lat: 36.778259,
                            lng: -119.417931
        },
                        zoom: 11
                    });
                    google.maps.event.addListener(map, 'click', function(event) {
                        placeMarker(event.latLng);
                        console.log(event.latLng.toUrlValue(5));
                    });

                    function placeMarker(location) {
                        var marker = new google.maps.Marker({
                            position: location,
                            map: map
                        });

                    }
                    var all_overlays = [];
                    var selectedShape;
                    var drawingManager = new google.maps.drawing.DrawingManager({
                        drawingMode: google.maps.drawing.OverlayType.MARKER,
                        drawingControl: true,
                        drawingControlOptions: {
                            position: google.maps.ControlPosition.TOP_CENTER,
                            drawingModes: [
                                //google.maps.drawing.OverlayType.MARKER,
//                                google.maps.drawing.OverlayType.CIRCLE,
                                google.maps.drawing.OverlayType.POLYGON,
//                                google.maps.drawing.OverlayType.RECTANGLE
                            ]
                        },
                        markerOptions: {
                            icon: 'images/beachflag.png'
                        },
                        circleOptions: {
                            fillColor: '#ffff00',
                            fillOpacity: 0.2,
                            strokeWeight: 3,
                            clickable: false,
                            editable: true,
                            zIndex: 1
                        },
                        polygonOptions: {
                            clickable: true,
                            draggable: true,
                            editable: true,
                            fillColor: '#ffff00',
                            fillOpacity: 1,

                        },
                        rectangleOptions: {
                            clickable: true,
                            draggable: true,
                            editable: true,
                            fillColor: '#ffff00',
                            fillOpacity: 0.5,
                        }
                    });

                    function clearSelection() {
                        if (selectedShape) {
                            selectedShape.setEditable(false);
                            selectedShape = null;
                        }
                    }

                    function setSelection(shape) {
                        clearSelection();
                        selectedShape = shape;
                        shape.setEditable(true);
                        // google.maps.event.addListener(selectedShape.getPath(), 'insert_at', getPolygonCoords(shape));
                        // google.maps.event.addListener(selectedShape.getPath(), 'set_at', getPolygonCoords(shape));
                    }

                    function deleteSelectedShape() {
                        if (selectedShape) {
                            selectedShape.setMap(null);
                        }
                    }

                    function deleteAllShape() {
                        for (var i = 0; i < all_overlays.length; i++) {
                            all_overlays[i].overlay.setMap(null);
                        }
                        all_overlays = [];
                    }

                    function CenterControl(controlDiv, map) {

                        // Set CSS for the control border.
                        var controlUI = document.createElement('div');
                        controlUI.style.backgroundColor = '#fff';
                        controlUI.style.border = '2px solid #fff';
                        controlUI.style.borderRadius = '3px';
                        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
                        controlUI.style.cursor = 'pointer';
                        controlUI.style.marginBottom = '22px';
                        controlUI.style.textAlign = 'center';
                        controlUI.title = 'Select to delete the shape';
                        controlDiv.appendChild(controlUI);

                        // Set CSS for the control interior.
                        var controlText = document.createElement('div');
                        controlText.style.color = 'rgb(25,25,25)';
                        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
                        controlText.style.fontSize = '16px';
                        controlText.style.lineHeight = '38px';
                        controlText.style.paddingLeft = '5px';
                        controlText.style.paddingRight = '5px';
                        controlText.innerHTML = 'Delete Selected Zone';
                        controlUI.appendChild(controlText);


                        controlUI.appendChild(controlText);

                        // Setup the click event listeners: simply set the map to Chicago.
                        controlUI.addEventListener('click', function() {
                            deleteSelectedShape();
                        });

                    }
                    drawingManager.setMap(map);
                    var getPolygonCoords = function(newShape) {
                        console.log("We are one");
                        var len = newShape.getPath().getLength();
                        for (var i = 0; i < len; i++) {
                            console.log(newShape.getPath().getAt(i).toUrlValue(6));
                        }
                    };



                    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
                        console.log("overlaycomplete");
                        console.log(event);

                        all_overlays.push(event);
                        map.data.add(new google.maps.Data.Feature({
                            geometry: new google.maps.Data.Polygon([event.overlay.getPath().getArray()])
                        }));



                    });

//            map.data.addListener('addfeature', refreshGeoJsonFromData);

                    var centerControlDiv = document.createElement('div');
                    var centerControl = new CenterControl(centerControlDiv, map);

                    centerControlDiv.index = 1;
//                    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
                    var saveBtn = document.createElement('button');
                    saveBtn.innerHTML = "Save Zone";
                    saveBtn.setAttribute("data-toggle", "modal");
                    saveBtn.setAttribute("data-target", "#savemodal");
                    saveBtn.className = " btn btn-success";


                    var deleteBtn = document.createElement('button');
                    deleteBtn.innerHTML = "Delete Zone";
                    deleteBtn.style.marginLeft = "20px";
                    deleteBtn.className = " btn btn-danger";
                    deleteBtn.addEventListener('click', function() {
                        deleteSelectedShape();
                    });

                    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(saveBtn);
//                    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(deleteBtn);
            var polygons;
            function createPolygon(paths) {

                var polygon = new google.maps.Polygon({
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 1,
                    strokeColor: '#0000ff',
                    editable: true,
                    draggable: true,
                    paths: paths,
                    map: map
                });
                polygons = paths;
            }

            var overlays = [
            ];
            var zones = [];
            @foreach ($zones as $zone)

            overlays.push({!! $zone->polygon !!});
            zones.push("{{$zone->name}}");
            @endforeach
                    var j=0;
            overlays.forEach(function(entry) {
                map.data.addGeoJson(entry);
                console.log(entry);
                var polygonCoords=entry.geometry.coordinates;
                var bounds = new google.maps.LatLngBounds();
                var i;
                for (i = 0; i < polygonCoords[0].length; i++) {
                    var position = polygonCoords[0][i];
                    var latlan = new google.maps.LatLng(position[1], position[0]);

                    bounds.extend(latlan);
                }
                var center=bounds.getCenter();

                console.log(center.lat());
                console.log(center.lng());
                var name=zones[j];
                var mapLabel = new MapLabel({
                    text: name,
                    position: center,
                    map: map,
                    fontSize: 25,
                    align: 'center'
                });
                mapLabel.set("position", center);

j++;



            })

        })


    </script>

@endsection