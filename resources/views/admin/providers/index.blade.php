@extends('admin.layout.base')

@section('title', 'Providers ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Providers
                @if(Setting::get('demo_mode', 0) == 1)
                <span class="pull-right">(*personal information hidden in demo)</span>
                @endif
            </h5>
            <a href="{{ route('admin.provider.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Provider</a>
            <table class="table table-striped table-bordered dataTable" id="table-1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Ride City</th>
                        <th>Wallet</th>
                        <th>Total Requests</th>
                        <th>Accepted Requests</th>
                        <th>Cancelled Requests</th>
                        <th>Documents / Service Type</th>
                        <th>Online</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($providers as $index => $provider)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ @$provider->first_name }} {{ @$provider->last_name }}</td>
                        @if(Setting::get('demo_mode', 0) == 1)
                        <td>{{ substr(@$provider->email, 0, 3).'****'.substr(@$provider->email, strpos(@$provider->email, "@")) }}</td>
                        @else
                        <td>{{ @$provider->email }}</td>
                        @endif
                        @if(Setting::get('demo_mode', 0) == 1)
                        <td>+919876543210</td>
                        @else
                        <td>{{ @$provider->mobile }}</td>
                        @endif
                        <td>{{ @$provider->ride}}</td>
                        <td>{{ @$provider->wallet_amount}}</td>
                        <td>{{ @$provider->total_requests }}</td>
                        <td>{{ @$provider->accepted_requests }}</td>
                        <td>{{ @$provider->total_requests - @$provider->accepted_requests }}</td>
                        <td>
                            @if(@$provider->pending_documents() > 0 || @$provider->service == null)
                                <a class="btn btn-danger btn-block label-right" href="{{route('admin.provider.document.index', $provider->id )}}">Attention! <span class="btn-label">{{ @$provider->pending_documents() }}</span></a>
                            @else
                                <a class="btn btn-success btn-block" href="{{route('admin.provider.document.index', $provider->id )}}">All Set!</a>
                            @endif
                        </td>
                        <td>
                            @if($provider->service)
                                @if(@$provider->service->status == 'active')
                                    <label class="btn btn-block btn-primary">Yes</label>
                                @else
                                    <label class="btn btn-block btn-warning">No</label>
                                @endif
                            @else
                                <label class="btn btn-block btn-danger">N/A</label>
                            @endif
                        </td>
                        <td>
                            <div class="input-group-btn">
                                @if(@$provider->status == 'approved')
                                <a class="btn btn-danger btn-block" href="{{ route('admin.provider.disapprove', $provider->id ) }}">Disable</a>
                                @else
                                <a class="btn btn-success btn-block" href="{{ route('admin.provider.approve', $provider->id ) }}">Enable</a>
                                @endif
                                <button type="button" 
                                    class="btn btn-info btn-block dropdown-toggle"
                                    data-toggle="dropdown">Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('admin.provider.request', $provider->id) }}" class="btn btn-default"><i class="fa fa-search"></i> History</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.provider.statement', $provider->id) }}" class="btn btn-default"><i class="fa fa-account"></i> Statements</a>
                                    </li>
									<li>
                                        <a type="button" class="btn btn-info"  onclick="edit({{$provider->id}})"><i class="fa fa-account"></i>Add Wallet</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.provider.edit', $provider->id) }}" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
                                    </li>
                                    <li>
                                        <form action="{{ route('admin.provider.destroy', $provider->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-default look-a-like" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Ride City</th>
                        <th>Wallet</th>
                        <th>Total Requests</th>
                        <th>Accepted Requests</th>
                        <th>Cancelled Requests</th>
                        <th>Documents / Service Type</th>
                        <th>Online</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade edit" id="logu" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <h5 style="padding-left: 10px; ">Wallet:</h5>
        <form class="common-form" method="post" action="{{ url('/admin/add_wallet') }}" id="form_driver">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
        <div class="modal-body">
            <input type="number"  class="form-control" name="wallet">
            <input type="hidden" id="id" name="id">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button  onclick="submit()"  class="btn btn-primary" data-dismiss="modal">Submit</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
@endsection

@section('scripts')
   
    <script>
        function edit(id) {
            $("#id").val(id);
            $('#logu').modal('show'); 
        }    
		
		
		
		 $('#table-1').dataTable( {
  "scrollY": "500px",
  "scrollX": true
 
} ); 
        
    </script>
    
    
@endsection