@extends('admin.layout.base')

@section('title', 'Site Settings ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
			<h5>Referral Settings</h5>

            <form class="form-horizontal" action="{{ route('admin.settings.referral_store') }}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="referral_sender" class="col-xs-2 col-form-label">Referral Sender ({{ currency() }})</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ Setting::get('referral_sender', '')  }}" name="referral_sender" required id="referral_sender" placeholder="Referral Sender">
					</div>
				</div>

				<div class="form-group row">
					<label for="referral_receiver" class="col-xs-2 col-form-label">Referral Receiver ({{ currency() }})</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ Setting::get('referral_receiver', '')  }}" name="referral_receiver" required id="referral_receiver" placeholder="Referral Receiver">
					</div>
				</div>
				
				<div class="form-group row">
					<label for="referral_usage" class="col-xs-2 col-form-label">Referral Usage</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ Setting::get('referral_usage', '')  }}" name="referral_usage" required id="referral_usage" placeholder="Referral Usage">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
