<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>bcl_1672705653.htm</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.140">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;
margin: 48px 0px 139px 0px;padding: 0px;border: none;width: 793px;height: 1090px;border:10px solid #EEEEEE;
  border-collapse: collapse;}
#page_1 #id1_1 {border:none;margin: 27px 0px 0px 0px;padding: 0px;border:none;width: 793px;overflow: hidden;}
#page_1 #id1_2 {border:none;margin: 42px 0px 0px 68px;padding: 0px;border:none;
width: 725px;overflow: hidden;margin-left:auto!important;margin-right:auto!important}
#page_1 #id1_2 #id1_2_1 {float:left;border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 315px;overflow: hidden;}
#page_1 #id1_2 #id1_2_2 {float:left;border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 410px;overflow: hidden;}
#page_1 #id1_3 {border:none;margin: 23px 0px 0px 362px;padding: 0px;border:none;width: 431px;overflow: hidden;}
#page_1 #id1_4 {border:none;margin: 1px 0px 0px 179px;padding: 0px;border:none;width: 428px;overflow: hidden;}

#page_1 #p1dimg1 {position:absolute;top:0px;left:38px;z-index:-1;
width:717px;height:936px;}
#page_1 #p1dimg1 #p1img1 {width:717px;height:936px;}




#page_2 {position:relative; overflow: hidden;margin: 38px 0px 346px 38px;padding: 0px;border: none;width: 717px;height: 739px;}

#page_2 #p2dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:717px;height:739px;}
#page_2 #p2dimg1 #p2img1 {width:717px;height:739px;}




#page_3 {position:relative; overflow: hidden;margin: 38px 0px 207px 38px;padding: 0px;border: none;width: 755px;height: 878px;}
#page_3 #id3_1 {border:none;margin: 22px 0px 0px 290px;padding: 0px;border:none;width: 465px;overflow: hidden;}
#page_3 #id3_2 {border:none;margin: 28px 0px 0px 36px;padding: 0px;border:none;width: 644px;overflow: hidden;}
#page_3 #id3_2 #id3_2_1 {float:left;border:none;margin: 33px 0px 0px 0px;padding: 0px;border:none;width: 419px;overflow: hidden;}
#page_3 #id3_2 #id3_2_2 {float:left;border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 225px;overflow: hidden;}
#page_3 #id3_3 {border:none;margin: 24px 0px 0px 36px;padding: 0px;border:none;width: 645px;overflow: hidden;}

#page_3 #p3dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:717px;height:878px;}
#page_3 #p3dimg1 #p3img1 {width:717px;height:878px;}




.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: 14px 'Arial';color: #707070;line-height: 16px;}
.ft1{font: bold 36px 'Arial';line-height: 43px;}
.ft2{font: 14px 'Arial';color: #777777;line-height: 16px;}
.ft3{font: 14px 'Arial';line-height: 16px;}
.ft4{font: bold 16px 'Arial';color: #333333;line-height: 19px;}
.ft5{font: 16px 'Arial';line-height: 18px;}
.ft6{font: 1px 'Arial';line-height: 1px;}
.ft7{font: 1px 'Arial';line-height: 9px;}
.ft8{font: 1px 'Arial';line-height: 12px;}
.ft9{font: bold 16px 'Arial';line-height: 19px;}
.ft10{font: 13px 'Arial';color: #707070;line-height: 16px;}
.ft11{font: bold 13px 'Arial';color: #707070;line-height: 16px;}
.ft12{font: 1px 'Arial';line-height: 4px;}
.ft13{font: 13px 'Arial';color: #707070;line-height: 17px;}
.ft14{font: 13px 'Arial';color: #707070;line-height: 18px;}
.ft15{font: 14px 'Arial';color: #5d93bb;line-height: 16px;}
.ft16{font: bold 14px 'Arial';line-height: 16px;}
.ft17{font: 18px 'Arial';line-height: 21px;}
.ft18{font: 12px 'Arial';line-height: 15px;}
.ft19{font: 12px 'Arial';color: #9b9b9b;line-height: 15px;}
.ft20{font: 1px 'Arial';line-height: 11px;}
.ft21{font: 1px 'Arial';line-height: 10px;}
.ft22{font: bold 12px 'Arial';line-height: 15px;}
.ft23{font: 1px 'Arial';line-height: 8px;}
.ft24{font: bold 12px 'Arial';color: #9b9b9b;line-height: 15px;}
.ft25{font: 9px 'Arial';line-height: 15px;}
.ft26{font: 12px 'Arial';line-height: 16px;}
.ft27{font: 12px 'Arial';line-height: 24px;}
.ft28{font: 12px 'Arial';color: #9b9b9b;line-height: 24px;}
.ft29{font: 11px 'Arial';line-height: 14px;}
.ft30{font: 11px 'Arial';color: #9b9b9b;line-height: 14px;}
.left-img{
 height:300px;
    width:85%;
    min-width:85%;
margin-top: 10px

}
.p0{text-align: left;margin-top: 41px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 70px;margin-top: 1px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 359px;margin-top: 27px;margin-bottom: 0px;}
.p3{text-align: left;padding-left: 339px;margin-top: 35px;margin-bottom: 0px;}
.p4{text-align: right;padding-right: 243px;margin-top: 8px;margin-bottom: 0px;}
.p5{text-align: left;padding-left: 95px;margin-top: 0px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 71px;margin-top:20px;margin-bottom: 0px;}
.flex-container {
  display: flex;
  flex-wrap: wrap;
 margin-top: 40px;
}
.flex-container1 {
  display: flex;
  flex-wrap: wrap;
 margin-top: 20px;
}
.flex-container3 {
 display: flex;
  flex-flow: row wrap;
 

}
.flex-container3 img{
  height:30px;
  width:30px;
 

}
.hr-line1{
border-bottom: 2px solid #EEEEEE;
 line-height: 4;
 padding-top:5px;
}
.hr-line2{
border-bottom: 2px solid #EEEEEE;
 line-height: 4;
 padding-bottom:5px;
}
.p7{text-align: left;padding-left: 70px;margin-top: 20px;margin-bottom: 0px;}
.p8{text-align: left;padding-left: 11px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 111px;margin-top: 0px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: right;padding-right: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 8px;padding-right: 148px;margin-top: 10px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 8px;padding-right: 121px;margin-top: 13px;margin-bottom: 0px;}
.p15{text-align: left;padding-left: 8px;margin-top: 11px;margin-bottom: 0px;}
.p16{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p17{text-align: left;padding-left: 277px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: left;padding-left: 116px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: right;padding-right: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: left;padding-left: 36px;padding-right: 54px;margin-top: 131px;margin-bottom: 0px;}
.p24{text-align: left;padding-right: 181px;margin-top: 1px;margin-bottom: 0px;}
.p25{text-align: right;margin-top: 0px;margin-bottom: 0px;}
.left-img{width:100px;}


.td0{border-top: #eeeeee 1px solid;padding: 0px;margin: 0px;width: 101px;vertical-align: bottom;}
.td1{border-top: #eeeeee 1px solid;padding: 0px;margin: 0px;width: 182px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 101px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 182px;vertical-align: bottom;}
.td4{border-bottom: #f3f3f3 1px solid;padding: 0px;margin: 0px;width: 251px;vertical-align: bottom;background: #f3f3f3;}
.td5{border-bottom: #f3f3f3 1px solid;padding: 0px;margin: 0px;width: 54px;vertical-align: bottom;background: #f3f3f3;}
.td6{padding: 0px;margin: 0px;width: 251px;vertical-align: bottom;}
.td7{padding: 0px;margin: 0px;width: 54px;vertical-align: bottom;}
.td8{padding: 0px;margin: 0px;width: 251px;vertical-align: bottom;background: #f3f3f3;}
.td9{padding: 0px;margin: 0px;width: 54px;vertical-align: bottom;background: #f3f3f3;}
.td10{padding: 0px;margin: 0px;width: 403px;}
.td11{padding: 0px;margin: 0px;width: 25px;}
.td12{padding: 0px;margin: 0px;width: 0px;vertical-align: bottom;}


.tr0{height: 22px;}
.tr1{height: 17px;}
.tr2{height: 18px;}
.tr3{height: 33px;}
.tr4{height: 9px;}
.tr5{height: 32px;}
.tr6{height: 37px;}
.tr7{height: 12px;}
.tr8{height: 26px;}
.tr9{height: 21px;}
.tr10{height: 4px;}
.tr11{height: 23px;}
.tr12{height: 39px;}
.tr13{height: 16px;}
.tr14{height: 7px;}
.tr15{height: 6px;}
.tr16{height: 11px;}
.tr17{height: 27px;}
.tr18{height: 10px;}
.tr19{height: 31px;}
.tr20{height: 8px;}
.tr21{height: 24px;}
.tr22{height: 30px;}
.tr23{height: 41px;}
.tr24{height: 19px;}
.tr25{height: 43px;}
.tr26{height: 35px;}

.t0{width: 283px;margin-top: 33px;font: 14px 'Arial';}
.t1{width: 305px;margin-top: 10px;font: 14px 'Arial';color: #707070;}
.t2{width: 428px;font: 14px 'Arial';color: #707070;}
.t3{width: 645px;margin-left: 36px;margin-top: 10px;font: 12px 'Arial';}
.t4{width: 645px;font: 12px 'Arial';}
.t5{width: 645px;margin-top: 11px;font: 12px 'Arial';}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
<DIV class="dclr"></DIV>
<DIV id="id1_1">
<div class="flex-container3">
<P class="p0 ft0">{{$Email->payment?$Email->payment->created_at->formatLocalized('%A %d %B %Y'):''}}<br>
<span>Invoice Serial Id:{{$Email->payment?$Email->booking_id:''}}</span>
</P>
<P class="p0 ft0">
<img src="images/f1.png" style="padding-left:360px;"></P>
</div>


<P class="p2 ft1">{{Setting::get('currency','INR')}}{{$Email->payment->total }}</P>
<P class="p3 ft2">{{$Email->payment?$Email->booking_id:''}}</P>
<P class="p4 ft3">Thanks for travelling with us, {{$Email->user?$Email->user->first_name:''}} {{$Email->user?$Email->user->last_name:''}}</P>
</DIV>
<DIV>
<div class="hr-line1"></div>
</DIV>



<DIV id="id1_2">
<DIV id="id1_2_1">
<P class="p5 ft4">Ride Details</P>
<?php 
		$map_icon = asset('asset/img/marker-start.png');
		$static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$Email->s_latitude.",".$Email->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$Email->d_latitude.",".$Email->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$Email->route_key.
                            "&key=AIzaSyDLw7mPIUwqqFnqxtRKUb6zznvayHE6wKg";
							?>
<img src="<?php echo $static_map;?>"class="left-img">
<div class="flex-container">
<img src="http://139.59.27.56/storage/<?php echo $Email->provider->avatar;?>">
<P class="p6 ft5">{{$Email->provider?$Email->provider->first_name:''}} {{$Email->provider?$Email->provider->last_name:''}}</P>
</div>
<div class="hr-line2"></div>
	
	
<div class="flex-container1">
<img src="<?php echo $Email->service_type->image;?>">
<P class="p7 ft3"><?php echo $Email->service_type->name;?></P>
</div>

<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p8 ft3"><A href="javascript:void(0)"><?php $time = date("H:i:s A",strtotime($Email->started_at)); echo $time;?></A></P></TD>
	<TD class="tr0 td1"><P class="p9 ft3">{{$Email->s_address}}</P></TD>
</TR>
<!--<TR>
	<TD class="tr1 td2"><P class="p9 ft6">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p9 ft3">Bendre Nagar, Bengaluru,</P></TD>
</TR>
<TR>
	<TD class="tr2 td2"><P class="p9 ft6">&nbsp;</P></TD>
	<TD class="tr2 td3"><P class="p9 ft3">Karnataka, India</P></TD>
</TR>-->
<TR>
	<TD class="tr3 td2"><P class="p8 ft3"><A href="javascript: void(0)"><?php $time = date("H:i:s A",strtotime($Email->finished_at)); echo $time;?></A></P></TD>
	<TD class="tr3 td3"><P class="p9 ft3">{{$Email->d_address}}</P></TD>
</TR>
<!--<TR>
	<TD class="tr2 td2"><P class="p9 ft6">&nbsp;</P></TD>
	<TD class="tr2 td3"><P class="p9 ft3">Kamakya Layout,</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p9 ft6">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p9 ft3">Banashankari 3rd Stage,</P></TD>
</TR>
<TR>
	<TD class="tr2 td2"><P class="p9 ft6">&nbsp;</P></TD>
	<TD class="tr2 td3"><P class="p9 ft3">Banashankari, Bengaluru</P></TD>
</TR>-->
<TR>
	<TD class="tr4 td2"><P class="p9 ft7">&nbsp;</P></TD>
	<TD class="tr4 td3"><P class="p9 ft7">&nbsp;</P></TD>
</TR>
</TABLE>
</DIV>
<DIV id="id1_2_2">
<P class="p10 ft4">Bill Details</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr5 td4"><P class="p11 ft3">Ride Fare</P></TD>
	<TD class="tr5 td5"><P class="p9 ft3">{{Setting::get('currency','INR')}}{{$Email->payment?$Email->payment->fixed:''}}</P></TD>
</TR>
<TR>
	<TD class="tr3 td6"><P class="p11 ft0">Distance Fare</P></TD>
	<TD class="tr3 td7"><P class="p12 ft0"><NOBR>{{Setting::get('currency','INR')}}{{$Email->payment->distance }}</NOBR></P></TD>
</TR>
<TR>
	<TD class="tr6 td6"><P class="p11 ft0">Day Allowance Fare</P></TD>
	<TD class="tr6 td7"><P class="p12 ft0">{{Setting::get('currency','INR')}}{{$Email->payment->day }}</P></TD>
</TR>
<TR>
	<TD class="tr6 td6"><P class="p11 ft0">Commission</P></TD>
	<TD class="tr6 td7"><P class="p12 ft0">{{Setting::get('currency','INR')}}{{$Email->payment->commision }}</P></TD>
</TR>
<TR>
	<TD class="tr7 td6"><P class="p9 ft8">&nbsp;</P></TD>
	<TD class="tr7 td7"><P class="p9 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td8"><P class="p11 ft10"><SPAN class="ft9">Total Bill </SPAN>(rounded off)</P></TD>
	<TD class="tr8 td9"><P class="p12 ft9">{{Setting::get('currency','INR')}}{{$Email->payment->total }}</P></TD>
</TR>
<TR>
	<TD class="tr9 td8"><P class="p11 ft11">Includes {{Setting::get('currency','INR')}}{{$Email->payment->tax }} Taxes</P></TD>
	<TD class="tr9 td9"><P class="p9 ft6">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr10 td8"><P class="p9 ft12">&nbsp;</P></TD>
	<TD class="tr10 td9"><P class="p9 ft12">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p13 ft13">*Access Fee is charged for availing the Wegacabs
platform</P>
<P class="p14 ft14">We've fulfilled our promise to take you to destination for <NOBR>pre-agreed</NOBR> Total Fare. Modifying the drop/route can change this fare.</P>
<P class="p15 ft0">Have queries or complaints? <A href="mailto:help@wegacabs?Subject=Support"><SPAN class="ft15">Get support.</SPAN></A></P>
</DIV>
</DIV>
<DIV id="id1_3">
<P class="p16 ft9">Payment</P>
</DIV>

<DIV>
<div class="hr-line1"></div>
</DIV>

<DIV id="id1_4">
<TABLE cellpadding=0 cellspacing=0 class="t2">
<TR>

<TD class="tr8 td10"><img src="https://img.icons8.com/cotton/1600/money-bag-rupee.png" width="30" height="30"></TD>


	<TD class="tr8 td10"><P class="p9 ft0">Paid by {{$Email->payment_mode}}</P></TD>
	<TD class="tr8 td11"><P class="p9 ft0">{{Setting::get('currency','INR')}}{{$Email->payment->total }}</P></TD>
	
</TR>
</TABLE>
</DIV>
</DIV>



</BODY>
</HTML>
