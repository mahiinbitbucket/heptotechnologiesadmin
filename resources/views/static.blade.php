@extends('user.layout.app')

@section('content')
<div class="row gray-section no-margin">
    <div class="container">
        <div class="content-block">
            <h1 class="entry-title">Privacy Policy &amp; Terms</h1>
            <p>Welcome to WooCabs. While browsing or using our website and our services users are agreed and bound by the under listed terms and conditions of use, which are also policies of Woo Corporation and in relation to this website and the Service.</p>
<p>&nbsp;</p>
<h1>Privacy Policy:</h1>
<p>WooCabs adheres to ensuring that your privacy is protected. WooCabs platform may ask its users to provide certain information by which they can be identified while using the website &amp; mobile app, then you can be assured that it will only be used in accordance with this privacy statement.</p>
<p>WooCabs may change this policy from time to time by updating this page. Users may check this page time to time to be aware of the changes.</p>
<p>WHAT WE COLLECT</p>
<p>We may collect the following information:</p>
<ul>
<li>Name, Addresses</li>
<li>Contact information including email address and telephone number</li>
<li>Demographic information such as postcode, preferences and interests</li>
<li>Other information relevant to customer surveys and/or offers</li>
</ul>
<p>WHAT WE DO WITH THE INFORMATION WE GATHER</p>
<p>The information requested by WooCabs platform is essential to understand user needs and based on this collected information an itinerary is prepared. The information is collected for the following natural reasons:</p>
<ul>
<li>Keeping records to automate future purchases/bookings</li>
<li>To improve products and services for better user experience.</li>
<li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li>
<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.</li>
</ul>
<p>SECURITY</p>
<p>WooCabs ensures that all of our user&rsquo;s information is secure with us and cannot be compromised in any case. To prevent unauthorized access or disclosure we have put in highly secure physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
<h1>Taxi fare:</h1>
<p>In order to use WooCabs services, all customers will pay taxi fare, govt. taxes, toll fees, parking charge, driver night allowances, waiting charges, post limit extra per KM. price.</p>
<p>GST: A 5% of total fare will be charged additionally as Goods &amp; service tax to price quoted on the website or communicated over phone. This would be updated with the latest GST when ever is subjected to apply.</p>
<p>State Tax: Paid extra as per actual.</p>
<p>Toll Fee: Paid extra as per actual.</p>
<p>Airport Parking Charges: Paid Extra as per actual.</p>
<p>Driver Night Allowance: Rs. 250/night will be applicable in case of multiple day round trip or tour package bookings or cab booked between 22:00 hrs to 05:00 hrs. (Not applicable on One Way Routes)</p>
<p>Daily Limit: Fare mentioned on the website will be applicable for 250 km. Limit per day. Extra Km. will be charged extra based on the car category and per km. Price.</p>
<p>All toll&rsquo;s, state taxes and parking fee will be paid by customers directly whenever required.</p>
<p>&nbsp;</p>
<h1>Bookings:</h1>
<p>Customers can book online from WooCabs website or by calling on our 24/7 hotline (09779390039)</p>
<p>Customers can get an instant confirmation by booking online and making either advance payment or full payment on the website.</p>
<p>In order to book offline through our hotline all the bookings will be confirmed on tentative basis and will be confirmed 24 hrs. prior to departure.</p>
<h1>Responsibility:</h1>
<p>WooCabs do not take any responsibility of delay in reaching at the final destination if there will be any of the following issues:</p>
<ul>
<li>Traffic</li>
<li>Riots or Complete traffic Jam.</li>
<li>If any other alternate route is taken but not the actual.</li>
<li>Internal routes taken on customer&rsquo;s request.</li>
</ul>
<p>Customer will be responsible for their belongings. In case if any of the belongings left in taxi by mistake, customer has to inform the company within the time duration of 24 hours. These left over items will be kept with the lost &amp; found department by partner taxi operator and will be handed over to the customers once informed.</p>
<p>Customers will require informing woocabs customer service department in case of misconduct of cab driver. An appropriate action will be taken by WooCabs and will be notified to its customers.</p>
<p>WooCabs operates as an aggregator and does not own any cab and driver but works as a medium to book cabs.</p>
<p>WooCabs do not offer to transport any unlawful material (such as pornographic content, drugs or any other material that violates law or encourages criminal offense) is strictly prohibited. If the customer is found with any unlawful material such as drug, liquors, Weapons the taxi Driver and the company shall not be responsible for any consequences. In such case customers are fully responsible and will face any consequences by police or any legal authority.</p>
<p>If customer has any complaints regarding the services of the company or taxi, complaints and suggestions can be given with the help of feedback kit available with the driver in the taxi or at the website feedback section or by calling customer care.</p>
<p>The company reserves a right to cancel any booking at any time without giving any reason. (In such condition the customer will be informed well in advance). However the reason could be non availability of cabs, Riots, Changes in company policy for a specific route etc.</p>
<h1>Don&rsquo;ts While Using WooCabs Services:</h1>
<p>Smoking, and consuming alcohol inside the taxi is strictly prohibited. In case of littering the taxi and damaging the seat or parts of taxi in any manner customer has to pay in case of any damage done by them.</p>
<p>Encouraging taxi drivers to violate the traffic rules any kind of misbehave with the driver will not be tolerated and driver has right to refuse such pick-up or has right to alight the customers from the taxi immediately.</p>
<h1>Using Customers Contact Information:</h1>
<p>The company has right to use the customers contact details to send offers and events information for marketing purposes. The company reserves a right to call the customer or send notifications to promote the company offers and events.</p>
<h1>Change of T&amp;C</h1>
<p>Terms and conditions can be changed without any prior notice; customers can check the updates regularly on WooCabs website. By subscribing to or using any of the company services, you agree that you are bound by the Terms and Conditions aforesaid, regardless of how you subscribe to or use the services.</p>
			
        </div>
    </div>
</div>
@endsection