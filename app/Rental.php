<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
	protected $fillable = [
        'service_type',
        'hours',
        'kms',
        'amount',
        'extra_km',
        'extra_hours'
	];
	
	 protected $hidden = [
        'created_at', 'updated_at'
    ];
	
    //protected $guarded = [];
}
