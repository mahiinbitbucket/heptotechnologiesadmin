<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderPayment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id',
        'txnid',
        'amount'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'created_at', 'updated_at'
    ];

	protected $dates = [
        'created_at',
        'updated_at'
        
    ];

   public function scopeWalletStatement($query, $user_id)
    {
        return $query->where('provider_id', '=', $user_id)
				->whereNotIn('type' , ['RIDE', 'ADVANCE'])
                    ->select('*');
                    
    }
}
