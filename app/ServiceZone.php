<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceZone extends Model
{
	protected $fillable = [
        'user_id',
        'txnid',
        'amount'
        
    ];
	
	 protected $hidden = [
        'created_at', 'updated_at'
    ];
	
    //protected $guarded = [];
}
