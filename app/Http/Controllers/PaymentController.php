<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SendPushNotification;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\StripeInvalidRequestError;
use Log;
use Auth;
use Setting;
use Exception;

use App\Card;
use App\User;
use App\Provider;
use App\UserRequests;
use App\UserRequestPayment;
use App\UserPayment;
use App\ProviderPayment;

class PaymentController extends Controller
{
       /**
     * payment for user.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function payment(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|exists:user_request_payments,request_id|exists:user_requests,id,paid,0,user_id,'.Auth::user()->id,
                'tips' => 'numeric',
            ]);


        $UserRequest = UserRequests::find($request->request_id);

        if($UserRequest->payment_mode == 'CARD') {

            $RequestPayment = UserRequestPayment::where('request_id',$request->request_id)->first(); 

            $StripeCharge = $RequestPayment->total * 100;
            $chargeid = 0;
            try {
                if($StripeCharge > 0){
                  $Card = Card::where('user_id',Auth::user()->id)->where('is_default',1)->first();

                  Stripe::setApiKey(Setting::get('stripe_secret_key'));

                  $Charge = Charge::create(array(
                        "amount" => $StripeCharge,
                        "currency" => "usd",
                        "customer" => Auth::user()->stripe_cust_id,
                        "card" => $Card->card_id,
                        "description" => "Payment Charge for ".Auth::user()->email,
                        "receipt_email" => Auth::user()->email
                      ));
                }
                $RequestPayment->payment_id = $chargeid;
                $RequestPayment->tips = $request->tips?:0;
                $RequestPayment->payment_mode = 'CARD';
                $RequestPayment->save();

                $UserRequest->paid = 1;
                $UserRequest->status = 'COMPLETED';
                $UserRequest->save();

                if($request->ajax()) {
                   return response()->json(['message' => trans('api.paid')]); 
                } else {
                    return redirect('dashboard')->with('flash_success','Paid');
                }

            } catch(StripeInvalidRequestError $e){
                if($request->ajax()){
                    return response()->json(['error' => $e->getMessage()], 500);
                } else {
                    return back()->with('flash_error', $e->getMessage());
                }
            } catch(Exception $e) {
                if($request->ajax()){
                    return response()->json(['error' => $e->getMessage()], 500);
                } else {
                    return back()->with('flash_error', $e->getMessage());
                }
            }
        }
    } */
	
	
	public function payment(Request $request)
	{
		Log::info("Payment Request id :".$request->request_id);
		
		$this->validate($request, [
                'request_id' => 'required'
            ]);
			
			
			try
		{
			
			$UserRequest = UserRequests::find($request->request_id);
			
		//	if($UserRequest->payment_mode == 'PAYU') {
				
			$user = User::find(Auth::user()->id);
			$MERCHANT_KEY = "ZXX3ZK4R"; //change  merchant with yours
			$SALT = "0p7EJ6sBGg";  //change salt with yours  

			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			//optional udf values 
			
			
			$udf1 = '';
			$udf2 = '';
			$udf3 = '';
			$udf4 = '';
			$udf5 = '';

			$RequestPayment = UserRequestPayment::where('request_id',$request->request_id)->first(); 
			
			$amount= $RequestPayment->total;
			//$amount= 1.10;
			$product_info= "ride";
			$customer_name= $user->first_name;
			$customer_email= $user->email;
			
			
			$data = array("MERCHANT_KEY"=>$MERCHANT_KEY,"txnid"=>$txnid,"amount"=>$amount,"product_info"=>$product_info,"customer_name"=>$customer_name,"customer_email"=>$customer_email,"udf1"=>$udf1,"udf2"=>$udf2,"udf3"=>$udf3,"udf4"=>$udf4,"udf5"=>$udf5,"SALT"=>$SALT);
			
			$hashstring = $MERCHANT_KEY . '|' . $this->checkNull($txnid) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($product_info)  . '|' . $this->checkNull($customer_name) . '|' . $this->checkNull($customer_email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $SALT;
			$hash = strtolower(hash('sha512', $hashstring));
			
			return response()->json(['txnid' => $txnid, 'hash' =>$hash, 'HashString' => $hashstring]);
			
		//	}
			
			
		} catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
	}
	


    /**
     * add wallet money for user.
     *
     * @return \Illuminate\Http\Response
     */
	 
   /*  public function add_money(Request $request){

        $this->validate($request, [
                'amount' => 'required|integer',
                'card_id' => 'required|exists:cards,card_id,user_id,'.Auth::user()->id
            ]);

        try{
            
            $StripeWalletCharge = $request->amount * 100;

            Stripe::setApiKey(Setting::get('stripe_secret_key'));

            $Charge = Charge::create(array(
                  "amount" => $StripeWalletCharge,
                  "currency" => "usd",
                  "customer" => Auth::user()->stripe_cust_id,
                  "card" => $request->card_id,
                  "description" => "Adding Money for ".Auth::user()->email,
                  "receipt_email" => Auth::user()->email
                ));

            $update_user = User::find(Auth::user()->id);
            $update_user->wallet_balance += $request->amount;
            $update_user->save();

            Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
            Card::where('card_id',$request->card_id)->update(['is_default' => 1]);

            //sending push on adding wallet money
            (new SendPushNotification)->WalletMoney(Auth::user()->id,currency($request->amount));

            if($request->ajax()){
                return response()->json(['message' => currency($request->amount).trans('api.added_to_your_wallet'), 'user' => $update_user]); 
            } else {
                return redirect('wallet')->with('flash_success',currency($request->amount).' added to your wallet');
            }

        } catch(StripeInvalidRequestError $e) {
            if($request->ajax()){
                 return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } catch(Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => $e->getMessage()], 500);
            } else {
                return back()->with('flash_error', $e->getMessage());
            }
        }
    } */
	
	public function checkNull($value) {
            if ($value == null) {
                  return '';
            } else {
                  return $value;
            }
      }
	  
	public function get_payutxnid(Request $request)
	{
		Log::info("HI");
		Log::info("amount:".$request->amount);
		
		$this->validate($request, [
                'amount' => 'required'
            ]);
			
			
			try
		{
			$user = User::find(Auth::user()->id);
			$MERCHANT_KEY = "ZXX3ZK4R"; //change  merchant with yours
			$SALT = "0p7EJ6sBGg";  //change salt with yours  

			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			//optional udf values 
			
			
			$udf1 = '';
			$udf2 = '';
			$udf3 = '';
			$udf4 = '';
			$udf5 = '';

			$amount= $request->amount.'.0';
			$product_info= "wallet";
			$customer_name= $user->first_name;
			$customer_email= $user->email;
			
			
			$data = array("MERCHANT_KEY"=>$MERCHANT_KEY,"txnid"=>$txnid,"amount"=>$amount,"product_info"=>$product_info,"customer_name"=>$customer_name,"customer_email"=>$customer_email,"udf1"=>$udf1,"udf2"=>$udf2,"udf3"=>$udf3,"udf4"=>$udf4,"udf5"=>$udf5,"SALT"=>$SALT);
			
			
			//$hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|5' . $customer_name . '|' . $customer_email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
			
			
			Log::info("DATA PAYU:",$data);
			
			
			$hashstring = $MERCHANT_KEY . '|' . $this->checkNull($txnid) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($product_info)  . '|' . $this->checkNull($customer_name) . '|' . $this->checkNull($customer_email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $SALT;
			$hash = strtolower(hash('sha512', $hashstring));
			Log::info("HashString:".$hashstring);
			Log::info("PAYU HASH:".$hash);
			Log::info("PAYU TXNID:".$txnid);
			
			return response()->json(['txnid' => $txnid, 'hash' =>$hash, 'HashString' => $hashstring]);
			
		} catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
	}
	
	public function add_money(Request $request)
	{
		Log::info("Add Wallet txnid:".$request->txnid);
		Log::info("Add Wallet amount:".$request->amount);
		$this->validate($request, [
                'txnid' => 'required',
                'amount' => 'required'
            ]);
		
		try
		{
			
			$user = User::find(Auth::user()->id);
			$user->wallet_balance  = $user->wallet_balance + $request->amount;
			$user->save();
			
			$Payment = new UserPayment;
            $Payment->user_id = $user->id;
            $Payment->txnid = $request->txnid;
            $Payment->amount = $request->amount;
            $Payment->type = "WALLET";
			$Payment->save();
			
			//User::where('id',$user->id)->update(['wallet_balance' => $wallet]);
			
			/* $key = "ZXX3ZK4R"; //change  merchant with yours
			$salt = "0p7EJ6sBGg";  //change salt with yours 
			$command = 'verify_payment';
			$var1 = $request->txnid;
			$hashstring = $key . '|' . $command . '|' . $var1 . '|'  . $salt;
			
			$hash = strtolower(hash('sha512', $hashstring));
			
			$r = array('key' => $key , 'hash' =>$hash , 'var1' => $var1, 'command' => $command);   
			$wsUrl = "https://test.payu.in/merchant/postservice.php?form=1";
			$qs= http_build_query($r);
			$result = $this->curlCall($qs,$wsUrl);
			$data = json_decode($result);
			 */
		//	Log::info("PAYU STATUS:",$data);
			
			/* if($data['status'] == 1)
			{ */
			
			/* }	 */
			
			//Log::info("PAYU STATUS:",$data);
			
			$wsUrl = "https://api.razorpay.com/v1/payments/".$request->txnid."/capture";
			$keyId = 'rzp_live_crFaJ0i4kheD0b';
			$keySecret = 'Qw9aCqNzG3k6QuG35ZvRtW0E';
			$da = array();
			$da['amount'] = $request->amount * 100; 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_USERPWD, $keyId . ":" . $keySecret);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $da);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL,$wsUrl);
			$result=curl_exec($ch);
			curl_close($ch);
			
			$er = json_decode($result, TRUE);
			
			Log::info("Add Wallet capture:",$er);
			
			return response()->json(['status' => "SUCCESS"]);
		}
		catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
		
	}
	
	public function ride_transaction_details(Request $request)
	{
		Log::info("Razor_pay txnid:".$request->txnid);
		Log::info("Razor_pay amount:".$request->amount);
		Log::info("Razor_pay request_id:".$request->request_id);
		Log::info("Razor_pay payment_mode:".$request->payment_mode);
		
		$this->validate($request, [
                'txnid' => '',
                'amount' => 'required',
                'request_id' => 'required',
				'payment_mode' => 'in:CASH,RAZORPAY'
            ]);
		
		try
		{
			
			$UserRequest = UserRequests::findOrFail($request->request_id);
			if($request->payment_mode == 'CASH')
			{
				$UserRequest->payment_mode = 'CASH';
				$UserRequest->paid = 1;
			//	$UserRequest->status = 'COMPLETED';
				$UserRequest->save();
				return response()->json([
						'message' => trans('api.paid'),
						'status' => 'SUCCESS'
					]);
			}
			elseif($request->payment_mode == 'RAZORPAY') {
				$user = User::find(Auth::user()->id);
			
			$Payment = new UserPayment;
            $Payment->user_id = $user->id;
            $Payment->txnid =  $request->txnid;
            $Payment->amount = $request->amount;
            $Payment->type = "RIDE";
			$Payment->save();
			
		//	$UserRequest->status = 'COMPLETED';
			$UserRequest->paid = 1;
			$UserRequest->save();
			
			$pay = UserRequestPayment::where('request_id',$request->request_id)->first();
				
				$Provider = Provider::find($UserRequest->provider_id);
				$Provider->wallet_amount  = $Provider->wallet_amount - $pay->commision;
				$Provider->save();
				
				$Payment = new ProviderPayment;
				$Payment->provider_id = $Provider->id;
				$Payment->txnid = "";
				$Payment->amount = $pay->commision;
				$Payment->type = "WALLET_DEBIT";
				$Payment->save();
			
				// charged wallet money push 
				$t = currency($pay->commision).' '.trans('api.push.charged_from_wallet').' Current Balance:'. currency($Provider->wallet_amount);
				
				(new SendPushNotification)->ChargedWalletMoney_Provider($UserRequest->provider_id,$t);
				
				$wsUrl = "https://api.razorpay.com/v1/payments/".$request->txnid."/capture";
				$keyId = 'rzp_live_crFaJ0i4kheD0b';
				$keySecret = 'Qw9aCqNzG3k6QuG35ZvRtW0E';
				$da = array();
				$da['amount'] = $request->amount * 100; 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_USERPWD, $keyId . ":" . $keySecret);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $da);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$wsUrl);
				$result=curl_exec($ch);
				curl_close($ch);
				
				$er = json_decode($result, TRUE);
				
				Log::info("Ride Transaction capture:",$er);
				
			 return response()->json(['status' => "SUCCESS"]);
			}
		
			
		}
		catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
		
	}
	
	public function curlCall ($qs, $wsUrl)
	{
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $wsUrl);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
			curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
			$o = curl_exec($c);
	}
	
	public function advance_payment(Request $request)
	{
		$this->validate($request, [
                'amount' => 'required'
            ]);
			
			
			try
		{
			$user = User::find(Auth::user()->id);
			$MERCHANT_KEY = "ZXX3ZK4R"; //change  merchant with yours
			$SALT = "0p7EJ6sBGg";  //change salt with yours  

			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			//optional udf values 
			
			
			$udf1 = '';
			$udf2 = '';
			$udf3 = '';
			$udf4 = '';
			$udf5 = '';

			//$amount= $request->amount.'.0';
			$amount= $request->amount;
			$product_info= "advance";
			$customer_name= $user->first_name;
			$customer_email= $user->email;
			
			$hashstring = $MERCHANT_KEY . '|' . $this->checkNull($txnid) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($product_info)  . '|' . $this->checkNull($customer_name) . '|' . $this->checkNull($customer_email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $SALT;
			$hash = strtolower(hash('sha512', $hashstring));
			Log::info("HashString:".$hashstring);
			Log::info("PAYU HASH:".$hash);
			Log::info("PAYU TXNID:".$txnid);
			
			return response()->json(['txnid' => $txnid, 'hash' =>$hash, 'HashString' => $hashstring]);
			
		} catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
	}
	
	public function schedule_pay(Request $request)
	{
		
		Log::info("schedule_pay txnid:".$request->txnid);
		Log::info("schedule_pay amount:".$request->amount);
			
		$this->validate($request, [
                'txnid' => 'required',
                'amount' => 'required'
            ]);
		
		try
		{
			$user = User::find(Auth::user()->id);
			
			$Payment = new UserPayment;
            $Payment->user_id = $user->id;
            $Payment->txnid = $request->txnid;
            $Payment->amount = $request->amount;
            $Payment->type = "ADVANCE";
			$Payment->save();
			
			return response()->json(['status' => "SUCCESS"]);
		}
		catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
		
	}
	
	
	public function add_provider_wallet(Request $request)
	{
		Log::info("RAZOR txnid:".$request->txnid);
		Log::info("RAZOR amount:".$request->amount);
		$this->validate($request, [
                'txnid' => 'required',
                'amount' => 'required'
            ]);
		
		try
		{
			
			$Provider = Provider::find(Auth::user()->id);
			$Provider->wallet_amount  = $Provider->wallet_amount + $request->amount;
			$Provider->save();
			
			$Payment = new ProviderPayment;
            $Payment->provider_id = $Provider->id;
            $Payment->txnid = $request->txnid;
            $Payment->amount = $request->amount;
            $Payment->type = "WALLET";
			$Payment->save();
			
			$TextMsg = urlencode("Dear ".$Provider->first_name." ".$Provider->last_name.", You have sucessfully added money to your wallet. Added Amount: ".$request->amount." ,Wallet Balance: ".$Provider->wallet_amount." ,Transaction ID: ".$request->txnid);
			
			$MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$Provider->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
			
			
			$json = curl($MessageUrl);
			
			$MessageUrl = json_decode($json, TRUE);
				
			$wsUrl = "https://api.razorpay.com/v1/payments/".$request->txnid."/capture";
				$keyId = 'rzp_live_crFaJ0i4kheD0b';
				$keySecret = 'Qw9aCqNzG3k6QuG35ZvRtW0E';
				$da = array();
				$da['amount'] = $request->amount * 100; 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_USERPWD, $keyId . ":" . $keySecret);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $da);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$wsUrl);
				$result=curl_exec($ch);
				curl_close($ch);
				
				$er = json_decode($result, TRUE);
				
				Log::info("Add Provider Wallet capture:",$er);
			
			return response()->json(['status' => "SUCCESS"]);
		}
		catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
		
	}
	
	
	public function update_paymentmode(Request $request)
    {
		$this->validate($request, [
				'payment_mode' => 'in:CASH,CARD',
            ]);
		
		try
		{
			$UserRequest = UserRequests::find($request->request_id);
			$UserRequest->payment_mode = $request->payment_mode;
            $UserRequest->save();
			return $UserRequest;
			
		}
		catch (ModelNotFoundException $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }else{
                return back()->with('flash_error', 'No Request Found!');
            }
        }
		
		
	}
	
	
	
	
}
