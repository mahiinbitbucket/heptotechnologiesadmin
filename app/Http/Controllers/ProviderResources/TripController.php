<?php
namespace App\Http\Controllers\ProviderResources;
date_default_timezone_set("Asia/Kolkata");
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Auth;
use Log;
use Setting;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Http\Controllers\SendPushNotification;
use Mail;
use App\Mail\RequestUserInvoiceReminder;
use App\Mail\RequestUserBookingReminder;

use App\User;
use App\CarCategory;
use App\Provider;
use App\Admin;
use App\Chat;
use App\Promocode;
use App\UserRequests;
use App\ServiceType;
use App\Rental;
use App\RequestFilter;
use App\PromocodeUsage;
use App\ProviderService;
use App\UserRequestRating;
use App\UserRequestPayment;
use App\UserPayment;
use App\ProviderPayment;
use Location\Coordinate;
use Location\Distance\Vincenty;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            if($request->ajax()) {
                $Provider = Auth::user();
            } else {
                $Provider = Auth::guard('provider')->user();
            }

            $provider = $Provider->id;
			 if ($request->has("uuid")) {
                $udid=$Provider->device->udid;
                if ($request->uuid!=$udid) {

                    $Response = [
                        'account_status' => "invalid_session",

                        'service_status' => $Provider->service ? Auth::user()->service->status : 'offline',
                    ];

                    return $Response;
                }

            }

			$Provider = Provider::find(Auth::user()->id);
			
			$ProviderService1 = ProviderService::where('provider_id',$Provider->id)->first();
			if(($Provider->wallet_amount <=500) && ($Provider->status =='approved') && ($ProviderService1->status =='active'))
			{
				(new SendPushNotification)->WalletLimit($Provider->id);
				
				ProviderService::where('provider_id',$Provider->id)->update(['status' =>'offline']);
				
				//Provider::where('id',$Provider->id)->update(['status' => 'banned']);
				
			}
			
			 $Rides = UserRequests::where('provider_id', $Provider->id)
						->where('status', 'COMPLETED')
						->where('created_at', '>=', Carbon::today())
						->with('payment', 'service_type')
						->get();
			$f 	   = $Rides->count(); 
			Provider::where('id',$Provider->id)->update(['todays_ride' =>$Rides->count()]);

				
            $AfterAssignProvider = RequestFilter::with(['request.user', 'request.payment', 'request'])
                ->where('provider_id', $provider)
				->orderBy('id', 'asc')
                ->whereHas('request', function($query) use ($provider) {
                        $query->where('status','<>', 'CANCELLED');
                        $query->where('status','<>', 'SCHEDULED');
                        $query->where('provider_id', $provider );
                        $query->where('current_provider_id', $provider);
                    });

            $BroadCastAssignProvider = RequestFilter::with(['request.user', 'request.payment', 'request'])
                ->where('provider_id', $provider)
				->orderBy('id', 'asc')
                ->whereHas('request', function($query) use ($provider){
                        $query->where('status','<>', 'CANCELLED');
                        $query->where('status','<>', 'SCHEDULED');
                        $query->where('broad_cast', 'YES');
                        $query->whereNull('current_provider_id');
                    });

            $BeforeAssignProvider = RequestFilter::with(['request.user', 'request.payment', 'request'])
                ->where('provider_id', $provider)
				->orderBy('id', 'asc')
                ->whereHas('request', function($query) use ($provider){
                        $query->where('status','<>', 'CANCELLED');
                        $query->where('status','<>', 'SCHEDULED');
                        $query->where('broad_cast', 'NO');
                        $query->where('current_provider_id',$provider);
                    });
            

            $IncomingRequests =$BeforeAssignProvider->union($BroadCastAssignProvider)->union($AfterAssignProvider)->orderBy('created_at','desc')->get();

           if(!empty($request->latitude) && $request->latitude !=0.00000000 && $request->longitude !=0.00000000) {
                $Provider->update([
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude,
                ]);
            }

			$send_notify = UserRequests::where('status','SCHEDULED')
                            ->where('provider_id',$Provider->id)
                            ->orderBy('schedule_at','asc');
							
			$currenttime = Carbon::now();				
			if(!empty($send_notify))
			{
				foreach ($send_notify as $key => $value) 
				{
					$finalDate = date("Y-m-d H:i:s", strtotime("-1 hour", $value->schedule_at));

					if($finalDate == $currenttime)	
					{
						(new SendPushNotification)->ScheduleNotify($Provider->id);
					}
				}		
			}
			
			
            $Timeout = Setting::get('provider_select_timeout', 180);
                if(!empty($IncomingRequests)){
                    for ($i=0; $i < sizeof($IncomingRequests); $i++) {
                        $IncomingRequests[$i]->time_left_to_respond = $Timeout - (time() - strtotime($IncomingRequests[$i]->request->assigned_at));
						
						if($IncomingRequests[$i]->request->schedule_at == NULL)	
						{	
						$ddate = date('d-m-Y', strtotime($IncomingRequests[$i]->request->created_at));
						$ttime = date("H:i A",strtotime($IncomingRequests[$i]->request->created_at));
						}
						else
						{
						$ddate = date('d-m-Y', strtotime($IncomingRequests[$i]->request->schedule_at));
						$ttime = date("H:i A",strtotime($IncomingRequests[$i]->request->schedule_at));
						}	

                        if($IncomingRequests[$i]->request->status == 'SEARCHING' && $IncomingRequests[$i]->time_left_to_respond < 0 && $IncomingRequests[$i]->request->broad_cast == "NO" ) {
                            $this->assign_next_provider($IncomingRequests[$i]->request->id);
							
							$User = User::find($IncomingRequests[$i]->request->user_id);

							if($User){
                                $TextMsg = urlencode("ID: ".$IncomingRequests[$i]->request->booking_id." from ".$IncomingRequests[$i]->request->s_address." to ".$IncomingRequests[$i]->request->d_address." at ".$ddate." ".$ttime." is confirmed. Fare Rs.".$IncomingRequests[$i]->request->estimated_fare.".Toll Included. Preferred mode of payment : ".$IncomingRequests[$i]->request->payment_mode."  call us 080-41553311 www.wegacabs.com");
                                
								$MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";

								$json = curl($MessageUrl);

                                $MessageUrl = json_decode($json, TRUE);
                                
                               
                            } 
							
                        }else if($IncomingRequests[$i]->request->status == 'SEARCHING' && $IncomingRequests[$i]->time_left_to_respond < 0 && $IncomingRequests[$i]->request->broad_cast == "YES" ) {

                            $User = User::find($IncomingRequests[$i]->request->user_id);

                            if($User){
                                $TextMsg = urlencode("ID: ".$IncomingRequests[$i]->request->booking_id." from ".$IncomingRequests[$i]->request->s_address." to ".$IncomingRequests[$i]->request->d_address." at ".$ddate." ".$ttime." is confirmed. Fare Rs.".$IncomingRequests[$i]->request->estimated_fare.".Toll Included. Preferred mode of payment : ".$IncomingRequests[$i]->request->payment_mode."  call us 080-41553311 www.wegacabs.com");
                                
                                /* $MessageUrl =  "https://control.msg91.com/api/sendhttp.php?authkey=".env('MSG91KEY')."&mobiles=".$User->mobile."&message=".$TextMsg."&sender=".env('MSGSENDERID')."&route=4&country=91"; */
								
								$MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";

								Log::info("MessageUrl:".$MessageUrl);
								
								$json = curl($MessageUrl);

                                $MessageUrl = json_decode($json, TRUE);
                                
                                /* \Mail::to($User)->send(new RequestUserBookingReminder($IncomingRequests[$i]->request->id)); */
                            }  

                            //UserRequests::where('id', $IncomingRequests[$i]->request->id)->update(['status' => 'CANCELLED']);
                            // No longer need request specific rows from RequestMeta
                            //RequestFilter::where('request_id', $IncomingRequests[$i]->request->id)->delete();
                            // request push to user provider not available
                            //(new SendPushNotification)->ProviderNotAvailable($IncomingRequests[$i]->request->user_id);
                        }
						
						$IncomingRequests[$i]->request->distance_calculation =null;	
						$IncomingRequests[$i]->request->latlong_calculation =null;
						
                    }
                }


            $Response = [
                    'account_status' => $Provider->status,
                    'service_status' => $Provider->service ? Auth::user()->service->status : 'offline',
                    'requests' => $IncomingRequests,
                ];

            return $Response;
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
    }

    /**
     * Calculate distance between two coordinates.
     * 
     * @return \Illuminate\Http\Response
     */

    public function calculate_distance(Request $request, $id){
        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric'
            ]);
        try{

            if($request->ajax()) {
                $Provider = Auth::user();
            } else {
                $Provider = Auth::guard('provider')->user();
            }

            $UserRequest = UserRequests::where('status','PICKEDUP')
                            ->where('provider_id',$Provider->id)
                            ->find($id);

            if($UserRequest && ($request->latitude && $request->longitude)){

                //Log::info("REQUEST ID:".$UserRequest->id."==SOURCE LATITUDE:".$UserRequest->travel_latitude."==SOURCE LONGITUDE:".$UserRequest->travel_longitude);
            
			//Log::info("REQUEST ID:".$UserRequest->id."Travel LATITUDE:".$request->latitude."Travel LONGITUDE:".$request->longitude);
			
                if($UserRequest->travel_latitude !=0.00000000 && $UserRequest->travel_longitude !=0.00000000){
                    
					if(($request->latitude != 0.00000000) && ($request->longitude != 0.00000000))
						{
                    $coordinate1 = new Coordinate($UserRequest->travel_latitude, $UserRequest->travel_longitude); /** Set Distance Calculation Source Coordinates ****/
                    $coordinate2 = new Coordinate($request->latitude, $request->longitude); /** Set Distance calculation Destination Coordinates ****/

                    $calculator = new Vincenty();

                    /***Distance between two coordinates using spherical algorithm (library as mjaschen/phpgeo) ***/ 

                    $mydistance = $calculator->getDistance($coordinate1, $coordinate2); 

                    $meters = round($mydistance);

                    //Log::info("REQUEST ID:".$UserRequest->id."==BETWEEN TWO COORDINATES DISTANCE:".$meters." (m)");
		
                 //   if($meters >= 10){
                        /*** If traveled distance riched houndred meters means to be the source coordinates ***/
						
						
                        $traveldistance = round(($meters/1000),8);
                       // $traveldistance = round(($meters/1000),8);
						
					//	$calulatedistance = $UserRequest->travel_distance + $traveldistance;
						
						$multidimentional_array =  array();
						$w_array = array();
						
			if($UserRequest->distance_calculation !='')
			{ 
					$multidimentional_array  = json_decode($UserRequest->distance_calculation,true);
			}  
			if($UserRequest->latlong_calculation !='')
			{ 
					$w_array  = json_decode($UserRequest->latlong_calculation,true);
			}
			$calulatedistance = 0;
			$z =array();
			if(count($w_array) > 1)
			{
				$d = array_chunk($w_array,100,true);
				$h = count($d);
				$r = 0;
			for ($x1 = 0; $x1 < $h; $x1++) 
			{
				$output = array_slice($w_array, $r, 100, true);
				///////////////
				
				$firstKey = array_keys(array_slice($output, 0, 1, TRUE))[0];
				$last_key = key( array_slice( $output, -1, 1, TRUE ) );
				
				$details1 = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$firstKey.",".$output[$firstKey]."&destinations=".$last_key.",".$output[$last_key]."&mode=driving&sensor=false&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";	

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$details1);
				$json=curl_exec($ch);
				curl_close($ch);
				
				$details1 = json_decode($json, TRUE);
				$meter = $details1['rows'][0]['elements'][0]['distance']['value'];
				$z1 = round(($meter/1000),8); 
				$calulatedistance = $calulatedistance + $z1;
				$r =$r+99;
				
				///////////////
				
				/* $a =  str_replace('=', ',', http_build_query($output, null, '|'));
				$q ="https://roads.googleapis.com/v1/snapToRoads?path=".$a."&interpolate=true&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";	
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$q);
				$json=curl_exec($ch);
				curl_close($ch);
				
				$details = json_decode($json, TRUE);
				$snap =  count(@$details['snappedPoints'])-1;
				
				if(count(@$details['snappedPoints']) > 1)
				{
				$y =array();
				$z =array();
				for ($x = 0; $x <= $snap; $x++) 
				{
					$lat =  $details['snappedPoints'][$x]['location']['latitude'];
					$long =  $details['snappedPoints'][$x]['location']['longitude'];
					$y[(string)$lat] =$long ;
				}
				$qw =  count($y)-1;
				for ($x = 0; $x < $qw; $x++) 
				{
					$travelled = new Coordinate(array_keys($y)[$x], array_values($y)[$x]); 
					$expected = new Coordinate(array_keys($y)[$x+1], array_values($y)[$x+1]); 
					$calculator1 = new Vincenty();
					$mydistance1 = $calculator1->getDistance($travelled, $expected); 
					$meters1 = $mydistance1;
					$traveldistance1 = round(($meters1/1000),8);
					
					//echo array_keys($y)[$x] .",".array_values($y)[$x]."\r\n";
					//echo array_keys($y)[$x+1] .",".array_values($y)[$x+1]."\r\n";
					$z[] =  $traveldistance1;
				}
			//	$calulatedistance = $calulatedistance + array_sum($z);
			//	$r =$r+90;
				}
				else{
					$qw =  count($output)-1;
					for ($x = 0; $x < $qw; $x++) 
					{
						$travelled = new Coordinate(array_keys($output)[$x], array_values($output)[$x]); 
						$expected = new Coordinate(array_keys($output)[$x+1], array_values($output)[$x+1]); 
						$calculator1 = new Vincenty();
						$mydistance1 = $calculator1->getDistance($travelled, $expected); 
						$meters1 = $mydistance1;
						$traveldistance1 = round(($meters1/1000),8); 
						$z[] =  $traveldistance1;
					}
			//		$calulatedistance =  $calulatedistance + array_sum($z);
			//		$r =$r+90;
				} */
				
				}
			}
			
			
			if(count($multidimentional_array) > 1)
			{	
			$last_beforekey = key( array_slice( $multidimentional_array, -2, 1, TRUE ) );
			$last_key = key( array_slice( $multidimentional_array, -1, 1, TRUE ) );		
			$timeFirst  = strtotime($last_beforekey);
			$timeSecond = strtotime($last_key);
			$differenceInSeconds = $timeSecond - $timeFirst;
			Log::info("differenceInSeconds:".$differenceInSeconds);
			Log::info("travel distance:".$traveldistance);
			if($differenceInSeconds < 9)
			{
				//unset($multidimentional_array[count($multidimentional_array)-1]);
				unset($multidimentional_array[$last_key]);
			}elseif(($differenceInSeconds < 12) && (end($multidimentional_array) > 0.3))
			{
				unset($multidimentional_array[$last_key]);
			}
			
			
		}
		
				$date = date('Y-m-d h:i:s', time());
				$multidimentional_array[$date] = $traveldistance;
	//			$calulatedistance = array_sum($multidimentional_array);
				$check_sum = array_sum($multidimentional_array);
				$multidimentional_array=json_encode($multidimentional_array);
					
				$UserRequest->distance_calculation = $multidimentional_array;
						
				$w_array[(string)$request->latitude] = (string)$request->longitude;
				$w_array=json_encode($w_array);
				$UserRequest->latlong_calculation = $w_array;
				
				Log::info("REQUEST ID:".$UserRequest->id."Travel LATITUDE:".$request->latitude."Travel LONGITUDE:".$request->longitude."Travel Distance:".$calulatedistance);
				Log::info("check_sum:".$check_sum);
				Log::info("count w_array:".count($w_array));
				Log::info("count multidimentional_array:".count($multidimentional_array));
				
                       // $UserRequest->travel_distance = $calulatedistance;
                        $UserRequest->distance        = $calulatedistance;
                        $UserRequest->travel_latitude = $request->latitude;
                        $UserRequest->travel_longitude= $request->longitude;
                     //   $UserRequest->d_latitude      = $request->latitude;
                     //   $UserRequest->d_longitude     = $request->longitude;
                        $UserRequest->save();
				//		}
                    }
                }else if(($UserRequest->travel_latitude ==0.00000000 && $UserRequest->travel_longitude ==0.00000000) && ($request->latitude && $request->longitude)) {
                    $UserRequest->distance             = 0;
                    $UserRequest->travel_latitude      = $request->latitude;
                    $UserRequest->travel_longitude     = $request->longitude;
                    $UserRequest->save();
                }
            }
            return $UserRequest;
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }

    }

    /**
     * Cancel given request.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request)
    {
        $this->validate($request, [
            'cancel_reason'=> 'max:255',
        ]);
        try{

            $UserRequest = UserRequests::findOrFail($request->id);
            $Cancellable = ['SEARCHING', 'ACCEPTED', 'ARRIVED', 'STARTED', 'CREATED','SCHEDULED'];

            if(!in_array($UserRequest->status, $Cancellable)) {
                return back()->with(['flash_error' => 'Cannot cancel request at this stage!']);
            }

			if($UserRequest->schedule_at == NULL)	
			{
				$UserRequest->status = "CANCELLED";
				$UserRequest->cancel_reason = $request->cancel_reason;
				$UserRequest->cancelled_by = "PROVIDER";
			}
			else
			{
				 $UserRequest->status = "SCHEDULED";
				 $UserRequest->provider_id = 0;
			}
            
            $UserRequest->save();

             RequestFilter::where('request_id', $UserRequest->id)->delete();

             ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);

			 $User = User::find($UserRequest->user_id);
				if($User){
					$TextMsg = urlencode("Dear ".$User->first_name." ".$User->last_name.", Driver has cancelled your Ride. Your Booking ID : ".$UserRequest->booking_id."");
					
				   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
					$json = curl($MessageUrl);
					$MessageUrl = json_decode($json, TRUE);
				}
				
				
             // Send Push Notification to User
            (new SendPushNotification)->ProviderCancellRide($UserRequest);

            return $UserRequest;

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rate(Request $request, $id)
    {

        $this->validate($request, [
                'rating' => 'required|integer|in:1,2,3,4,5',
                'comment' => 'max:255',
            ]);
    
        try {

            $UserRequest = UserRequests::where('id', $id)
                ->where('status', 'COMPLETED')
                ->firstOrFail();

            if($UserRequest->rating == null) {
                UserRequestRating::create([
                        'provider_id' => $UserRequest->provider_id,
                        'user_id' => $UserRequest->user_id,
                        'request_id' => $UserRequest->id,
                        'provider_rating' => $request->rating,
                        'provider_comment' => $request->comment,
                    ]);
            } else {
                $UserRequest->rating->update([
                        'provider_rating' => $request->rating,
                        'provider_comment' => $request->comment,
                    ]);
            }

            $UserRequest->update(['provider_rated' => 1]);

            // Delete from filter so that it doesn't show up in status checks.
            RequestFilter::where('request_id', $id)->delete();

            ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);

            // Send Push Notification to Provider 
            $average = UserRequestRating::where('provider_id', $UserRequest->provider_id)->avg('provider_rating');

            $UserRequest->user->update(['rating' => $average]);

			$Provider = Provider::find($UserRequest->provider_id);
			$pay = UserRequestPayment::where('request_id',$id)->first();
			if($UserRequest->schedule_at == NULL)	
			{	
			$ddate = date('d-m-Y', strtotime($UserRequest->created_at));
			$ttime = date("H:i A",strtotime($UserRequest->created_at));
			}
			else
			{
			$ddate = date('d-m-Y', strtotime($UserRequest->schedule_at));
			$ttime = date("H:i A",strtotime($UserRequest->schedule_at));
			}
			
			if($Provider)
			{
			   $TextMsg = urlencode("Trip Fare : ".$pay->total." for the Booking ID: ".$UserRequest->booking_id.", PickUpArea: ".$UserRequest->s_address.", PickUp Time: ".$ddate." ".$ttime.", Drop Area: ".$UserRequest->d_address.", Commission deducted for this booking is Rs.".$pay->commision.", your current balance after deducting commission is Rs.".$Provider->wallet_amount."/- In case of any confusion please call - 9036364433 (24/7). www.wegacabs.com");
				
			   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$Provider->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
				$json = curl($MessageUrl);
				$MessageUrl = json_decode($json, TRUE);
			}
				
            return response()->json(['message' => 'Request Completed!']);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Request not yet completed!'], 500);
        }
    }

    /**
     * Get the trip history of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function scheduled(Request $request)
    {
        
        try{

            $Jobs = UserRequests::where('provider_id', Auth::user()->id)
                    ->where('status', 'SCHEDULED')
                    ->with('service_type')
                    ->get();

            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }

            return $Jobs;
            
        } catch(Exception $e) {
            return response()->json(['error' => "Something Went Wrong"]);
        }
    }

	
	public function unassigned_schedule(Request $request)
    {
        
        try{

		$current_time = Carbon::now();
            $Jobs = UserRequests::where('provider_id', 0)
                    ->where('status', 'SCHEDULED')
					->where('schedule_at', '>', $current_time)
                    ->with('service_type')
                    ->get();

            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }

            return $Jobs;
            
        } catch(Exception $e) {
            return response()->json(['error' => "Something Went Wrong"]);
        }
    }
	
    /**
     * Get the trip history of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        if($request->ajax()) {

            $Jobs = UserRequests::where('provider_id', Auth::user()->id)
                    ->where('status', 'COMPLETED')
                    ->orderBy('created_at','desc')
                    ->with('payment')
                    ->get();

            if(!empty($Jobs)){
                $map_icon = asset('asset/marker.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $Jobs;
        }
        $Jobs = UserRequests::where('provider_id', Auth::guard('provider')->user()->id)->with('user', 'service_type', 'payment', 'rating')->get();
        return view('provider.trip.index', compact('Jobs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
	 /* public function provider_rides() {
    
        try{
			$Rides = UserRequests::where('status','COMPLETED')
                            ->where('provider_id', Auth::user()->id)
							->where('created_at', '>=', Carbon::today())
                            ->count();
							
			$last_ride = UserRequests::where('status','COMPLETED')
                            ->where('provider_id', Auth::user()->id)
							->where('created_at', '>=', Carbon::today())
							->orderBy('id','desc')
                            ->first();
							
							
			$revenue = UserRequestPayment::whereHas('request', function($query) use ($request) {
                                $query->where('provider_id', Auth::user()->id);
                                $query->where('created_at', '>=', Carbon::today());
                            })
                        ->sum('total');
			
			$rating = UserRequestRating::whereHas('request', function($query) use ($request) {
                                $query->where('provider_id', Auth::user()->id);
                                $query->whereDate('created_at', Carbon::today());
                                $query->max('user_rating');
                            })
                        ->first();
			
			if($rating)
			{
				$user = User::find($rating->user_id);
			}	 
				

 
			return response()->json([
                    'rides' => $Checkrides, 
                    'revenue' => $revenue,
                    'rating' => $rating->user_rating,
                    'user' => $user ? $user : '',
                ]);
			
			}

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    } */
	
	
	 
	 
    public function accept(Request $request, $id)
    {
		
		Log::info('ACCEPT', $request->all());
		Log::info('ACCEPT REQUEST ID:'. $id);
		
        try {

            $UserRequest = UserRequests::findOrFail($id);

            if(($UserRequest->status != "SEARCHING") && ($UserRequest->status != "SCHEDULED") && ($UserRequest->provider_id != 0)) {
                return response()->json(['error' => 'Request already under progress!']);
            }
            
            $UserRequest->provider_id = Auth::user()->id;
            $UserRequest->current_provider_id = Auth::user()->id;

            if($UserRequest->schedule_at != ""){

                $beforeschedule_time = strtotime($UserRequest->schedule_at."- 1 hour");
                $afterschedule_time = strtotime($UserRequest->schedule_at."+ 1 hour");

                $CheckScheduling = UserRequests::where('status','SCHEDULED')
                            ->where('provider_id', Auth::user()->id)
                            ->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
                            ->count();

                if($CheckScheduling > 0 ){
                    if($request->ajax()) {
                        return response()->json(['error' => trans('api.ride.request_already_scheduled')]);
                    }else{
                        return redirect('dashboard')->with('flash_error', 'If the ride is already scheduled then we cannot schedule/request another ride for the after 1 hour or before 1 hour');
                    }
                }

				$ch = RequestFilter::where('request_id',$UserRequest->id)
                            ->where('provider_id', Auth::user()->id)
							->count();
				
				if($ch > 0){
					RequestFilter::where('request_id',$UserRequest->id)->where('provider_id',Auth::user()->id)->update(['status' => 2]);
				}
				else{
					$Filter = new RequestFilter;
					$Filter->request_id = $UserRequest->id;
					$Filter->provider_id = Auth::user()->id;
					$Filter->status = 2;
					$Filter->save();
				}
                

                $UserRequest->status = "SCHEDULED";
                $UserRequest->save();

            }else{


                $UserRequest->status = "STARTED";
                $UserRequest->accepted_at = Carbon::now();
                $UserRequest->save();


                ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'riding']);

                $Filters = RequestFilter::where('request_id', $UserRequest->id)->where('provider_id', '!=', Auth::user()->id)->get();
                // dd($Filters->toArray());
                foreach ($Filters as $Filter) {
                    $Filter->delete();
                }
            }

            $UnwantedRequest = RequestFilter::where('request_id','!=' ,$UserRequest->id)
                                ->where('provider_id',Auth::user()->id )
                                ->whereHas('request', function($query){
                                    $query->where('status','<>','SCHEDULED');
                                });

            if($UnwantedRequest->count() > 0){
                $UnwantedRequest->delete();
            }  

            // Send Push Notification to User
            (new SendPushNotification)->RideAccepted($UserRequest);

            $User = User::find($UserRequest->user_id);

            $Provider = Provider::find($UserRequest->provider_id);
            
			$ProviderService1 = ProviderService::where('provider_id',$Provider->id)->first();
			
			$car = CarCategory::find($ProviderService1->car_categories_id);
			$servicetype = ServiceType::findOrFail($UserRequest->service_type_id);
            if($User){
                $TextMsg = urlencode("ID: ".$UserRequest->booking_id." Driver Details: ".$Provider->first_name." ".$Provider->last_name.", ".$Provider->mobile.", Vehicle Number: ".$ProviderService1->service_number.", CarType: ".$servicetype->name.". Preferred Mode of Payment : ".$UserRequest->payment_mode." Call us 080-41553311 www.wegacabs.com");
                
				$MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
				
				$json = curl($MessageUrl);

                $MessageUrl = json_decode($json, TRUE);
                
				
				if($UserRequest->schedule_at == NULL)	
			{	
			$ddate = date('d-m-Y', strtotime($UserRequest->created_at));
			$ttime = date("H:i A",strtotime($UserRequest->created_at));
			}
			else
			{
			$ddate = date('d-m-Y', strtotime($UserRequest->schedule_at));
			$ttime = date("H:i A",strtotime($UserRequest->schedule_at));
			}
				$TextMsg1 = urlencode("ID: ".$UserRequest->booking_id." Customer Details: ".$User->first_name." ".$User->last_name.", ".$User->mobile.", PickUpArea: ".$UserRequest->s_address.", PickUp Time: ".$ddate." ".$ttime.", Drop Area: ".$UserRequest->d_address.", Collect Fare: ".$UserRequest->estimated_fare.". Toll Included  Call us 080-41553311. www.wegacabs.com");
				$MessageUrl1 ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$Provider->mobile."&sid=WEGACB&msg=".$TextMsg1."&fl=0&gwid=2";
				
				$json1 = curl($MessageUrl1);

                $MessageUrl1 = json_decode($json1, TRUE);
				
                Mail::to($User)->send(new RequestUserBookingReminder($UserRequest->id));
            }  

            return $UserRequest;

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to accept, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }
    }

	
	public function get_wallet_balance()
	{
		//Log::info('Wallet Amt');
		try
		{
			$Provider = Provider::find(Auth::user()->id);
			/* if($Provider->wallet_amount <=500)
			{
				(new SendPushNotification)->WalletLimit($Provider->id);
				//Provider::where('id',$Provider->id)->update(['status' => 'banned']);
				ProviderService::where('provider_id',$Provider->id)->update(['status' =>'offine']);
			} */
			return response()->json(['wallet_amount' => $Provider->wallet_amount]);
				
		}catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to get Wallet Balance']);
        }
	}
	
	
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

	Log::info('UPDATE', $request->all());
	Log::info('UPDATE REQUEST ID:'. $id);
        $this->validate($request, [
              'status' => 'required|in:ACCEPTED,STARTED,ARRIVED,PICKEDUP,DROPPED,PAYMENT,COMPLETED',
			  'mcd_tax'=>'',
			  'state_tax'=>'',
			  'toll_tax'=>'',
			  'parking_tax'=>'',
			  'airport_tax'=>'',
			  'start_meter'=>'',
			  'end_meter'=>'',
			  'days'=>'',
           ]);

        try{

            $UserRequest = UserRequests::findOrFail($id);

			if($request->has('mcd_tax')){
				$UserRequest->mcd_tax = $request->mcd_tax;
			}
			if($request->has('state_tax')){
				$UserRequest->state_tax = $request->state_tax;
			}
			if($request->has('toll_tax')){
				$UserRequest->toll_tax = $request->toll_tax;
			}
			if($request->has('parking_tax')){
				$UserRequest->parking_tax = $request->parking_tax;
			}
			if($request->has('airport_tax')){
				$UserRequest->airport_tax = $request->airport_tax;
			}
			
			if($request->has('start_meter')){
				$UserRequest->start_meter = $request->start_meter;
			}
			
			if($request->has('end_meter')){
				$UserRequest->end_meter = $request->end_meter;
			}
			
			if($request->has('days')){
				$UserRequest->days = $request->days;
			}
			
            if($request->status == 'DROPPED' && $UserRequest->payment_mode != 'CASH') {
                $UserRequest->status = 'COMPLETED';
				
			} else if ($request->status == 'COMPLETED' && $UserRequest->payment_mode == 'CASH') {
                $UserRequest->status = $request->status;
                $UserRequest->paid = 1;
				
				$pay = UserRequestPayment::where('request_id',$id)->first();
				
				$Provider = Provider::find($UserRequest->provider_id);
				$Provider->wallet_amount  = $Provider->wallet_amount - $pay->commision;
				$Provider->save();
                ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);
				
				
				$Payment = new ProviderPayment;
				$Payment->provider_id = $Provider->id;
				$Payment->txnid = "";
				$Payment->amount = $pay->commision;
				$Payment->type = "WALLET_DEBIT";
				$Payment->save();
			
			
				// charged wallet money push 
				$t = currency($pay->commision).' '.trans('api.push.charged_from_wallet').'. Current Balance:'. currency($Provider->wallet_amount);
				
				$User = User::find($UserRequest->user_id);
				if($User){
				   $TextMsg = urlencode("Thanks for using wegacabs, Trip Fare : Rs.".$pay->total." for the Booking ID: ".$UserRequest->booking_id.". Please visit our website or call 080-41553311. www.wegacabs.com");
					
				   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
					$json = curl($MessageUrl);
					$MessageUrl = json_decode($json, TRUE);
				}
				
				if($Provider)
				{
				   $TextMsg = urlencode("Trip Fare : Rs.".$pay->total." for the Booking ID: ".$UserRequest->booking_id.", 15 percent commission deducted for this booking, In case of any details please call - 080-41553311 (24/7). www.wegacabs.com");
					
				   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$Provider->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
					$json = curl($MessageUrl);
					$MessageUrl = json_decode($json, TRUE);
				}
				
				(new SendPushNotification)->ChargedWalletMoney_Provider($UserRequest->provider_id,$t);
			
            } else {
                $UserRequest->status = $request->status;
                if($request->status == 'ARRIVED'){
                    $UserRequest->arrived_at=Carbon::now();
					
				$User = User::find($UserRequest->user_id);
				if($User){
					$TextMsg = urlencode("Dear ".$User->first_name." ".$User->last_name.", Thanks for Booking, Driver has arrived at your location. Your Booking ID : ".$UserRequest->booking_id."");
					
				   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
					$json = curl($MessageUrl);
					$MessageUrl = json_decode($json, TRUE);
				} 
							
							
                    (new SendPushNotification)->Arrived($UserRequest);
                }
            }
            if($request->status == 'PICKEDUP'){
                $UserRequest->distance  = 0;
                $UserRequest->started_at=Carbon::now();
            }

            $UserRequest->save();

            if($request->status == 'DROPPED') {
                $User = User::where('id',$UserRequest->user->id)->first();
                ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);

                $UserRequest->finished_at = Carbon::now();
			//	$UserRequest->d_latitude =  $request->latitude?:$UserRequest->d_latitude;
            //    $UserRequest->d_longitude =  $request->longitude?:$UserRequest->d_longitude;
			
			$travelled = new Coordinate($UserRequest->travel_latitude, $UserRequest->travel_longitude); 
        $expected = new Coordinate($UserRequest->d_latitude, $UserRequest->d_longitude); 
		$calculator1 = new Vincenty();
		$mydistance1 = $calculator1->getDistance($travelled, $expected); 
		$meters1 = round($mydistance1);
		$traveldistance1 = 	round(($meters1/1000),8);
	
		/* if($traveldistance1 < 1)
		{
			$UserRequest->distance = $UserRequest->travel_distance;
		} */
		
		$UserRequest->d_address =  $request->address?:$UserRequest->d_address;
				
		//////////new distance calculation
		/* $jsondata = $UserRequest->latlong_calculation;
		$w_array  = json_decode($jsondata,true); */
		//$w_array[$UserRequest->travel_latitude] = $UserRequest->travel_longitude;
		//$w_array[$UserRequest->travel_latitude] = $UserRequest->travel_longitude;
		//$w_array=array($UserRequest->s_latitude=>$UserRequest->s_longitude) + $w_array;
		
		$Provider = Provider::find($UserRequest->provider_id);
		$jsondata = $UserRequest->latlong_calculation;
		$w_array  = json_decode($jsondata,true);

		if($Provider->latitude !=0.00000000 && $Provider->longitude !=0.00000000){
		$w_array[(string)$Provider->latitude] = (string)$Provider->longitude;
		$w_array=json_encode($w_array);
		$UserRequest->latlong_calculation = $w_array;
				
		$jsondata = $UserRequest->latlong_calculation;
		$w_array  = json_decode($jsondata,true);
		}
		
			$calulatedistance = 0;
			$z =array();
			 if(count($w_array) > 1)
			{
				$d = array_chunk($w_array,100,true);
				$h = count($d);
				$r = 0;
			for ($x1 = 0; $x1 < $h; $x1++) 
			{
				$output = array_slice($w_array, $r, 100, true);
				$firstKey = array_keys(array_slice($output, 0, 1, TRUE))[0];
				$last_key = key( array_slice( $output, -1, 1, TRUE ) );
		
				$details1 = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$firstKey.",".$output[$firstKey]."&destinations=".$last_key.",".$output[$last_key]."&mode=driving&sensor=false&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL,$details1);
				$json=curl_exec($ch);
				curl_close($ch);
				
				$details12 = json_decode($json, TRUE);
				
				Log::info("Distance Matrix Status dropped: ".$details12['status']);
				Log::info("Distance Matrix URL: ".$details1);
				
				if($details12['status'] == "OK"){
				$meter = $details12['rows'][0]['elements'][0]['distance']['value'];
				}
				else{
					$travelled5 = new Coordinate($firstKey, $output[$firstKey]); 
					$expected5 = new Coordinate($last_key, $output[$last_key]); 
					$calculator5 = new Vincenty();
					$mydistance5 = $calculator5->getDistance($travelled5, $expected5); 
					$meter = round($mydistance5);
				}	
				$z1 = round(($meter/1000),8); 
				$calulatedistance = $calulatedistance + $z1;
				$r =$r+99;
			}
			}
			
			$UserRequest->calculated_distance =  $calulatedistance;
			$UserRequest->distance =  $calulatedistance;
			
			
		
		/////////
		$df = abs($UserRequest->travel_distance - $calulatedistance);
		
		if(($df <= 2) || ($traveldistance1 <= 2))
		{
			$UserRequest->distance = $UserRequest->travel_distance;
		}
		
                $UserRequest->save();
                $UserRequest->with('user')->findOrFail($id);
                $UserRequest->invoice = $this->invoice($id);
			 
				/* if($this->invoice($id) != null) {
					
					$pays = UserRequestPayment::where('request_id', $id)->count();
					if($pays > 0)
					{
						Log::info("invoice Not empty loop1");
						$UserRequest->invoice = $this->invoice($id);
					}
					else{
						Log::info("invoice Not empty l0op2");
						$UserRequest->invoice = $this->invoice1($id);
					}
					 
					Log::info("invoice Not empty");
				}
				else {
					
					$pays = UserRequestPayment::where('request_id', $id)->count();
					if($pays > 0)
					{
						Log::info("invoice empty loop1");
						$UserRequest->invoice = $this->invoice($id);
					}
					else{
						$UserRequest->invoice = $this->invoice1($id);
						Log::info("invoice empty loop1");
					}
					Log::info("invoice empty");
				} */
				
				
                if($User !=""){
                   \Mail::to($User)->send(new RequestUserInvoiceReminder($UserRequest->id));
                }  
            }
            // Send Push Notification to User
       
			if(!empty($UserRequest)){
					$UserRequest->distance_calculation =null;	
					$UserRequest->latlong_calculation =null;
			} 
			Log::info('UserRequest', array($UserRequest));
			
            return $UserRequest;

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to update, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $UserRequest = UserRequests::find($id);

        try {
            if($UserRequest->broad_cast == "NO"){
               $this->assign_next_provider($UserRequest->id); 
            }
            return $UserRequest->with('user')->get();

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to reject, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }
    }

    public function assign_next_provider($request_id) {

        try {
            $UserRequest = UserRequests::findOrFail($request_id);
        } catch (ModelNotFoundException $e) {
            // Cancelled between update.
            return false;
        }

        $RequestFilter = RequestFilter::where('provider_id', $UserRequest->current_provider_id)
            ->where('request_id', $UserRequest->id)
            ->delete();

        try {

            $next_provider = RequestFilter::where('request_id', $UserRequest->id)
                            ->orderBy('id','asc')
                            ->firstOrFail();

            $UserRequest->current_provider_id = $next_provider->provider_id;
            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->save();

            // incoming request push to provider
            (new SendPushNotification)->IncomingRequest($next_provider->provider_id);
            
        } catch (ModelNotFoundException $e) {

			if(($UserRequest->schedule_at =="") && ($UserRequest->user_started_at =="")){
            UserRequests::where('id', $UserRequest->id)->update(['status' => 'CANCELLED']);
			}
			else{
			UserRequests::where('id', $UserRequest->id)->update(['status' => 'SCHEDULED']);	
			/* UserRequests::where('id', $UserRequest->id)->update([
			'status' => 'SEARCHING',
			'current_provider_id' => 0,
            ]); */
			
			}

            // No longer need request specific rows from RequestMeta
            RequestFilter::where('request_id', $UserRequest->id)->delete();

            //  request push to user provider not available
            (new SendPushNotification)->ProviderNotAvailable($UserRequest->user_id);
        }
    }

     /**
     * Get the trip invocie
     *
     * @return \Illuminate\Http\Response
     */

    public function invoice($request_id)
    {
        try {
            $UserRequest = UserRequests::findOrFail($request_id);
            
			UserRequestPayment::where('request_id', $request_id)->delete();
			
            $Fixed  = 0;
            $Distance = 0;
            $Discount = 0; // Promo Code discounts should be added here.
            $Minutes = 0; // Promo Code discounts should be added here.
            $DateFees = 0;
            $TravelDay = 0;
            $Connection = $UserRequest->service_type->connection ? : 0;
			$cancelfee = 0;

			$mcd_tax = 0;
			$state_tax = 0;
			$toll_tax = 0;
			$parking_tax = 0;
			$airport_tax = 0;
			
			$start_meter = 0;
			$end_meter = 0;
			$days = 0;
			
			$StartedDate  = date_create($UserRequest->started_at);
            $FinisedDate  = date_create($UserRequest->finished_at);
         //   $TraveledTime = round((strtotime($UserRequest->finished_at) - strtotime($UserRequest->started_at))/3600, 1);
			
            $TimeInterval = date_diff($StartedDate,$FinisedDate);
			$TraveledTime = $TimeInterval->h * 60;
            $TraveledTime += $TimeInterval->i;
			
			
			$mcd_tax = $UserRequest->mcd_tax;
			$state_tax = $UserRequest->state_tax;
			$toll_tax = $UserRequest->toll_tax;
			$parking_tax = $UserRequest->parking_tax;
			$airport_tax = $UserRequest->airport_tax;
				
			$start_meter = $UserRequest->start_meter;
			$end_meter   = $UserRequest->end_meter;
			$days        = $UserRequest->days;
            //$TimeInterval = date_diff($StartedDate,$FinisedDate);
           // $TraveledTime = $TimeInterval->i;
            if($UserRequest->ride_in  == 'INTER'){
               $Fixed = $UserRequest->service_type->fixed ? : 0;
			   
			   $TravelDistance = (round($UserRequest->distance,2));
               if($TravelDistance <= $UserRequest->service_type->distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->distance;
				}
				
			   $Distance = $TravelDistance1 * $UserRequest->service_type->price;
				
			   $DateFees  = floor ($TraveledTime / 1440);
				if($DateFees > 1)
				{
					$DateFees  = $DateFees  * $UserRequest->service_type->day;
				}			
				else{
                  $DateFees = 0;
               }
               $TravelDay = $DateFees;
			   
			   $Minutes  = $UserRequest->service_type->minute * $TraveledTime;
			   
			   $user = User::findOrFail($UserRequest->user_id);
			   if($user->pending_cancel_fee > 0)
			   {
				   $cancelfee = $user->pending_cancel_fee;
			   }
			   
            }else if($UserRequest->ride_in  == 'OUTER' && $UserRequest->ride_way == 'ONEROUND'){
               $Fixed = $UserRequest->service_type->outer_fixed? : 0;
               $TravelDistance = $end_meter - $start_meter;
			   $TravelDistance = $TravelDistance * 2;
               if($TravelDistance <= $UserRequest->service_type->outer_distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->outer_distance;
				}
				
               $Distance  = $TravelDistance1 * $UserRequest->service_type->outer_price;
               $TravelDay = $days  * $UserRequest->service_type->day;
			   $DateFees = $TravelDay;
               
            }else if($UserRequest->ride_in  == 'OUTER' && ($UserRequest->ride_way == 'ONE' || $UserRequest->ride_way == 'ROUND') ){
              $Fixed = $UserRequest->service_type->outer_fixed? : 0;
               $TravelDistance = $end_meter - $start_meter;
			  
			  if($TravelDistance <= $UserRequest->service_type->outer_distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->outer_distance;
				}
				
               $Distance  = $TravelDistance1 * $UserRequest->service_type->outer_price;
               $TravelDay = $days  * $UserRequest->service_type->day;
               $DateFees = $TravelDay;
			   
            }else if($UserRequest->ride_in  == 'RENTAL'){
				if($UserRequest->package != '')
				{
					$rental = Rental::where('service_type',$UserRequest->service_type_id)
							->where('hours',$UserRequest->package)
							->first();
					if($rental)
				{		
					$Fixed = $rental->amount;
					$TravelDistance = (round($UserRequest->distance,2));
					if($TravelDistance <= $rental->kms){
						$TravelDistance1 = 0;
					}
					else{
						$TravelDistance1 = $TravelDistance - $rental->kms;
					}
					$Distance = $TravelDistance1 * $rental->extra_km;
					
					$Hours = $TraveledTime/60;
					if($Hours <= $rental->hours)
					{
						$Hours1 = 0;
					}
					else{
						$Hours1 = $Hours - $rental->hours;
					}
					$Minutes += (ceil($Hours1) * $rental->extra_hours);
				}
				}
				if($UserRequest->airport == 1)
				{
					$Fixed = $UserRequest->service_type->airport;
				}
			}

            $UserRequest->traveled_time = $TraveledTime;
            $UserRequest->travel_day = $TravelDay;
            $UserRequest->save();

            if($PromocodeUsage = PromocodeUsage::where('user_id',$UserRequest->user_id)->where('status','ADDED')->first()){
                if($Promocode = Promocode::find($PromocodeUsage->promocode_id)){
					if($Promocode->ride == 'BOTH') {
					$Discount = $Promocode->discount;
                    $PromocodeUsage->status ='USED';
                    $PromocodeUsage->save();
					}
					elseif($UserRequest->ride_in  == 'INTER' && $Promocode->ride == 'INTER')
					{
					$Discount = $Promocode->discount;
                    $PromocodeUsage->status ='USED';
                    $PromocodeUsage->save();	
					}
					elseif($UserRequest->ride_in  == 'OUTER' && $Promocode->ride == 'OUTER')
					{
					$Discount = $Promocode->discount;
					$PromocodeUsage->status ='USED';
					$PromocodeUsage->save();	
					}
					elseif($UserRequest->ride_in  == 'RENTAL' && $Promocode->ride == 'RENTAL')
					{
					$Discount = $Promocode->discount;
					$PromocodeUsage->status ='USED';
					$PromocodeUsage->save();	
					}
                }
            }
            $Wallet = 0;
            $Surge = 0;
            $Paid = 0;

			if($UserRequest->ride_in  == 'INTER')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('inter_commission', 0) / 100);
			}
			elseif($UserRequest->ride_in  == 'OUTER')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('outer_commission', 0) / 100);
			}
			elseif($UserRequest->ride_in  == 'RENTAL')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('rental_commission', 0) / 100);
			}
           // $Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('commission_percentage', 0) / 100);

            $GrossTotal = ($DateFees  + $Minutes + $Fixed + $Distance + $Commision+$Connection + $cancelfee)-$Discount ;

            $Tax = ($GrossTotal) * (Setting::get('tax_percentage', 0) / 100);
            $Total = $GrossTotal  + $Tax + $mcd_tax + $state_tax + $toll_tax + $parking_tax + $airport_tax;

            if($UserRequest->surge){
                $Surge = (Setting::get('surge_percentage')/100) * $Total;
                $Total += $Surge;
            }

            if($Total < 0){
                $Total = 0.00; // prevent from negative value
            }
            $Paid = $Total;

            $Payment = new UserRequestPayment;
            $Payment->request_id = $UserRequest->id;
            $Payment->fixed = $Fixed;
            $Payment->distance = $Distance;
            $Payment->minutes = $Minutes;
            $Payment->commision = $Commision;
            $Payment->gross_total = $GrossTotal;
            $Payment->connection = $Connection;
            $Payment->cancelfee = $cancelfee;
			$Payment->mcd_tax = $mcd_tax; 
			$Payment->state_tax = $state_tax; 
			$Payment->toll_tax = $toll_tax; 
			$Payment->parking_tax = $parking_tax; 
			$Payment->airport_tax = $airport_tax; 
            $Payment->day = $DateFees;  
            $Payment->total = abs($Total);
            $Payment->surge = $Surge;
            if($Discount != 0 && $PromocodeUsage){
                $Payment->promocode_id = $PromocodeUsage->promocode_id;
            }
            $Payment->discount = $Discount;

			if($cancelfee > 0)
			{
				 User::where('id',$UserRequest->user_id)->update(['pending_cancel_fee' => 0]);
			}
			
            if($UserRequest->use_wallet == 1 && $Paid > 0){

                $User = User::find($UserRequest->user_id);

                $Wallet = $User->wallet_balance;

                if($Wallet != 0){

                    if($Paid > $Wallet) {

                        $Payment->wallet = $Wallet;
                        $Payable = $Paid - $Wallet;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => 0 ]);
                        $Payment->paid = abs($Payable);

                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Wallet));

                    } else {

                        $Payment->paid = 0;
                        $WalletBalance = $Wallet - $Paid;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => $WalletBalance]);
                        $Payment->wallet = $Paid;
                        
                        $Payment->payment_id = 'WALLET';
                        $Payment->payment_mode = $UserRequest->payment_mode;
                        $Payment->paid = 1;

                        $UserRequest->paid = 1;
                        $UserRequest->status = 'COMPLETED';
                        $UserRequest->save();

						$Payment1 = new UserPayment;
						$Payment1->user_id = $UserRequest->user_id;
						$Payment1->txnid = "";
						$Payment1->amount = $Paid;
						$Payment1->type = "WALLET_DEBIT";
						$Payment1->save();
			
                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Paid));
                    }

                }

            } else {
                $Payment->paid = abs($Paid);
            }

            $Payment->tax = $Tax;
            $Payment->save();

			
			if($UserRequest->use_wallet == 1 && $Paid > 0){
			$pay = UserRequestPayment::where('request_id',$UserRequest->id)->first();
				
			$Provider = Provider::find($UserRequest->provider_id);
			$Provider->wallet_amount  = $Provider->wallet_amount - $pay->commision;
			$Provider->save();
			ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);
			
			$Payment = new ProviderPayment;
            $Payment->provider_id = $Provider->id;
            $Payment->txnid = "";
            $Payment->amount = $pay->commision;
            $Payment->type = "WALLET_DEBIT";
			$Payment->save();
						
			// charged wallet money push 
			$t = currency($pay->commision).' '.trans('api.push.charged_from_wallet').'. Current Balance:'. currency($Provider->wallet_amount);
			
            (new SendPushNotification)->ChargedWalletMoney_Provider($UserRequest->provider_id,$t);
			}	
			
			return $Payment;

        } catch (ModelNotFoundException $e) {
            return false;
        }
    }
	
	public function wallet_statement() {
    
        try{
            $ProviderPayment = ProviderPayment::WalletStatement(Auth::user()->id)->get();
			return $ProviderPayment;
        }
		catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	public function cancelled_trips() {
    
        try{
           // $UserRequests = UserRequests::ProviderCancelTrips(Auth::user()->id)->get();

            $UserRequests = UserRequests::where('user_requests.provider_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'CANCELLED')
                    ->orderBy('user_requests.created_at','desc')
                    ->select('user_requests.*')
                    ->with('provider','service_type')->get();
            
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function history_details(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);

        if($request->ajax()) {
            
            $Jobs = UserRequests::where('id',$request->request_id)
                                ->where('provider_id', Auth::user()->id)
                                ->with('payment','service_type','user','rating')
                                ->get();
            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }

            return $Jobs;
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trips() {
    
        try{
//            $UserRequests = UserRequests::ProviderUpcomingRequest(Auth::user()->id)->get();

            $UserRequests = UserRequests::where('user_requests.provider_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'SCHEDULED')
                    ->select('user_requests.*')
                    ->with('service_type','user','provider')->get();


            if(!empty($UserRequests)){
                $map_icon = asset('asset/marker.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                                    "autoscale=1".
                                    "&size=320x130".
                                    "&maptype=terrian".
                                    "&format=png".
                                    "&visual_refresh=true".
                                    "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                                    "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                                    "&path=color:0x000000|weight:3|enc:".$value->route_key.
                                    "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' =>'djjd']);
        }
    }

    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming_details(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);

        if($request->ajax()) {
            
            $Jobs = UserRequests::where('id',$request->request_id)
                                ->where('provider_id', Auth::user()->id)
                                ->with('service_type','user')
                                ->get();
            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }

            return $Jobs;
        }

    }

    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        try{
            if($request->ajax()) {
                $rides = UserRequests::where('provider_id', Auth::user()->id)->count();
                $revenue = UserRequestPayment::whereHas('request', function($query) use ($request) {
                                $query->where('provider_id', Auth::user()->id);
                            })
                        ->sum('total');
                $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id', Auth::user()->id)->count();
                $scheduled_rides = UserRequests::where('status','SCHEDULED')->where('provider_id', Auth::user()->id)->count();

                return response()->json([
                    'rides' => $rides, 
                    'revenue' => $revenue,
                    'cancel_rides' => $cancel_rides,
                    'scheduled_rides' => $scheduled_rides,
                ]);
            }

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }

    }


    /**
     * help Details.
     *
     * @return \Illuminate\Http\Response
     */

    public function help_details(Request $request){

        try{

            if($request->ajax()) {
                return response()->json([
                    'contact_number' => Setting::get('contact_number',''), 
                    'contact_email' => Setting::get('contact_email','')
                     ]);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function chat_histroy(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
        try{
            $Chat = Chat::where('request_id',$request->request_id)
                        ->where('provider_id', Auth::user()->id)
                        ->get();
            return response()->json(["status"=>true,"messages"=>$Chat]);
        }catch (Exception $e) {
            return response()->json(["status"=>false,'error' => trans('api.something_went_wrong')], 500);
        }
    }
	
	
	public function invoice1($request_id)
    {
        try {
            $UserRequest = UserRequests::findOrFail($request_id);
            
			UserRequestPayment::where('request_id', $request_id)->delete();
			
            $Fixed  = 0;
            $Distance = 0;
            $Discount = 0; // Promo Code discounts should be added here.
            $Minutes = 0; // Promo Code discounts should be added here.
            $DateFees = 0;
            $TravelDay = 0;
            $Connection = $UserRequest->service_type->connection ? : 0;
			$cancelfee = 0;

			$mcd_tax = 0;
			$state_tax = 0;
			$toll_tax = 0;
			$parking_tax = 0;
			$airport_tax = 0;
			
			$StartedDate  = date_create($UserRequest->started_at);
            $FinisedDate  = date_create($UserRequest->finished_at);
         //   $TraveledTime = round((strtotime($UserRequest->finished_at) - strtotime($UserRequest->started_at))/3600, 1);
			
            $TimeInterval = date_diff($StartedDate,$FinisedDate);
			$TraveledTime = $TimeInterval->h * 60;
            $TraveledTime += $TimeInterval->i;
			
			
			$mcd_tax = $UserRequest->mcd_tax;
			$state_tax = $UserRequest->state_tax;
			$toll_tax = $UserRequest->toll_tax;
			$parking_tax = $UserRequest->parking_tax;
			$airport_tax = $UserRequest->airport_tax;
				
            //$TimeInterval = date_diff($StartedDate,$FinisedDate);
           // $TraveledTime = $TimeInterval->i;
            if($UserRequest->ride_in  == 'INTER'){
               $Fixed = $UserRequest->service_type->fixed ? : 0;
			   
			   $TravelDistance = (round($UserRequest->distance,2));
               if($TravelDistance <= $UserRequest->service_type->distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->distance;
				}
				
			   $Distance = $TravelDistance1 * $UserRequest->service_type->price;
				
			   $DateFees  = floor ($TraveledTime / 1440);
				if($DateFees > 1)
				{
					$DateFees  = $DateFees  * $UserRequest->service_type->day;
				}			
				else{
                  $DateFees = 0;
               }
               $TravelDay = $DateFees;
			   
			   $Minutes  = $UserRequest->service_type->minute * $TraveledTime;
			   
			   $user = User::findOrFail($UserRequest->user_id);
			   if($user->pending_cancel_fee > 0)
			   {
				   $cancelfee = $user->pending_cancel_fee;
			   }
			   
            }else if($UserRequest->ride_in  == 'OUTER' && $UserRequest->ride_way == 'ONEROUND'){
               $Fixed = $UserRequest->service_type->outer_fixed? : 0;
               $TravelDistance = (round($UserRequest->distance,2));
               if($TravelDistance <= $UserRequest->service_type->outer_distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->outer_distance;
				}
				
               $TravelDistance = $TravelDistance1 * 2;
               $Distance  = $TravelDistance * $UserRequest->service_type->outer_price;
               //$DateFees  = $TimeInterval->format('%h');
              $DateFees  = floor ($TraveledTime / 1440);
				if($DateFees > 1)
				{
					$DateFees  = $DateFees  * $UserRequest->service_type->day;
				}			
				else{
                  $DateFees = 0;
                }
               
			   $TravelDay = $DateFees;
            //   $DateFees  = $DateFees  * $UserRequest->service_type->day;
             //  $Fixed     = $Fixed;
               
            }else if($UserRequest->ride_in  == 'OUTER' && ($UserRequest->ride_way == 'ONE' || $UserRequest->ride_way == 'ROUND') ){
               $Fixed = $UserRequest->service_type->outer_fixed? : 0;
               $TravelDistance = (round($UserRequest->distance,2));
               if($TravelDistance <= $UserRequest->service_type->outer_distance){
					$TravelDistance1 = 0;
                }
				else{
					$TravelDistance1 = $TravelDistance - $UserRequest->service_type->outer_distance;
				}
				
               $Distance  = $TravelDistance1 * $UserRequest->service_type->outer_price;
               //$DateFees  = $TimeInterval->format('%h');
              
				$DateFees  = floor ($TraveledTime / 1440);
				if($DateFees > 1)
				{
					$DateFees  = $DateFees  * $UserRequest->service_type->day;
				}			
				else{
                  $DateFees = 0;
               }
               $TravelDay = $DateFees;
              
            }else if($UserRequest->ride_in  == 'RENTAL'){
				if($UserRequest->package != '')
				{
					$rental = Rental::where('service_type',$UserRequest->service_type_id)
							->where('hours',$UserRequest->package)
							->first();
					if($rental)
				{		
					$Fixed = $rental->amount;
					$TravelDistance = (round($UserRequest->distance,2));
					if($TravelDistance <= $rental->kms){
						$TravelDistance1 = 0;
					}
					else{
						$TravelDistance1 = $TravelDistance - $rental->kms;
					}
					$Distance = $TravelDistance1 * $rental->extra_km;
					
					$Hours = $TraveledTime/60;
					if($Hours <= $rental->hours)
					{
						$Hours1 = 0;
					}
					else{
						$Hours1 = $Hours - $rental->hours;
					}
					$Minutes += (ceil($Hours1) * $rental->extra_hours);
				}
				}
				if($UserRequest->airport == 1)
				{
					$Fixed = $UserRequest->service_type->airport;
				}
			}

            $UserRequest->traveled_time = $TraveledTime;
            $UserRequest->travel_day = $TravelDay;
            $UserRequest->save();

            if($PromocodeUsage = PromocodeUsage::where('user_id',$UserRequest->user_id)->where('status','ADDED')->first()){
                if($Promocode = Promocode::find($PromocodeUsage->promocode_id)){
					if($Promocode->ride == 'BOTH') {
					$Discount = $Promocode->discount;
                    $PromocodeUsage->status ='USED';
                    $PromocodeUsage->save();
					}
					elseif($UserRequest->ride_in  == 'INTER' && $Promocode->ride == 'INTER')
					{
					$Discount = $Promocode->discount;
                    $PromocodeUsage->status ='USED';
                    $PromocodeUsage->save();	
					}
					elseif($UserRequest->ride_in  == 'OUTER' && $Promocode->ride == 'OUTER')
					{
					$Discount = $Promocode->discount;
					$PromocodeUsage->status ='USED';
					$PromocodeUsage->save();	
					}
					elseif($UserRequest->ride_in  == 'RENTAL' && $Promocode->ride == 'RENTAL')
					{
					$Discount = $Promocode->discount;
					$PromocodeUsage->status ='USED';
					$PromocodeUsage->save();	
					}
                }
            }
            $Wallet = 0;
            $Surge = 0;
            $Paid = 0;

			if($UserRequest->ride_in  == 'INTER')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('inter_commission', 0) / 100);
			}
			elseif($UserRequest->ride_in  == 'OUTER')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('outer_commission', 0) / 100);
			}
			elseif($UserRequest->ride_in  == 'RENTAL')
			{
				$Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('rental_commission', 0) / 100);
			}
           // $Commision = ( $DateFees  + $Fixed + $Distance+$Minutes+$Connection ) * (Setting::get('commission_percentage', 0) / 100);

            $GrossTotal = ($DateFees  + $Minutes + $Fixed + $Distance + $Commision+$Connection + $cancelfee)-$Discount ;

            $Tax = ($GrossTotal) * (Setting::get('tax_percentage', 0) / 100);
            $Total = $GrossTotal  + $Tax + $mcd_tax + $state_tax + $toll_tax + $parking_tax + $airport_tax;

            if($UserRequest->surge){
                $Surge = (Setting::get('surge_percentage')/100) * $Total;
                $Total += $Surge;
            }

            if($Total < 0){
                $Total = 0.00; // prevent from negative value
            }
            $Paid = $Total;

            $Payment = new UserRequestPayment;
            $Payment->request_id = $UserRequest->id;
            $Payment->fixed = $Fixed;
            $Payment->distance = $Distance;
            $Payment->minutes = $Minutes;
            $Payment->commision = $Commision;
            $Payment->gross_total = $GrossTotal;
            $Payment->connection = $Connection;
            $Payment->cancelfee = $cancelfee;
			$Payment->mcd_tax = $mcd_tax; 
			$Payment->state_tax = $state_tax; 
			$Payment->toll_tax = $toll_tax; 
			$Payment->parking_tax = $parking_tax; 
			$Payment->airport_tax = $airport_tax; 
            $Payment->day = $DateFees;  
            $Payment->total = abs($Total);
            $Payment->surge = $Surge;
            if($Discount != 0 && $PromocodeUsage){
                $Payment->promocode_id = $PromocodeUsage->promocode_id;
            }
            $Payment->discount = $Discount;

			if($cancelfee > 0)
			{
				 User::where('id',$UserRequest->user_id)->update(['pending_cancel_fee' => 0]);
			}
			
            if($UserRequest->use_wallet == 1 && $Paid > 0){

                $User = User::find($UserRequest->user_id);

                $Wallet = $User->wallet_balance;

                if($Wallet != 0){

                    if($Paid > $Wallet) {

                        $Payment->wallet = $Wallet;
                        $Payable = $Paid - $Wallet;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => 0 ]);
                        $Payment->paid = abs($Payable);

                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Wallet));

                    } else {

                        $Payment->paid = 0;
                        $WalletBalance = $Wallet - $Paid;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => $WalletBalance]);
                        $Payment->wallet = $Paid;
                        
                        $Payment->payment_id = 'WALLET';
                        $Payment->payment_mode = $UserRequest->payment_mode;
                        $Payment->paid = 1;

                        $UserRequest->paid = 1;
                        $UserRequest->status = 'COMPLETED';
                        $UserRequest->save();

						$Payment1 = new UserPayment;
						$Payment1->user_id = $UserRequest->user_id;
						$Payment1->txnid = "";
						$Payment1->amount = $Paid;
						$Payment1->type = "WALLET_DEBIT";
						$Payment1->save();
			
                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Paid));
                    }

                }

            } else {
                $Payment->paid = abs($Paid);
            }

            $Payment->tax = $Tax;
            $Payment->save();

			
			if($UserRequest->use_wallet == 1 && $Paid > 0){
			$pay = UserRequestPayment::where('request_id',$UserRequest->id)->first();
				
			$Provider = Provider::find($UserRequest->provider_id);
			$Provider->wallet_amount  = $Provider->wallet_amount - $pay->commision;
			$Provider->save();
			ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);
			
			$Payment = new ProviderPayment;
            $Payment->provider_id = $Provider->id;
            $Payment->txnid = "";
            $Payment->amount = $pay->commision;
            $Payment->type = "WALLET_DEBIT";
			$Payment->save();
						
			// charged wallet money push 
			$t = currency($pay->commision).' '.trans('api.push.charged_from_wallet').'. Current Balance:'. currency($Provider->wallet_amount);
			
            (new SendPushNotification)->ChargedWalletMoney_Provider($UserRequest->provider_id,$t);
			}	
			
			return $Payment;

        } catch (ModelNotFoundException $e) {
            return false;
        }
    }
	

}
