<?php

namespace App\Http\Controllers;

use App\ServiceZone;
use Illuminate\Http\Request;

class ServiceZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $servicezones = ServiceZone::all();
			// return view('admin.servicezones')->withServiceZone($zones);
		    return view('admin.servicezones', compact('servicezones'));
		  /*  $servicezones = ServiceZone::orderBy('created_at' , 'desc')->get();
			return view('admin.servicezones', compact('servicezones')); */
		
        }
        catch(Exception $e){
            return redirect()->route('admin.user.index')->with('flash_error','Something Went Wrong with Dashboard!');
        }
    }

	
									 
	 public function sql_to_coordinates($blob)
    {
        $blob = str_replace("))", "", str_replace("POLYGON((", "", $blob));
        $coords = explode(",", $blob);
        $coordinates = array();
		
        foreach($coords as $coord)
        {
            $coord_split = explode(" ", $coord);
            $coordinates[]=array("lat"=>$coord_split[0], "lng"=>$coord_split[1]);
        } 
        return $coordinates;
    }
									 
									  
	public function create_service_zone()
	{
		$name =$_GET['name'];
		$services =$_GET['services'];
		$polygon =$_GET['polygon'];
		$comments =$_GET['comments'];
		
		$service_zone = new ServiceZone;
		$service_zone->name = $name;
		$service_zone->services = $services;
		$service_zone->polygon = $polygon;
		$service_zone->comment = $comments;
		$service_zone->save();
			
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $zone = new ServiceZone($request->all());
        $zone->save();
        dd("suucess");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zone=ServiceZone::findOrFail($id);
        $zone->delete();
        return redirect('/admin/servicezones');
    }
}
