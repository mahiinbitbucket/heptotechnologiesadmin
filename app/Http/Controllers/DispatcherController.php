<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use Setting;
use Auth;
use Exception;
use Carbon\Carbon;
use App\Helpers\Helper;

use App\User;
use App\Dispatcher;
use App\Provider;
use App\UserRequests;
use App\ServiceType;
use App\Rental;
use App\RequestFilter;
use App\ProviderService;
use Location\Coordinate;
use Location\Distance\Vincenty;
use App\Http\Controllers\SendPushNotification;

class DispatcherController extends Controller
{

    /**
     * Dispatcher Panel.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('admin')->user()){
            return view('admin.dispatcher');
        }elseif(Auth::guard('dispatcher')->user()){
            return view('dispatcher.dispatcher');
        }
    }

    /**
     * Display a listing of the active trips in the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function trips(Request $request)
    {
        /* $Trips = UserRequests::with('user', 'provider')->orderBy('id','desc')->paginate(10);
        return $Trips; */
		
		$Trips = UserRequests::with('user', 'provider')
                    ->orderBy('id','desc');
					
				//	 ->orderBy('schedule_at','asc')
				//	->orderBy('id','desc');

        if($request->type == "SEARCHING"){
            $Trips = $Trips->where('status',$request->type);
        }else if($request->type == "CANCELLED"){
            $Trips = $Trips->where('status',$request->type);
        }else if($request->type == "SCHEDULED"){
            $Trips = $Trips->where('status',$request->type) ->where('provider_id', '!=', 0);
        }else if($request->type == "UNASSIGNED"){
            $Trips = $Trips->where('status','SCHEDULED') ->where('provider_id', 0);
        }else if($request->type == "COMPLETED"){
            $Trips = $Trips->where('status',$request->type);
        }
		
        
        $Trips =  $Trips->paginate(10);

        return $Trips;
		
    }

    /**
     * Display a listing of the users in the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        $Users = new User;

        if($request->has('mobile')) {
            $Users->where('mobile', 'like', $request->mobile."%");
        }

        if($request->has('first_name')) {
            $Users->where('first_name', 'like', $request->first_name."%");
        }

        if($request->has('last_name')) {
            $Users->where('last_name', 'like', $request->last_name."%");
        }

        if($request->has('email')) {
            $Users->where('email', 'like', $request->email."%");
        }

        return $Users->paginate(10);
    }

    /**
     * Display a listing of the active trips in the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function providers(Request $request)
    {
        $Providers = new Provider;

        if($request->has('latitude') && $request->has('longitude')) {
            $ActiveProviders = ProviderService::AllAvailableServiceProvider($request->service_type)
                    ->get()
                    ->pluck('provider_id');

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->latitude;
            $longitude = $request->longitude;

			Log::info(print_r($ActiveProviders, true));
			
            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
				->orwhereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) = NULL")
                ->with('service', 'service.service_type')
                ->paginate(10);

            return $Providers;
        }

        return $Providers;
    }

    /**
     * Create manual request.
     *
     * @return \Illuminate\Http\Response
     */
    public function assign($request_id, $provider_id)
    {
        try {
            $Request = UserRequests::findOrFail($request_id);
            $Provider = Provider::findOrFail($provider_id);

            $Request->current_provider_id = $Provider->id;
            $Request->status = 'SEARCHING';
			$Request->assigned_at = Carbon::now();
            $Request->save();
            (new SendPushNotification)->IncomingRequest($Request->current_provider_id);

            try {
                RequestFilter::where('request_id', $Request->id)
                    ->where('provider_id', $Provider->id)
                    ->firstOrFail();
            } catch (Exception $e) {
                $Filter = new RequestFilter;
                $Filter->request_id = $Request->id;
                $Filter->provider_id = $Provider->id; 
                $Filter->save();
            }

            if(Auth::guard('admin')->user()){
                return redirect()
                        ->route('admin.dispatcher.index')
                        ->with('flash_success', 'Request Assigned to Provider!');

            }elseif(Auth::guard('dispatcher')->user()){
                return redirect()
                        ->route('dispatcher.index')
                        ->with('flash_success', 'Request Assigned to Provider!');

            }

        } catch (Exception $e) {
            if(Auth::guard('admin')->user()){
                return redirect()->route('admin.dispatcher.index')->with('flash_error', 'Something Went Wrong!');
            }elseif(Auth::guard('dispatcher')->user()){
                return redirect()->route('dispatcher.index')->with('flash_error', 'Something Went Wrong!');
            }
        }
    }


    /**
     * Create manual request.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {

        $this->validate($request, [
                's_latitude' => 'required|numeric',
                's_longitude' => 'required|numeric',
                'd_latitude' => 'required|numeric',
                'd_longitude' => 'required|numeric',
                'service_type' => 'required|numeric|exists:service_types,id',
                'distance' => 'required|numeric',
				'ride_in'=>'required|in:INTER,OUTER,BOTH,RENTAL',
				'package' => 'numeric',
				'airport' => 'numeric',
            ]);

        try {
            $User = User::where('mobile', $request->mobile)->firstOrFail();
        } catch (Exception $e) {
            try {
                $User = User::where('email', $request->email)->firstOrFail();
            } catch (Exception $e) {
                $User = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'mobile' => $request->mobile,
                    'password' => bcrypt($request->mobile),
                    'payment_mode' => 'CASH'
                ]);
            }
        }

        if($request->has('schedule_time')){
            try {
                $CheckScheduling = UserRequests::where('status', 'SCHEDULED')
                        ->where('user_id', $User->id)
                        ->where('schedule_at', '>', strtotime($request->schedule_time." - 1 hour"))
                        ->where('schedule_at', '<', strtotime($request->schedule_time." + 1 hour"))
                        ->firstOrFail();
                
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.request_scheduled')], 500);
                } else {
                    return redirect('dashboard')->with('flash_error', 'Already request is Scheduled on this time.');
                }

            } catch (Exception $e) {
                // Do Nothing
            }
        }

        try{

            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$request->s_latitude.",".$request->s_longitude."&destination=".$request->d_latitude.",".$request->d_longitude."&mode=driving&key=".Setting::get('map_key');

            $json = curl($details);

            $details = json_decode($json, TRUE);

            $route_key = $details['routes'][0]['overview_polyline']['points'];

            $UserRequest = new UserRequests;
            $UserRequest->booking_id = Helper::generate_booking_id();
            $UserRequest->user_id = $User->id;
            $UserRequest->current_provider_id = 0;
            $UserRequest->service_type_id = $request->service_type;
            $UserRequest->payment_mode = 'CASH';
            
            $UserRequest->status = 'SEARCHING';

            $UserRequest->s_address = $request->s_address ? : "";
            $UserRequest->s_latitude = $request->s_latitude;
            $UserRequest->s_longitude = $request->s_longitude;

            $UserRequest->d_address = $request->d_address ? : "";
            $UserRequest->d_latitude = $request->d_latitude;
            $UserRequest->d_longitude = $request->d_longitude;
			
			if($request->has('package') && ($request->ride_in =='RENTAL')){
            	$UserRequest->package = $request->package;
            }
			
			if($request->has('airport')){
            	$UserRequest->airport = $request->airport;
            
				if($request->airport == 1)
				{
					$UserRequest->d_address = "Kempegowda International Airport, KIAL Rd, Devanahalli, Bengaluru, Karnataka 560300";
					$UserRequest->d_latitude = 13.199379;
					$UserRequest->d_longitude = 77.710136;
				}
			}
			
			
            $UserRequest->route_key = $route_key;

            $UserRequest->distance = $request->distance;
			$UserRequest->travel_distance = $request->distance;
			
			 if($request->has('ride_in')){
            	$UserRequest->ride_in = $request->ride_in;
            }
			
            $UserRequest->assigned_at = Carbon::now();

            $UserRequest->use_wallet = 0;
            $UserRequest->surge = 0;        // Surge is not necessary while adding a manual dispatch

            if($request->has('schedule_time')) {
                $UserRequest->schedule_at = Carbon::parse($request->schedule_time);
            }
			if($request->has('round_time')) {
				$UserRequest->ride_way = "ROUND";
				 $UserRequest->user_started_at = Carbon::parse($request->schedule_time);
                $UserRequest->user_finished_at = Carbon::parse($request->round_time);
            }

			//////////estimated_fare
			
			if($request->ride_in != 'RENTAL') 
		{
			$Hours = 0;
            $kms =0;
            $extra_km = 0;
            $extra_hr = 0;
			$time = 0;
			$kilometer = 0;
			
			$details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');

            $json = curl($details);

            $details = json_decode($json, TRUE);

			if($details['status'] == "OK"){
            $meter = $details['rows'][0]['elements'][0]['distance']['value'];
            $time = $details['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $details['rows'][0]['elements'][0]['duration']['value'];
				
            $kilometer = round($meter/1000);
            $minutes = round($seconds/60);
			//$Hours = $seconds/3600;
			}
			else{
				$coordinate1 = new Coordinate($request->s_latitude, $request->s_longitude); /** Set Distance Calculation Source Coordinates ****/
				$coordinate2 = new Coordinate($request->d_latitude, $request->d_longitude); /** Set Distance calculation Destination Coordinates ****/
				$calculator = new Vincenty();
				$mydistance = $calculator->getDistance($coordinate1, $coordinate2); 
				$meter = round($mydistance);
				$kilometer = round($meter/1000);
				$time = ($kilometer * 1.2)." mins";
				$minutes = $time;
			}
		}
		
			$tax_percentage = Setting::get('tax_percentage');
            $commission_percentage = Setting::get('commission_percentage');
            $service_type = ServiceType::findOrFail($request->service_type);
            $DayPrices = 0;
            $DateFees = 0;
            $IsRound = false;
			
			 if($request->ride_in == 'INTER'){
				$price = $service_type->fixed;
                $base =  $service_type->fixed;
    	        
				if($kilometer <= $service_type->distance){
					$kilometer1 = 0;
                }
				else{
					$kilometer1 = $kilometer - $service_type->distance;
				}
    	       	if($service_type->calculator == 'MIN') {
    	                $price += $service_type->minute * $minutes;
    	        } else if($service_type->calculator == 'HOUR') {
    	                $price += $service_type->minute * 60;
    	        } else if($service_type->calculator == 'DISTANCE') {
    	                $price += ($kilometer1 * $service_type->price);
    	        } else if($service_type->calculator == 'DISTANCEMIN') {
    	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
    	        } else if($service_type->calculator == 'DISTANCEHOUR') {
    	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
    	        } else {
    	                $price += ($kilometer * $service_type->price);
    	        }
			 }else if($request->ride_in == 'OUTER'){
                $base =  $service_type->outer_fixed;
                $price =  $service_type->outer_fixed;
                $outerdistance = $kilometer;
                if($outerdistance <= $service_type->outer_distance){
                   $outerdistance = 0; 
                }
				else{
					$outerdistance = $outerdistance - $service_type->outer_distance;
				}
            	
    	       	if($service_type->calculator == 'MIN') {
    	              $price += $service_type->minute * $minutes;
    	        } else if($service_type->calculator == 'HOUR') {
    	               $price += $service_type->minute * 60;
    	        } else if($service_type->calculator == 'DISTANCE') {
    	                $price += ($outerdistance * $service_type->outer_price);
    	        } else if($service_type->calculator == 'DISTANCEMIN') {
    	                $price += ($outerdistance * $service_type->outer_price) + ($service_type->minute * $minutes);
    	        } else if($service_type->calculator == 'DISTANCEHOUR') {
    	                $price += ($outerdistance * $service_type->outer_price) + ($service_type->minute * $minutes * 60);
    	        } else {
    	                $price += ($outerdistance * $service_type->outer_price);
    	        }
    	        
    	        if($IsRound == true || $request->ride_way == 'ROUND'){
	               $price = $price * 2;
            	}
                if($request->ride_way == 'ROUND'){
                   $kilometer = $kilometer * 2 ;
                }
               
    	        if($request->has('user_started_at') && $request->has('user_finished_at')  && ($request->ride_in == 'OUTER' || $request->ride_way == 'ROUND')){
    	           $CreatedDate = date("Y-m-d H:i:s",strtotime($request->user_started_at));
    	           $FinishedDate = date("Y-m-d H:i:s",strtotime($request->user_finished_at));
    	           //$CreatedDate  = date_create($CreatedDate);
    	           //$FinishedDate = date_create($FinishedDate);
    	           //$TimeInterval = date_diff($CreatedDate,$FinishedDate);
                   $DateFees = round((strtotime($request->user_finished_at) - strtotime($request->user_started_at))/3600, 1);
    	           //$DateFees  = $TimeInterval->format('%h');
                   if($DateFees>0){
                      $DateFees  = ceil($DateFees/24);
                   }else{
                      $DateFees = 0;
                   }
    	           $price += ($service_type->day* $DateFees);
    	           $DayPrices = ($service_type->day* $DateFees);
    	        }
                
              
            }else if($request->ride_in == 'RENTAL'){
				
				$kilometer = 0 ;
				$time = 0;
				
				 if($request->package != '')
				{
					$rental = Rental::where('service_type',$request->service_type)
							->where('hours',$request->package)
							->first();
					if($rental)
					{						
						$price = $rental->amount;
						$base = $rental->amount;
						$Hours = $rental->hours;
						$kms = $rental->kms;
						$extra_km = $rental->extra_km;
						$extra_hr = $rental->extra_hours;
					}
					else{
						$price = 0;
						$base = 0;
						$Hours = 0;
						$kms = 0;
						$extra_km = 0;
						$extra_hr = 0;
					}
				}
				if($request->airport == '1')
				{
					$base = $service_type->airport;
					$price = $service_type->airport;
					$Hours = 0;
					$kms = 0;
					$extra_km = 0;
					$extra_hr = 0;
				}
				
				
			}
			
			$price += $service_type->connection;
            $commission_price = ( $commission_percentage/100 ) * $price;
            $price += $commission_price;
            $tax_price = ( $tax_percentage/100 ) * $price;
            $total = $price + $tax_price;
			$UserRequest->estimated_fare = round($total,2);
			/////////////////
            $UserRequest->save();

            if($request->has('provider_auto_assign')) {
                $ActiveProviders = ProviderService::AvailableServiceProvider($request->service_type)
                        ->get()
                        ->pluck('provider_id');

                $distance = Setting::get('provider_search_radius', '10');
                $latitude = $request->s_latitude;
                $longitude = $request->s_longitude;

                $Providers = Provider::whereIn('id', $ActiveProviders)
                    ->where('status', 'approved')
                    ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get();

                // List Providers who are currently busy and add them to the filter list.

                if(count($Providers) == 0) {
                    if($request->ajax()) {
                        // Push Notification to User
                        return response()->json(['message' => trans('api.ride.no_providers_found')]); 
                    } else {
                        return back()->with('flash_success', 'No Providers Found! Please try again.');
                    }
                }

                $Providers[0]->service()->update(['status' => 'riding']);

                $UserRequest->current_provider_id = $Providers[0]->id;
                $UserRequest->save();

                Log::info('New Dispatch : ' . $UserRequest->id);
                Log::info('Assigned Provider : ' . $UserRequest->current_provider_id);

                // Incoming request push to provider
                (new SendPushNotification)->IncomingRequest($UserRequest->current_provider_id);

                foreach ($Providers as $key => $Provider) {
                    $Filter = new RequestFilter;
                    $Filter->request_id = $UserRequest->id;
                    $Filter->provider_id = $Provider->id; 
                    $Filter->save();
                }
            }

            if($request->ajax()) {
                return $UserRequest;
            } else {
                return redirect('dashboard');
            }

        } catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong'), 'message' => $e], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('dispatcher.account.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@hepto.com');
        }

        $this->validate($request,[
            'name' => 'required|max:255',
            'mobile' => 'required|digits_between:6,13',
        ]);

        try{
            $dispatcher = Auth::guard('dispatcher')->user();
            $dispatcher->name = $request->name;
            $dispatcher->mobile = $request->mobile;
            $dispatcher->save();

            return redirect()->back()->with('flash_success','Profile Updated');
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('dispatcher.account.change-password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@hepto.com');
        }

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try {

           $Dispatcher = Dispatcher::find(Auth::guard('dispatcher')->user()->id);

            if(password_verify($request->old_password, $Dispatcher->password))
            {
                $Dispatcher->password = bcrypt($request->password);
                $Dispatcher->save();

                return redirect()->back()->with('flash_success','Password Updated');
            }
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
}
