<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Setting;
use Exception;
use App\Helpers\Helper;
use App\ServiceZone;
use App\ServiceType;
use App\Rental;

class RentalResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rental = Rental::all();
		$ServiceType = ServiceType::all();
        if($request->ajax()) {
            return $rental;
        } else {
            return view('admin.rental.index', compact('rental','ServiceType'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$ServiceType = ServiceType::all();
       // return view('admin.service.create');
		return view('admin.rental.create', compact('ServiceType'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@hepto.com');
        }

        $this->validate($request, [
            'service_type' => 'required|numeric',
			'hours' => 'required|numeric',
            'kms' => 'required|numeric',
            'extra_km' => 'required|numeric',
            'extra_hours' => 'required|numeric',
            'amount' => 'required|numeric'
       ]);

        try {
            $rental = $request->all();

			$rental = Rental::create($rental);

            return back()->with('flash_success','Rental Package Saved Successfully');
        } catch (Exception $e) {
            //dd("Exception", $e);
            return back()->with('flash_error', 'Rental Package Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Rental::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Rental Package Not Found');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $rental = Rental::findOrFail($id);
			$rentals = ServiceType::all();
            return view('admin.rental.edit',compact('rental','rentals'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Rental Package Not Found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@hepto.com');
        }

        $this->validate($request, [
            'service_type' => 'required|numeric',
			'hours' => 'required|numeric',
            'kms' => 'required|numeric',
            'extra_km' => 'required|numeric',
            'extra_hours' => 'required|numeric',
            'amount' => 'required|numeric'
        ]);

        try {

            $rental = Rental::findOrFail($id);

            $rental->service_type = $request->service_type;
            $rental->hours = $request->hours;
            $rental->kms = $request->kms;
            $rental->extra_km = $request->extra_km;
            $rental->extra_hours = $request->extra_hours;
            $rental->amount = $request->amount;
            $rental->save();

            return redirect()->route('admin.rental.index')->with('flash_success', 'Rental Package Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Rental Package Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@hepto.com');
        }
        
        try {
            Rental::find($id)->delete();
            return back()->with('message', 'Rental Package deleted successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Rental Package Not Found');
        } catch (Exception $e) {
            return back()->with('flash_error', 'Rental Package Not Found');
        }
    }
}
