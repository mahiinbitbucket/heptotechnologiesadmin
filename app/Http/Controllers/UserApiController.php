<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Kolkata");
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
use Notification;
use Mail;

use Carbon\Carbon;
use App\Http\Controllers\SendPushNotification;
use App\Notifications\ResetPasswordOTP;
use App\Helpers\Helper;
use App\Mail\RequestUserBookingReminder;
use App\Mail\RequestUserInvoiceReminder;

use App\Card;
use App\User;
use App\Chat;
use App\Provider;
use App\Location;
use App\CarCategory;
use App\Settings;
use App\Promocode;
use App\ServiceType;
use App\ServiceZone;
use App\Rental;
use App\UserRequests;
use App\UserPayment;
use App\RequestFilter;
use App\SavedPlaces;
use App\PromocodeUsage;
use App\ReferralUsage;
use App\ProviderService;
use App\UserRequestRating;
use App\ServiceCarBrand;
use App\Http\Controllers\ProviderResources\TripController;
use Location\Coordinate;
use Location\Distance\Vincenty;

class UserApiController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function signup(Request $request)
    {
        $this->validate($request, [
                'social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
                'device_type' => 'required|in:android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
              //  'mobile' => 'required|digits_between:6,13',
                'mobile' => 'required|digits:10',
                'password' => 'required|min:6',
            ]);

        try{
            
            $User = $request->all();

            
			//$s = ReferralUsage::where('user_id',$User->id)->count();
			//Log::info("ReferralUsage count:".$s);
			 //$request->referral_id ='E1PES';
			  if ($request->has('referral_id')) {
				
				 $user1 = User::where('referral_code' , $request->referral_id)->first();
				 
				 if($user1 == null)
				 {
						
						//return $User;
						return response()->json(['error' => 'Invalid Referral Code'], 500);
				 }
				 else{
						$User['payment_mode'] = 'CASH';
						$User['password'] = bcrypt($request->password);
						$User['referral_code'] = Helper::generate_referral_code();
						$User = User::create($User);

						Log::info("NEW USER ID:".$User->id);
				$User2=User::where('id', $User->id)->first();
				
				$a = ReferralUsage::where('user_id',$User2->id)->count();
				$b = ReferralUsage::where('referral_user',$user1->id)->count();
				$c = Setting::get('referral_usage');
				
				Log::info("a count:".$a);
				Log::info("b count:".$b);
				Log::info("c val:".$c);
				if(( $a< 1) &&( $b <= $c))
				{
					Log::info("entered loop");
				  $Refer = new ReferralUsage;
				$Refer->referral_user = $user1->id;
				$Refer->user_id = $User2->id;
				$Refer->status = 'ADDED';
				$Refer->save();  
				
				 $referral_receiver = Setting::get('referral_receiver');
				$referral_sender = Setting::get('referral_sender');
				
				$wallet1 = $user1->wallet_balance + $referral_sender;
				$wallet2 = $User2->wallet_balance + $referral_receiver;
				User::where('id',$user1->id)->update(['wallet_balance' => $wallet1]);
				User::where('id',$User2->id)->update(['wallet_balance' => $wallet2]); 
				} 
				 }
			}   
			else{
				$User['payment_mode'] = 'CASH';
            $User['password'] = bcrypt($request->password);
			$User['referral_code'] = Helper::generate_referral_code();
            $User = User::create($User);

			Log::info("NEW USER ID:".$User->id);
			}
			
            return $User;
        } catch (Exception $e) {
             return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function logout(Request $request)
    {
        try {
            User::where('id', $request->id)->update(['device_id'=> '', 'device_token' => '']);
            return response()->json(['message' => trans('api.logout_success')]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function change_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required',
            ]);

        $User = Auth::user();

        if(Hash::check($request->old_password, $User->password))
        {
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => trans('api.user.password_updated')]);
            }else{
                return back()->with('flash_success', 'Password Updated');
            }

        } else {
            return response()->json(['error' => trans('api.user.incorrect_password')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function update_location(Request $request){

        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

        if($user = User::find(Auth::user()->id)){

            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
            $user->save();

            return response()->json(['message' => trans('api.user.location_updated')]);

        }else{

            return response()->json(['error' => trans('api.user.user_not_found')], 500);

        }
	}
	
	public function update_home_work(Request $request){

        $this->validate($request, [
                'home' => 'max:255',
				'h_latitude' => 'numeric',
                'h_longitude' => 'numeric',
                'work' => 'max:255',
				'w_latitude' => 'numeric',
                'w_longitude' => 'numeric',
             ]);

        if($user = User::find(Auth::user()->id)){

		 if($request->has('home')){
            $user->home = $request->home;
            $user->h_latitude = $request->h_latitude;
            $user->h_longitude = $request->h_longitude;
		 }
		  if($request->has('work')){
            $user->work = $request->work;
            $user->w_latitude = $request->w_latitude;
            $user->w_longitude = $request->w_longitude;
		 }
		 
            $user->save();
			return $user;
          //  return response()->json(['message' => trans('api.user.home_updated')]);

        }else{

            return response()->json(['error' => trans('api.user.user_not_found')], 500);

        }
	}

	
	public function add_savedplaces(Request $request){

        $this->validate($request, [
				'name' => 'required|max:255',
				'address' => 'max:255',
                'latitude' => 'numeric',
                'longitude' => 'numeric',
             ]);

        if($user = User::find(Auth::user()->id)){

		if($request->name =='HOME'){
            $user->home = $request->address;
            $user->h_latitude = $request->latitude;
            $user->h_longitude = $request->longitude;
			$user->save();
			//return response()->json(['message' => trans('api.user.home_updated')]);
		}
		  if($request->name =='WORK'){
            $user->work = $request->address;
            $user->w_latitude = $request->latitude;
            $user->w_longitude = $request->longitude;
			$user->save();
			//return response()->json(['message' => trans('api.user.work_updated')]);
		 }
		 
		 
		  if(($request->name !='HOME') && ($request->name !='WORK')){
				$savedplace = new SavedPlaces;
                $savedplace->user_id = Auth::user()->id;
                $savedplace->name = $request->name; 
                $savedplace->address = $request->address; 
                $savedplace->latitude = $request->latitude; 
                $savedplace->longitude = $request->longitude; 
                $savedplace->save();
			//	return response()->json(['message' => trans('api.user.saved_place')]);
		  }
		   

		    $y = array();
			$y['address']= $user->home;
			$y['latitude']= $user->h_latitude;
			$y['longitude']= $user->h_longitude;
		  
			$xy = array();
			$xy['address']= $user->work;
			$xy['latitude']= $user->w_latitude;
			$xy['longitude']= $user->w_longitude;
			
            //$savedplaces = SavedPlaces::SavedPlaces(Auth::user()->id)->get();
            
            $savedplaces = SavedPlaces::where('saved_places.user_id', '=', Auth::user()->id)->select('saved_places.*')->get();
		  
		  
		  return response()->json([
                    'home' => $y,
                    'work' => $xy,
					'saved_places' => $savedplaces,
                    'user' => $user
                ]);
				
        }else{

            return response()->json(['error' => trans('api.user.user_not_found')], 500);

        }
	}
	

	public function delete_address(Request $request){
			$this->validate($request, [
				'name' => 'required|max:255'
			 ]);
			 
			
        if($user = User::find(Auth::user()->id)){

		if($request->name =='HOME'){
            $user->home = null;
            $user->h_latitude = null;
            $user->h_longitude = null;
			$user->save();
			return response()->json(['message' => 'success']);
		}
		  if($request->name =='WORK'){
            $user->work = null;
            $user->w_latitude = null;
            $user->w_longitude = null;
			$user->save();
			return response()->json(['message' => 'success']);
		 }
		 
		 }else{

            return response()->json(['error' => trans('api.user.user_not_found')], 500);

        }
	}
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function details(Request $request){

        $this->validate($request, [
            'device_type' => 'in:android,ios',
        ]);

        try{

            if($user = User::find(Auth::user()->id)){

                if($request->has('device_token')){
                    $user->device_token = $request->device_token;
                }

                if($request->has('device_type')){
                    $user->device_type = $request->device_type;
                }

                if($request->has('device_id')){
                    $user->device_id = $request->device_id;
                }

                $user->save();

                $user->currency = Setting::get('currency');
                $user->sos = Setting::get('sos_number', '911');
                return $user;

            } else {
                return response()->json(['error' => trans('api.user.user_not_found')], 500);
            }
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function update_profile(Request $request)
    {

        $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'max:255',
                'email' => 'email|unique:users,email,'.Auth::user()->id,
                'mobile' => 'required|digits_between:6,13',
                'picture' => 'mimes:jpeg,bmp,png',
            ]);

         try {

            $user = User::findOrFail(Auth::user()->id);

            if($request->has('first_name')){ 
                $user->first_name = $request->first_name;
            }
            
            if($request->has('last_name')){
                $user->last_name = $request->last_name;
            }
            
            if($request->has('email')){
                $user->email = $request->email;
            }
        
            if($request->has('mobile')){
                $user->mobile = $request->mobile;
            }

            if ($request->picture != "") {
                Storage::delete($user->picture);
                $user->picture = $request->picture->store('user/profile');
            }

            $user->save();

            if($request->ajax()) {
                return response()->json($user);
            }else{
                return back()->with('flash_success', trans('api.user.profile_updated'));
            }
        }

        catch (ModelNotFoundException $e) {
             return response()->json(['error' => trans('api.user.user_not_found')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon-1 ; $i < $points_polygon; $j = $i++) {
		if ( (($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
		($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) ) 
			$c = !$c;
	  }
	  return $c;
	}
 

    public function services(Request $request) {
		Log::info('serviceslatitude: '.$request->latitude);	
		Log::info('serviceslongitude: '.$request->longitude);	
		Log::info('servicesride_in: '.$request->ride_in);	 
			
		 $this->validate($request, [
					'latitude' => 'required|numeric',
					'longitude' => 'required|numeric',
					'ride_in'=> 'required|in:INTER,OUTER,BOTH,RENTAL'
				]);
			
		$user = User::findOrFail(Auth::user()->id);
		$latitude = $user->latitude;
		$longitude = $user->longitude;
		$distance = Setting::get('provider_search_radius', '10');
		
		$ride_in = $request->ride_in;
        $Ride_Status[] = 'BOTH';
        $Ride_Status[] = $ride_in;
		$serviceList = ServiceType::orderBy('sort')->get();
		
		//$servicezones = ServiceZone::all();
		
		/* $array = explode(',', $zone->polygon);
										
		foreach($array as $key => $value) {
		if(!($key&1)) unset($array[$key]);
		} */
		
		//unset($serviceList[5]);
		Log::info('service_type count: '.count($serviceList));
		foreach ($serviceList as $key=>$val) {
		$service_type = $val['id']; 
		// Log::info('service_type: '.$service_type);
		$Providers = Provider::with('service')
		->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'id')
		->where('status', 'approved')
	   // ->whereIn('ride',$Ride_Status)
		->where('ride', 'like', '%'. $ride_in .'%')
		->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
		->whereHas('service', function($query) use ($service_type){
					$query->where('status','active');
					$query->where('service_type_id',$service_type);
				})
		->orderBy('distance')
		->get();
		
		 Log::info('Providers count: '.count($Providers));
		 if(count($Providers) > 0) { 
		
		 Log::info('Providers id: '.$Providers[0]->id);
		 $Provider = Provider::findOrFail($Providers[0]->id);
		 
		 Log::info('Providers lat: '.$Provider->latitude);
		 Log::info('Providers long: '.$Provider->longitude);
			$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$latitude.",".$longitude."&destinations=".$Provider->latitude.",".$Provider->longitude."&mode=driving&sensor=false&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
			
			/* $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch); */
			$json = curl($url);

            $response_a = json_decode($json, TRUE);
			//$response_a = json_decode($response, true);
			$dist = 0;
			$time = ".. mins";
			
			if($response_a['status'] == "OK"){
			$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
			$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
				$serviceList[$key]['time'] = $time;
			}
			else{
				$coordinate1 = new Coordinate($user->latitude, $user->longitude); /** Set Distance Calculation Source Coordinates ****/
				$coordinate2 = new Coordinate($Provider->latitude, $Provider->longitude); /** Set Distance calculation Destination Coordinates ****/
				$calculator = new Vincenty();
				$mydistance = $calculator->getDistance($coordinate1, $coordinate2); 
				$meter = round($mydistance);
				$dist = round($meter/1000);
				if($dist == 0)
				{
					$time = "1 min";
				}
				else{
					$time = ($dist * 1.2)." mins";
				}
				$serviceList[$key]['time'] = $time;
			}
				
			}
			else{
				$serviceList[$key]['time'] = "N/A";
			}
			
			if($val['servicezone'] == "all")
			{
				//Log::info('key +'. $key);
				$serviceList[$key]['polygon'] = "true";
			}
			else
			{ 
				//Log::info('ServiceZone'. $val['servicezone']);
				 $ServiceZone = ServiceZone::find(@$val['servicezone']);
				 if($ServiceZone) 
				 {	 
				Log::info('ServiceZone name'. $ServiceZone->name);
				$array_x = explode(',', $ServiceZone->polygon);
				$array_y = explode(',', $ServiceZone->polygon);
					 foreach($array_x as $key1 => $value1) {
						if(($key1&1)) unset($array_x[$key1]);
					}
					foreach($array_y as $key2 => $value2) {
						if(!($key2&1)) unset($array_y[$key2]);
					} 
					$array_x = array_values($array_x);//re-index starting to zero
					$array_y = array_values($array_y);//re-index starting to zero
				$points_polygon = count($array_x); // number vertices
				$longitude_x = $request->longitude; // x-coordinate of the point to test
				$latitude_y = $request->latitude;// y-coordinate of the point to test

				 if ($this->is_in_polygon($points_polygon, $array_x, $array_y, $longitude_x, $latitude_y))
					{
						//echo "Is in polygon!";
						Log::info('Is in polygon!');
						$serviceList[$key]['polygon'] = "true";
					}
					else 
					{ 
						//echo "Is not in polygon"; 
						Log::info('Is not in polygon');
						//unset($serviceList[$key]);
						$serviceList[$key]['polygon'] = "false";
					
					}  
			}	

			}
			
			
		 }   
		 
		/* $serviceList = ServiceType::orderBy('sort')->get();
		Log::info(print_r($serviceList, true));  */
		 
		// Log::info(print_r($serviceList, true));
		 $type_1 = array();
		 
					  
		$Rental = Rental::all();
		
		 foreach ($serviceList as $key=>$val) {
			$type_1 = Rental::where('service_type', $val['id'])->get();		 
			$serviceList[$key]['rental'] = $type_1;
		 }
		 
		  
        if($serviceList) {
            return $serviceList;
         } else {
            return response()->json(['error' => trans('api.services_not_found')], 500);
        } 

    }
	
	
	public function services1() {

        if($serviceList = ServiceType::all()) {
            return $serviceList;
        } else {
            return response()->json(['error' => trans('api.services_not_found')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function brands() {

        $serviceBrand = ServiceCarBrand::where('service_type_id',$service)->with('service_type','cars')->get();
        if($serviceBrand) {
            return $serviceBrand;
        } else {
            return response()->json(['error' => trans('api.services_not_found')], 500);
        }
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function chat_histroy(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer'
            ]);
        try{
            $Chat = Chat::where('request_id',$request->request_id)
                        ->where('user_id', Auth::user()->id)
                        ->get();
            return response()->json(["status"=>true,"messages"=>$Chat]);
        }catch (Exception $e) {
            return response()->json(["status"=>false,'error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function send_request(Request $request) {

	Log::info('New Request from User: '.Auth::user()->id);
        Log::info('Request Details:', $request->all());
		
        $this->validate($request, [
                's_latitude' => 'required|numeric',
                'd_latitude' => 'numeric',
                's_longitude' => 'required|numeric',
                'd_longitude' => 'numeric',
                'ride_in'=>'required|in:INTER,OUTER,BOTH,RENTAL',
                'service_type' => 'required|numeric|exists:service_types,id',
                'promo_code' => 'exists:promocodes,promo_code',
                'distance' => 'numeric',
                'use_wallet' => 'numeric',
               // 'advance_booking' => 'numeric',
                'payment_mode' => 'required|in:CASH,CARD,PAYPAL,PAYU,RAZORPAY',
                'card_id' => ['required_if:payment_mode,CARD','exists:cards,card_id,user_id,'.Auth::user()->id],
				'package' => 'numeric',
				'airport' => 'numeric',
            ]);

        

        //$ActiveRequests = UserRequests::PendingRequest(Auth::user()->id)->count();

        $ActiveRequests = 0;

        $broadcast = false;

        if($ActiveRequests > 0) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.ride.request_inprogress')], 500);
            } else {
                return redirect('dashboard')->with('flash_error', 'Already request is in progress. Try again later');
            }
        }

        if($request->has('schedule_date') && $request->has('schedule_time')){
            $beforeschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->subHour(1);
            $afterschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->addHour(1);

            $CheckScheduling = UserRequests::where('status','SCHEDULED')
                            ->where('user_id', Auth::user()->id)
                            ->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
                            ->count();


            if($CheckScheduling > 0){
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.request_scheduled')], 500);
                }else{
                    return redirect('dashboard')->with('flash_error', 'Already request is Scheduled on this time.');
                }
            }

        }elseif($request->has('user_started_at') ){
			if($request->user_started_at !=""){
               // $broadcast = true;
				$beforeschedule_time = (new Carbon("$request->user_started_at"))->subHour(1);
				$afterschedule_time = (new Carbon("$request->user_started_at"))->addHour(1);

				$CheckScheduling = UserRequests::where('status','SCHEDULED')
								->where('user_id', Auth::user()->id)
								->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
								->count();

				if($CheckScheduling > 0){
					if($request->ajax()) {
						return response()->json(['error' => trans('api.ride.request_scheduled')], 500);
					}else{
						return redirect('dashboard')->with('flash_error', 'Already request is Scheduled on this time.');
					}
				}
			}

        }

		
        $distance = Setting::get('provider_search_radius', '100');
        $latitude = $request->s_latitude;
        $longitude = $request->s_longitude;
        $service_type = $request->service_type;
        $ride_in = $request->ride_in;
        
        $Ride_Status[] = 'BOTH';
        $Ride_Status[] = $ride_in;

        $Providers = Provider::with('service')
            ->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'id','todays_ride')
            ->where('status', 'approved')
            ->where('last_login', '!=', NULL)
            //->whereIn('ride',$Ride_Status)
			->where('ride', 'like', '%'. $ride_in .'%')
			->whereDate('last_login', '>=', date('Y-m-d 00:00:00'))
			->whereDate('last_login', '<=', date('Y-m-d 23:59:59'))
            ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
			->orwhereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) = NULL")
            ->whereHas('service', function($query) use ($service_type){
                        $query->where('status','active');
                        $query->where('service_type_id',$service_type);
                    })
           // ->orderBy('distance')
			->orderBy('last_login', 'asc')
			//->orderBy('todays_ride', 'asc')
            ->get();

		
		
		if(count($Providers) > 0) {
		foreach ($Providers as $key => $Provider) {	
			if($Provider->todays_ride > 0)
			{
			$Providers = $Providers->sortBy(function ($bank, $key) {
				return $bank->todays_ride;
			});
			
			}
		}
		}
		
		foreach ($Providers as $key => $Provider) {
			Log::info('todays_ride prov id: '.$Provider->id);
		}
					
        // List Providers who are currently busy and add them to the filter list.

		if($request->has('schedule_date') && $request->has('schedule_time')){
			
		}
		elseif($request->has('user_started_at'))
		{
			
		}
		else
		{
			if(count($Providers) == 0) {
				if($request->ajax()) {
					// Push Notification to User
					return response()->json(['message' => trans('api.ride.no_providers_found')]); 
				}else{
					return back()->with('flash_success', 'No Providers Found! Please try again.');
				}
			}
		}

        try{

		
		
		 if($request->ride_in == 'RENTAL') 
		{ 
			$route_key = '';
		}
		else{	
            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$request->s_latitude.",".$request->s_longitude."&destination=".$request->d_latitude.",".$request->d_longitude."&mode=driving&key=AIzaSyDLw7mPIUwqqFnqxtRKUb6zznvayHE6wKg";

            $json = curl($details);

            $details = json_decode($json, TRUE);
			//Log::info('$response_a-status-'.$details['status']);	
			if($details['status'] == "OK"){
				$route_key = $details['routes'][0]['overview_polyline']['points'];
			}
			else{
				$route_key = '';
			}
            
		}
            $EstimateFare = $this->estimated_fare($request)->getData();

            $UserRequest = new UserRequests;
            $UserRequest->booking_id = Helper::generate_booking_id();
            $UserRequest->user_id = Auth::user()->id;
            if($request->has('schedule_date') && $request->has('schedule_time')){
              //  $broadcast = true;
                $UserRequest->schedule_at = date("Y-m-d H:i:s",strtotime("$request->schedule_date $request->schedule_time"));
            }
			
			if(count($Providers) > 0) {
				if($broadcast == false){
					$p =0;
					foreach ($Providers as $key => $Provider) {	
						if($Provider->todays_ride > 0)
						{
							$p = 1;
						}
					} 
					if($p == 1) {
						$g = 0;
						foreach ($Providers as $key => $Provider) {	
							if($g ==0) {
							$UserRequest->current_provider_id = $Provider->id;
							}
							$g++;
						}
					}
					else
					{
					$UserRequest->current_provider_id = $Providers[0]->id;
					}
				}
			}

            $UserRequest->service_type_id = $request->service_type;
            $UserRequest->payment_mode = $request->payment_mode;
            $UserRequest->estimated_fare = $EstimateFare->estimated_fare;
            $UserRequest->travel_day = $EstimateFare->travel_day;
            
            $UserRequest->broad_cast = ($broadcast == false?"NO":"YES");
            
			if(count($Providers) == 0) {
				$UserRequest->status = 'SCHEDULED';
			}
			else
			{
				$UserRequest->status = 'SEARCHING';
			}
            if($request->has('ride_in')){
            	$UserRequest->ride_in = $request->ride_in;
            }
			if($request->has('package')){
            	$UserRequest->package = $request->package;
            }
			
            if($request->has('ride_way')){
            	$UserRequest->ride_way = $request->ride_way;
            }
            if($request->has('user_started_at')){
            	$UserRequest->user_started_at= date("Y-m-d H:i:s",strtotime($request->user_started_at));
            	if($request->user_started_at !=""){
					$UserRequest->schedule_at = date("Y-m-d H:i:s",strtotime($request->user_started_at));
                    //$broadcast = true;
				}
            }
            if($request->has('user_finished_at')){
            	$UserRequest->user_finished_at= date("Y-m-d H:i:s",strtotime($request->user_finished_at));
            }
			
		/* 	if($request->has('advance_booking'))
		{
			if($request->advance_booking !="" && $request->advance_booking ==0)
			{
			$percentage = 15;
			$totalWidth = $EstimateFare->estimated_fare;
			$new_width = ($percentage / 100) * $totalWidth;

			$user = User::find(Auth::user()->id);
			$user->wallet_balance  = $user->wallet_balance - $new_width;
			$user->save();
			}
		} */
		
		

            $UserRequest->s_address = $request->s_address ? : "";
            $UserRequest->s_latitude = $request->s_latitude;
            $UserRequest->s_longitude = $request->s_longitude;
			
			$UserRequest->d_address = $request->d_address ? : "";
            $UserRequest->d_latitude = $request->d_latitude ? : "0.00";
            $UserRequest->d_longitude = $request->d_longitude ? : "0.00";
			
			if($request->has('airport')){
            	$UserRequest->airport = $request->airport;
            
				if($request->airport == 1)
				{
					$UserRequest->d_address = "Kempegowda International Airport, KIAL Rd, Devanahalli, Bengaluru, Karnataka 560300";
					$UserRequest->d_latitude = 13.199379;
					$UserRequest->d_longitude = 77.710136;
				}
			}
            $UserRequest->distance = $request->distance ? : "0.00";
            $UserRequest->travel_distance = $request->distance ? : "0.00";

            $UserRequest->use_wallet = $request->use_wallet ? : 0;
            
            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->route_key = $route_key;

            if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
                $UserRequest->surge = 0;
            }

			Log::info('user request before save'.$UserRequest);
            $UserRequest->save();

          //  (new SendPushNotification)->user_booking($UserRequest->user_id,$UserRequest->booking_id);

		  if($UserRequest->schedule_at == NULL)	
		{	
		$ddate = date('d-m-Y', strtotime($UserRequest->created_at));
		$ttime = date("H:i A",strtotime($UserRequest->created_at));
		}
		else
		{
		$ddate = date('d-m-Y', strtotime($UserRequest->schedule_at));
		$ttime = date("H:i A",strtotime($UserRequest->schedule_at));
		}
						
            $User = User::find($UserRequest->user_id);
			 if($User){
                $TextMsg = urlencode("ID: ".$UserRequest->booking_id." from ".$UserRequest->s_address." to ".$UserRequest->d_address." at ".$ddate." ".$ttime." is confirmed. Fare Rs.".$UserRequest->estimated_fare.".Toll Included. Preferred mode of payment : ".$UserRequest->payment_mode."  call us 080-41553311 www.wegacabs.com");
                $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$User->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
				
				Log::info('MessageUrl - '.$MessageUrl);
				
				$json = curl($MessageUrl);

                $MessageUrl = json_decode($json, TRUE);
                
                /* Mail::to($User)->send(new RequestUserBookingReminder($UserRequest->id)); */
            }  
            

            //Log::info('New Request id : '. $UserRequest->id .' Assigned to provider : '. $UserRequest->current_provider_id);


            // update payment mode 

            User::where('id',Auth::user()->id)->update(['payment_mode' => $request->payment_mode]);

            if($request->has('card_id')){

                Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
                Card::where('card_id',$request->card_id)->update(['is_default' => 1]);
            }

            $broadcast = false;

			if(count($Providers) > 0) {
            if($broadcast == false){
				
				$p =0;
					foreach ($Providers as $key => $Provider) {	
						if($Provider->todays_ride > 0)
						{
							$p = 1;
						}
					} 
					if($p == 1) {
						$g = 0;
						foreach ($Providers as $key => $Provider) {	
							if($g ==0) {
							(new SendPushNotification)->IncomingRequest($Provider->id);
							}
							$g++;
						}
					}
					else
					{
					 (new SendPushNotification)->IncomingRequest($Providers[0]->id);
					}
					
					
               
            }

			
            foreach ($Providers as $key => $Provider) {
                if($broadcast == true){
                    (new SendPushNotification)->IncomingRequest($Provider->id);
                }
                $Filter = new RequestFilter;
                // Send push notifications to the first provider
                // incoming request push to provider
                $Filter->request_id = $UserRequest->id;
                $Filter->provider_id = $Provider->id; 
                $Filter->save();
            }
			}

            if($request->ajax()) {
                return response()->json([
                        'message' => 'New request Created!',
                        'request_id' => $UserRequest->id,
                        'current_provider' => $UserRequest->current_provider_id,
                    ]);
            }else{
                return redirect('dashboard');
            }

        } catch (Exception $e) {
            //dd($e);
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function cancel_request(Request $request) {

        $this->validate($request, [
            'request_id' => 'required|numeric|exists:user_requests,id,user_id,'.Auth::user()->id,
        ]);

        try{

            $UserRequest = UserRequests::findOrFail($request->request_id);

            if($UserRequest->status == 'CANCELLED')
            {
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.already_cancelled')], 500); 
                }else{
                    return back()->with('flash_error', 'Request is Already Cancelled!');
                }
            }

            if(in_array($UserRequest->status, ['SEARCHING','STARTED','ARRIVED','SCHEDULED'])) {

                if($UserRequest->status != 'SEARCHING'){
                    $this->validate($request, [
                        'cancel_reason'=> 'max:255',
                    ]);
                }

                $UserRequest->status = 'CANCELLED';
                $UserRequest->cancel_reason = $request->cancel_reason;
                $UserRequest->cancelled_by = 'USER';
                $UserRequest->save();

				$user = User::findOrFail(Auth::user()->id);
				$servicetype = ServiceType::findOrFail($UserRequest->service_type_id);
				
				$to_time = date('Y-m-d h:i:s', time());
				$from_time = $UserRequest->accepted_at;
				$cancel_mins = round(abs($to_time - $from_time) / 60,2);

				if($cancel_mins > 5)
				{
				if($user->wallet_balance > $servicetype->ride_cancel_fee)
				{
				$user->wallet_balance = $user->wallet_balance - $servicetype->ride_cancel_fee;
				$user->save();
				}
				 else
				{
					$user->pending_cancel_fee = $user->pending_cancel_fee + $servicetype->ride_cancel_fee;
					$user->save();
				}   
				}
			
				
				
                RequestFilter::where('request_id', $UserRequest->id)->delete();

                if($UserRequest->status != 'SCHEDULED'){

                    if($UserRequest->provider_id != 0){

                        ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' => 'active']);

                    }
                }

				$Provider = User::find($UserRequest->provider_id);
				if($Provider){
					$TextMsg = urlencode("Dear ".$Provider->first_name." ".$Provider->last_name.", User has cancelled the Ride. Your Booking ID : ".$UserRequest->booking_id."");
					
				   $MessageUrl ="http://180.150.251.47/vendorsms/pushsms.aspx?user=wegacabsin&password=123456&msisdn=".$Provider->mobile."&sid=WEGACB&msg=".$TextMsg."&fl=0&gwid=2";
					$json = curl($MessageUrl);
					$MessageUrl = json_decode($json, TRUE);
				}
				
				
                 // Send Push Notification to User
                (new SendPushNotification)->UserCancellRide($UserRequest);

                if($request->ajax()) {
                    return response()->json(['message' => trans('api.ride.ride_cancelled')]); 
                }else{
                    return redirect('dashboard')->with('flash_success','Request Cancelled Successfully');
                }

            } else {
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.already_onride')], 500); 
                }else{
                    return back()->with('flash_error', 'Service Already Started!');
                }
            }
        }

        catch (ModelNotFoundException $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }else{
                return back()->with('flash_error', 'No Request Found!');
            }
        }

    }

    /**
     * Show the request status check.
     *
     * @return \Illuminate\Http\Response
     */

    public function request_status_check(Request $request) {

	//Log::info('Estimate', $request->all());
        try{
			//Log::info('request_status_check'.request()->uuid);
            //Log::info('device_id');
            
			 if ( request()->has("uuid")) {
				// User::where('id',Auth::user()->id)->update(['device_id' => request()->uuid]);
                $udid = Auth::user()->device_id;
				//Log::info('device_id'.$udid);
				//Log::info('uuid'.request()->uuid);
				
                if ($request->uuid != $udid) {

                    $Response = [
                        'account_status' => "invalid_session"
                    ];
                    return json_encode($Response);
                }

            }
			
			if($user = User::find(Auth::user()->id)){

            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
            $user->save();
			}
				
            $check_status = ['CANCELLED', 'SCHEDULED'];

           /*  $UserRequests = UserRequests::UserRequestStatusCheck(Auth::user()->id, $check_status)
                                        ->get()
										->toArray(); */
			$UserRequests = UserRequests::where('user_requests.user_id', Auth::user()->id)->where('user_requests.user_rated',0)
                    ->whereNotIn('user_requests.status', $check_status)
                    ->select('user_requests.*')
                    ->with('user','provider','service_type','provider_service','rating','payment');

			$er = $UserRequests->get()->toArray();
			
			if($er){
                $er[0]['provider_service']['service_model'] = CarCategory::where('id',$er[0]['provider_service']['car_categories_id'])->value('name');
            }

            $search_status = ['SEARCHING','SCHEDULED'];
            /* $UserRequestsFilter = UserRequests::UserRequestAssignProvider(Auth::user()->id,$search_status)->get(); */ 


			$UserRequestsFilter = UserRequests::where('user_requests.user_id', Auth::user()->id)
                    ->where('user_requests.user_rated',0)
                    ->where('user_requests.provider_id',0)
                    ->whereIn('user_requests.status', $search_status)
                    ->select('user_requests.*')
                    ->with('filter')->get();
					
					
            // Log::info($UserRequestsFilter);

            $Timeout = Setting::get('provider_select_timeout', 180);

            if(!empty($UserRequestsFilter)){
                for ($i=0; $i < sizeof($UserRequestsFilter); $i++) {
                    $ExpiredTime = $Timeout - (time() - strtotime($UserRequestsFilter[$i]->assigned_at));
                    if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime < 0) {
                        $Providertrip = new TripController();
                        $Providertrip->assign_next_provider($UserRequestsFilter[$i]->id);
                    }else if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime > 0){
                        break;
                    }
                }
            }  

            return response()->json(['data' => $er]);

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	 public function request_status_check1() {

	
        try{
			 $check_status = ['CANCELLED', 'SCHEDULED'];

            $UserRequests = UserRequests::UserRequestStatusCheck(Auth::user()->id, $check_status)
                                        ->get()
                                        ->toArray();
            if($UserRequests){
                $UserRequests[0]['provider_service']['service_model'] = CarCategory::where('id',$UserRequests[0]['provider_service']['car_categories_id'])->value('name');
            }

            $search_status = ['SEARCHING','SCHEDULED'];
            $UserRequestsFilter = UserRequests::UserRequestAssignProvider(Auth::user()->id,$search_status)->get(); 

            // Log::info($UserRequestsFilter);

            $Timeout = Setting::get('provider_select_timeout', 180);

            if(!empty($UserRequestsFilter)){
                for ($i=0; $i < sizeof($UserRequestsFilter); $i++) {
                    $ExpiredTime = $Timeout - (time() - strtotime($UserRequestsFilter[$i]->assigned_at));
                    if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime < 0) {
                        $Providertrip = new TripController();
                        $Providertrip->assign_next_provider($UserRequestsFilter[$i]->id);
                    }else if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime > 0){
                        break;
                    }
                }
            }

            return response()->json(['data' => $UserRequests]);

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }
	

    public function rate_provider(Request $request) {

        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id,user_id,'.Auth::user()->id,
                'rating' => 'required|integer|in:1,2,3,4,5',
                'comment' => 'max:255',
            ]);
    
        $UserRequests = UserRequests::where('id' ,$request->request_id)
                ->where('status' ,'COMPLETED')
                ->where('paid', 0)
                ->first();

        if ($UserRequests) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.user.not_paid')], 500);
            } else {
                return back()->with('flash_error', 'Service Already Started!');
            }
        }

        try{

            $UserRequest = UserRequests::findOrFail($request->request_id);
            
            if($UserRequest->rating == null) {
                UserRequestRating::create([
                        'provider_id' => $UserRequest->provider_id,
                        'user_id' => $UserRequest->user_id,
                        'request_id' => $UserRequest->id,
                        'user_rating' => $request->rating,
                        'user_comment' => $request->comment,
                    ]);
            } else {
                $UserRequest->rating->update([
                        'user_rating' => $request->rating,
                        'user_comment' => $request->comment,
                    ]);
            }

            $UserRequest->user_rated = 1;
            $UserRequest->save();

            $average = UserRequestRating::where('provider_id', $UserRequest->provider_id)->avg('user_rating');

            Provider::where('id',$UserRequest->provider_id)->update(['rating' => $average]);

            $User = User::find($UserRequest->user_id);
/* 
            if($User){
                Mail::to($User)->send(new RequestUserInvoiceReminder($UserRequest->id));
            }  */

            // Send Push Notification to Provider 
            if($request->ajax()){
                return response()->json(['message' => trans('api.ride.provider_rated')]); 
            }else{
                return redirect('dashboard')->with('flash_success', 'Driver Rated Successfully!');
            }
        } catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong');
            }
        }

    } 


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	 public function cancelled_trips() {
    
        try{
            //$UserRequests = UserRequests::UserCancelTrips(Auth::user()->id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'CANCELLED')
                    ->orderBy('user_requests.created_at','desc')
                    ->select('user_requests.*')
                    ->with('user','service_type')->get();
            
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	
    public function trips() {
    
        try{
           // $UserRequests = UserRequests::UserTrips(Auth::user()->id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'COMPLETED')
                    ->orderBy('user_requests.created_at','desc')
                    ->select('user_requests.*')
                    ->with('payment','service_type')->get();
            
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	
	public function recent_trips() {
    
        try{
            /* $UserRequests = UserRequests::UserRecentTrips(Auth::user()->id)->get();
            $UserRequests1 = UserRequests::UserRecentTrips1(Auth::user()->id)->get();
            
			$a = array_merge(json_decode($UserRequests, true),json_decode($UserRequests1, true)); */
			
			$UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'COMPLETED')
                    ->where('user_requests.ride_in', '=', 'INTER')
                    ->orderBy('user_requests.created_at','desc')
                    ->limit(5)
                    ->select('d_latitude','d_longitude','d_address','ride_in')->get();
			
			$UserRequests1 = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)
                    ->where('user_requests.status', '=', 'COMPLETED')
                    ->where('user_requests.ride_in', '=', 'OUTER')
                    ->orderBy('user_requests.created_at','desc')
                    ->limit(5)
                    ->select('d_latitude','d_longitude','d_address','ride_in')->get();

			$a = array_merge(json_decode($UserRequests, true),json_decode($UserRequests1, true));
			
			$user = User::find(Auth::user()->id);
		//	$b =array();
		//	$b['trips'] = $a;
		//	$b['user'] = json_decode($user,true);
			//return $b;
          
			$y = array();
			$y['address']= $user->home;
			$y['latitude']= $user->h_latitude;
			$y['longitude']= $user->h_longitude;
		  
			$xy = array();
			$xy['address']= $user->work;
			$xy['latitude']= $user->w_latitude;
			$xy['longitude']= $user->w_longitude;
			
			/*  $savedplaces = SavedPlaces::SavedPlaces(Auth::user()->id)->get();  */
			
			 $savedplaces = SavedPlaces::where('saved_places.user_id', '=', Auth::user()->id)->select('saved_places.*')->get();
					
					
			$xdvy = array();
		  
		  return response()->json([
                    'home' => $y,
                    'work' => $xy,
					'recent_trips' => $a, 
                    'saved_places' => $savedplaces,
                    'user' => $user
                ]);
				
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	public function get_savedplaces() {
    
        try{
           // $savedplaces = SavedPlaces::SavedPlaces(Auth::user()->id)->get();

            $savedplaces = SavedPlaces::where('saved_places.user_id', '=', Auth::user()->id)
                    ->select('saved_places.*')->get();
			return $savedplaces;
          
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	public function wallet_statement() {
    
        try{
           // $UserPayment = UserPayment::WalletStatement(Auth::user()->id)->get();

            $UserPayment = UserPayment::where('user_id', '=', $user_id)->whereNotIn('type' , ['RIDE', 'ADVANCE'])->select('*')->get();
			return $UserPayment;
        }
		catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	

	public function checkForPendingRating(){
        $request = UserRequests::where('user_id', (Auth::user()->id))->latest()->with('rating')->first();
        if ($request==null) {
            return false;
        }
        if ($request->status=='CANCELLED') {
            return false;
        }
		if (($request->status=='SCHEDULED') && ($request->provider_id==0)) {
            return false;
        }
        $rating=$request->rating;
        if ($rating==null) {
            return true;
        }
        $value=$rating->user_rating;
        if (isset($value)) {
            return false;

        }else{
            return true;
        }

    }
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function estimated_fare(Request $request){
		
		 /* $haspendingRating=$this->checkForPendingRating();
        if ($haspendingRating) {

            return response()->json(['success',false,'message','Please rate the driver for previous ride before booking any new ride']);
        } */
		
        \Log::info('Estimate', $request->all());
        $this->validate($request,[
                's_latitude' => 'numeric',
                's_longitude' => 'numeric',
                'd_latitude' => 'numeric',
                'd_longitude' => 'numeric',
                'ride_in'=>'required|in:INTER,OUTER,BOTH,RENTAL',
                'service_type' => 'required|numeric|exists:service_types,id',
                'package' => 'numeric',
                'airport' => 'numeric',
            ]);

        try{

		if($request->ride_in != 'RENTAL') 
		{
			
			$Hours = 0;
            $kms =0;
            $extra_km = 0;
            $extra_hr = 0;
			$time = 0;
			$kilometer = 0;
			
			$details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=AIzaSyDLw7mPIUwqqFnqxtRKUb6zznvayHE6wKg";

            $json = curl($details);

            $details = json_decode($json, TRUE);

			if($details['status'] == "OK"){
            $meter = $details['rows'][0]['elements'][0]['distance']['value'];
            $time = $details['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $details['rows'][0]['elements'][0]['duration']['value'];
				
            $kilometer = round($meter/1000);
            $minutes = round($seconds/60);
			//$Hours = $seconds/3600;
			}
			else{
				$coordinate1 = new Coordinate($request->s_latitude, $request->s_longitude); /** Set Distance Calculation Source Coordinates ****/
				$coordinate2 = new Coordinate($request->d_latitude, $request->d_longitude); /** Set Distance calculation Destination Coordinates ****/
				$calculator = new Vincenty();
				$mydistance = $calculator->getDistance($coordinate1, $coordinate2); 
				$meter = round($mydistance);
				$kilometer = round($meter/1000);
				$time = ($kilometer * 1.2)." mins";
				$minutes = $time;
			}
		}
            $tax_percentage = Setting::get('tax_percentage');
            $commission_percentage = Setting::get('commission_percentage');
            $service_type = ServiceType::findOrFail($request->service_type);
            $DayPrices = 0;
            $DateFees = 0;
            $IsRound = false;
           
			
            if($request->ride_in == 'INTER'){
				
				
				//////////
				//$ServiceZone = ServiceZone::find(@$val['servicezone']);
				
				 // $ServiceZone = ServiceZone::where('name', 'Bengaluru')->first();
				
			/* if($ServiceZone) 
			 {	 
				Log::info('ServiceZone name'. $ServiceZone->name);
				$array_x = explode(',', $ServiceZone->polygon);
				$array_y = explode(',', $ServiceZone->polygon);
					 foreach($array_x as $key1 => $value1) {
						if(($key1&1)) unset($array_x[$key1]);
					}
					foreach($array_y as $key2 => $value2) {
						if(!($key2&1)) unset($array_y[$key2]);
					} 
					$array_x = array_values($array_x);//re-index starting to zero
					$array_y = array_values($array_y);//re-index starting to zero
				$points_polygon = count($array_x); // number vertices
				$longitude_x = $request->s_longitude; // x-coordinate of the point to test
				$latitude_y = $request->s_latitude;// y-coordinate of the point to test

				 if ($this->is_in_polygon($points_polygon, $array_x, $array_y, $longitude_x, $latitude_y))
					{
						//echo "Is in polygon!";
						//Log::info('source Is in polygon!');
					}
					else 
					{ 
						//Log::info('source Is not in polygon');
						return response()->json(['message' => 'Pickup location is outside city limits'], 500);
					} 

				$longitude_x = $request->d_longitude; // x-coordinate of the point to test
				$latitude_y = $request->d_latitude;// y-coordinate of the point to test

				 if ($this->is_in_polygon($points_polygon, $array_x, $array_y, $longitude_x, $latitude_y))
					{
						//echo "Is in polygon!";
						//Log::info('destination Is in polygon!');
					}
					else 
					{ 
						//Log::info('destination Is not in polygon');
					 
						return response()->json(['message' => 'Drop location is outside city limits'], 500);
					}
					
			}	  */ 
				
				
				////////////
				
				
    	        $price = $service_type->fixed;
                $base =  $service_type->fixed;
    	        
				
				if($kilometer <= $service_type->distance){
					$kilometer1 = 0;
                }
				else{
					$kilometer1 = $kilometer - $service_type->distance;
				}
    	       	if($service_type->calculator == 'MIN') {
    	                $price += $service_type->minute * $minutes;
    	        } else if($service_type->calculator == 'HOUR') {
    	                $price += $service_type->minute * 60;
    	        } else if($service_type->calculator == 'DISTANCE') {
    	                $price += ($kilometer1 * $service_type->price);
    	        } else if($service_type->calculator == 'DISTANCEMIN') {
    	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
    	        } else if($service_type->calculator == 'DISTANCEHOUR') {
    	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
    	        } else {
    	                $price += ($kilometer * $service_type->price);
    	        }
            }else if($request->ride_in == 'OUTER'){
                $base =  $service_type->outer_fixed;
                $price =  $service_type->outer_fixed;
                $outerdistance = $kilometer;
                if($outerdistance <= $service_type->outer_distance){
                   $outerdistance = 0; 
                }
				else{
					$outerdistance = $outerdistance - $service_type->outer_distance;
				}
            	//$price  = 0;
    	       	if($service_type->calculator == 'MIN') {
    	              $price += $service_type->minute * $minutes;
    	        } else if($service_type->calculator == 'HOUR') {
    	               $price += $service_type->minute * 60;
    	        } else if($service_type->calculator == 'DISTANCE') {
    	                $price += ($outerdistance * $service_type->outer_price);
    	        } else if($service_type->calculator == 'DISTANCEMIN') {
    	                $price += ($outerdistance * $service_type->outer_price) + ($service_type->minute * $minutes);
    	        } else if($service_type->calculator == 'DISTANCEHOUR') {
    	                $price += ($outerdistance * $service_type->outer_price) + ($service_type->minute * $minutes * 60);
    	        } else {
    	                $price += ($outerdistance * $service_type->outer_price);
    	        }
    	        
    	        if($request->has('started_city') && $request->has('finished_city') && $request->ride_in == 'OUTER'){

                    $astartcity = explode(" ", $request->started_city);
                    $aendcity = explode(" ", $request->finished_city);

    	            $CityCount = Location::whereIn('from_city',$astartcity)
                                         ->whereIn('to_city',$aendcity)->count();                     
    	            if($CityCount==0) {
    	            	$IsRound = true;
    	            }
    	        }

            	if($IsRound == true || $request->ride_way == 'ROUND'){
	               $price = $price * 2;
            	}
                if($request->ride_way == 'ROUND'){
                   $kilometer = $kilometer * 2 ;
                }
               
    	        if($request->has('user_started_at') && $request->has('user_finished_at')  && ($request->ride_in == 'OUTER' || $request->ride_way == 'ROUND')){
    	           $CreatedDate = date("Y-m-d H:i:s",strtotime($request->user_started_at));
    	           $FinishedDate = date("Y-m-d H:i:s",strtotime($request->user_finished_at));
    	           //$CreatedDate  = date_create($CreatedDate);
    	           //$FinishedDate = date_create($FinishedDate);
    	           //$TimeInterval = date_diff($CreatedDate,$FinishedDate);
                   $DateFees = round((strtotime($request->user_finished_at) - strtotime($request->user_started_at))/3600, 1);
    	           //$DateFees  = $TimeInterval->format('%h');
                   if($DateFees>0){
                      $DateFees  = ceil($DateFees/24);
                   }else{
                      $DateFees = 0;
                   }
    	           $price += ($service_type->day* $DateFees);
    	           $DayPrices = ($service_type->day* $DateFees);
    	        }
                
              //  $price += $base;
            }else if($request->ride_in == 'RENTAL'){
				
				$kilometer = 0 ;
				$time = 0;
				
				 if($request->package != '')
				{
					$rental = Rental::where('service_type',$request->service_type)
							->where('hours',$request->package)
							->first();
					if($rental)
					{						
						$price = $rental->amount;
						$base = $rental->amount;
						$Hours = $rental->hours;
						$kms = $rental->kms;
						$extra_km = $rental->extra_km;
						$extra_hr = $rental->extra_hours;
					}
					else{
						$price = 0;
						$base = 0;
						$Hours = 0;
						$kms = 0;
						$extra_km = 0;
						$extra_hr = 0;
					}
				}
				if($request->airport == '1')
				{
					$base = $service_type->airport;
					$price = $service_type->airport;
					$Hours = 0;
					$kms = 0;
					$extra_km = 0;
					$extra_hr = 0;
				}
				
				
			}

            $price += $service_type->connection;
            $commission_price = ( $commission_percentage/100 ) * $price;
            $price += $commission_price;
            $tax_price = ( $tax_percentage/100 ) * $price;
            $total = $price + $tax_price;

            /* $ActiveProviders = ProviderService::AvailableServiceProvider($request->service_type)->get()->pluck('provider_id'); */
			
			$ActiveProviders = ProviderService::where('service_type_id', $request->service_type)->where('status', 'active')->get()->pluck('provider_id');
			

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->s_latitude;
            $longitude = $request->s_longitude;
            $ride_in = $request->ride_in;
            $Ride_Status[] = 'BOTH';
            $Ride_Status[] = $ride_in;

            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereIn('ride',$Ride_Status)
                ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                ->get();

            $surge = 0;
            $surge_price = 0;
            /*if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
                $surge_price = (Setting::get('surge_percentage')/100) * $total;
                $total += $surge_price;
                $surge = 1;
            }*/

            return response()->json([
                    'estimated_fare' => round($total,2), 
                    'distance' => $kilometer,
                    'time' => $time,
                    'hours' => $Hours,
                    'kms' => $kms,
                    'extra_km' => $extra_km,
                    'extra_hr' => $extra_hr,
                    'surge' => $surge,
                    'surge_value' => '1.4X',
                    'ride_way' => (($IsRound == true)?"ONEROUND":$request->ride_way),
                    'tax_price' => $tax_price,
                    'commission_price' => $commission_price,
                    'base_price' => $base,
                    'day_price' => $DayPrices,
                    'travel_day' => $DateFees,
                    'wallet_balance' => Auth::user()->wallet_balance
                ]);

        } catch(Exception $e) {
            //return response()->json(['error' => trans('api.something_went_wrong')], 500);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	 public function cancel_trip_details(Request $request) {

         $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
    
        try{
           // $UserRequests = UserRequests::CancelUserTripDetails(Auth::user()->id,$request->request_id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)->where('user_requests.id', '=', $request->request_id)->where('user_requests.status', '=', 'CANCELLED')->select('user_requests.*')->with('user')->get();

            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
	
	
    public function trip_details(Request $request) {

         $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
    
        try{
           // $UserRequests = UserRequests::UserTripDetails(Auth::user()->id,$request->request_id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)->where('user_requests.id', '=', $request->request_id)->where('user_requests.status', '=', 'COMPLETED')->select('user_requests.*')->with('payment','service_type','user','provider','rating')->get();

            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x191919|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * get all promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function promocodes() {
        try{
            $this->check_expiry();

            return PromocodeUsage::Active()
                    ->where('user_id', Auth::user()->id)
                    ->with('promocode')
                    ->get();

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    } 

    /**
     * get all promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_promocodes() {
        try{
            
            $PromoCodes = Promocode::where('expiration','>=' ,date("Y-m-d"))
                            ->where('status' ,"ADDED")
                            ->get();

            return $PromoCodes;

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    } 


    public function check_expiry(){
        try{
            $Promocode = Promocode::all();
            foreach ($Promocode as $index => $promo) {
                if(date("Y-m-d") > $promo->expiration){
                    $promo->status = 'EXPIRED';
                    $promo->save();
                    PromocodeUsage::where('promocode_id', $promo->id)->update(['status' => 'EXPIRED']);
                }
            }
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * add promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function add_promocode(Request $request) {

        $this->validate($request, [
                'promocode' => 'required|exists:promocodes,promo_code',
            ]);

        try{

            $find_promo = Promocode::where('promo_code',$request->promocode)->first();

            if($find_promo->status == 'EXPIRED' || (date("Y-m-d") > $find_promo->expiration)){

                if($request->ajax()){

                    return response()->json([
                        'message' => trans('api.promocode_expired'), 
                        'code' => 'promocode_expired'
                    ]);

                }else{
                    return back()->with('flash_error', trans('api.promocode_expired'));
                }

            }elseif(PromocodeUsage::where('promocode_id',$find_promo->id)->where('user_id', Auth::user()->id)->count() >= $find_promo->usage_limit){

                if($request->ajax()){

                    return response()->json([
                        'message' => trans('api.promocode_already_in_use'), 
                        'code' => 'promocode_already_in_use'
                        ]);

                }else{
                    return back()->with('flash_error', 'Promocode Already in use');
                }

            }else{

			for($x = 1; $x <= $find_promo->usage_limit; $x++) {
                $promo = new PromocodeUsage;
                $promo->promocode_id = $find_promo->id;
                $promo->user_id = Auth::user()->id;
                $promo->status = 'ADDED';
                $promo->save();
			}
                if($request->ajax()){

                    return response()->json([
                            'message' => trans('api.promocode_applied') ,
                            'code' => 'promocode_applied'
                         ]); 

                }else{
                    return back()->with('flash_success', trans('api.promocode_applied'));
                }
            }

        }

        catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something Went Wrong');
            }
        }

    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trips() {
    
        try{
            //$UserRequests = UserRequests::UserUpcomingTrips(Auth::user()->id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)->where('user_requests.status', '=', 'SCHEDULED')->orderBy('user_requests.created_at','desc')->select('user_requests.*')->with('service_type','provider')->get();

            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trip_details(Request $request) {

         $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
    
        try{
            //$UserRequests = UserRequests::UserUpcomingTripDetails(Auth::user()->id,$request->request_id)->get();

            $UserRequests = UserRequests::where('user_requests.user_id', '=', Auth::user()->id)
            ->where('user_requests.id', '=', $request->request_id)
            ->where('user_requests.status', '=', 'SCHEDULED')
            ->select('user_requests.*')
            ->with('service_type','user','provider')->get();

            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-start.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$value->route_key.
                            "&key=AIzaSyBRwQP02SSXRCyh5VQTaOsZ8o1eSG5nXzg";
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }


    /**
     * Show the nearby providers.
     *
     * @return \Illuminate\Http\Response
     */

	 public function deviceID(Request $request){
    $user=User::where('email', $request->email)->first();
    $user->device_id = $request->device_id;
    $user->save();
   // return json_encode(array("status",true));
	return response()->json(['status' => true]);
				

}

    public function show_providers(Request $request) {

        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
                'ride_in'=> 'required|in:INTER,OUTER,BOTH',
                'service' => 'numeric|exists:service_types,id',
            ]);

        try{

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $ride_in = $request->ride_in;
            $Ride_Status[] = 'BOTH';
            $Ride_Status[] = $ride_in;

            if($request->has('service')){
                /* $ActiveProviders = ProviderService::AvailableServiceProvider($request->service)->get()->pluck('provider_id'); */
				
				$ActiveProviders = ProviderService::where('service_type_id', $request->service)->where('status', 'active')->get()->pluck('provider_id');
				
				
				
                $Providers = Provider::whereIn('id', $ActiveProviders)
                    ->where('status', 'approved')
                    //->whereIn('ride',$Ride_Status)
					->where('ride', 'like', '%'. $ride_in .'%')
                    ->whereRaw("( 6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get();
            } else {
                $Providers = Provider::where('status', 'approved')
                    ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get();
            }

			Log::info("count providers:".count($Providers));
            if(count($Providers) == 0) {
                if($request->ajax()) {
                    return response()->json(['message' => "No Providers Found"]); 
                }else{
                    return back()->with('flash_success', 'No Providers Found! Please try again.');
                }
            }
        
            return $Providers;

        } catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }
    }


    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */


    public function forgot_password(Request $request){

        $this->validate($request, [
                'email' => 'required|email|exists:users,email',
            ]);

        try{  
            
            $user = User::where('email' , $request->email)->first();

            $otp = mt_rand(100000, 999999);

            $user->otp = $otp;
            $user->save();

            Notification::send($user, new ResetPasswordOTP($otp));

            return response()->json([
                'message' => 'OTP sent to your email!',
                'user' => $user
            ]);

        }catch(Exception $e){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function reset_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'id' => 'required|numeric|exists:users,id'
            ]);

        try{

            $User = User::findOrFail($request->id);
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => 'Password Updated']);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * help Details.
     *
     * @return \Illuminate\Http\Response
     */

    public function help_details(Request $request){

        try{

            if($request->ajax()) {
                return response()->json([
                    'contact_number' => Setting::get('contact_number',''), 
                    'contact_email' => Setting::get('contact_email','')
                     ]);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * help Details.
     *
     * @return \Illuminate\Http\Response
     */

    public function terms(Request $request){

        try{

            if($request->ajax()) {
                return response()->json(['status'=>true,
                    'terms' => Setting::get('user_terms') 
                     ]);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['status'=>false,'error' => trans('api.something_went_wrong')]);
            }
        }
    }
	
	public function referral()
	{
		
        try{
            $user = User::find(Auth::user()->id);
			
            return response()->json(["referral_code"=>$user->referral_code]);
			
        }catch (Exception $e) {
            return response()->json(["status"=>false,'error' => trans('api.something_went_wrong')], 500);
        }
	}
}
