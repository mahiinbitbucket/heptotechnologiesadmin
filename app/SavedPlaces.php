<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedPlaces extends Model
{
   	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','name','address','latitude','longitude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'
    ];

    /**
     * The services that belong to the user.
     */
    public function request()
    {
        return $this->belongsTo('App\UserRequests');
    }
	
	
	public function scopeSavedPlaces($query, $user_id)
    {
        return $query->where('saved_places.user_id', '=', $user_id)
                    ->select('saved_places.*');
                    
    }
}
