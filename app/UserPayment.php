<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'txnid',
        'amount'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'created_at', 'updated_at'
    ];

	protected $dates = [
        'created_at',
        'updated_at'
        
    ];
	
	public function scopeWalletStatement($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id)
				->whereNotIn('type' , ['RIDE', 'ADVANCE'])
                    ->select('*');
                    
    }
	
}
